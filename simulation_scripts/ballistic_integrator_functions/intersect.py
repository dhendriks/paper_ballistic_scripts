"""
Functions to determine the (self) intersection of trajectories
"""

import numpy as np

# A Python3 program to find if 2 given line segments intersect or not


# Given three colinear points p, q, r, the function checks if
# point q lies on line segment 'pr'
def onSegment(p_x, p_y, q_x, q_y, r_x, r_y):
    if (
        (q_x <= max(p_x, r_x))
        and (q_x >= min(p_x, r_x))
        and (q_y <= max(p_y, r_y))
        and (q_y >= min(p_y, r_y))
    ):
        return True
    return False


def orientation(p_x, p_y, q_x, q_y, r_x, r_y):
    # to find the orientation of an ordered triplet (p,q,r)
    # function returns the following values:
    # 0 : Colinear points
    # 1 : Clockwise points
    # 2 : Counterclockwise

    # See https://www.geeksforgeeks.org/orientation-3-ordered-points/amp/
    # for details of below formula.

    val = (float(q_y - p_y) * (r_x - q_x)) - (float(q_x - p_x) * (r_y - q_y))
    if val > 0:
        # Clockwise orientation
        return 1
    elif val < 0:
        # Counterclockwise orientation
        return 2
    else:
        # Colinear orientation
        return 0


# The main function that returns true if
# the line segment 'p1q1' and 'p2q2' intersect.
def doIntersect(p1_x, p1_y, q1_x, q1_y, p2_x, p2_y, q2_x, q2_y):
    # Find the 4 orientations required for
    # the general and special cases
    o1 = orientation(p1_x, p1_y, q1_x, q1_y, p2_x, p2_y)
    o2 = orientation(p1_x, p1_y, q1_x, q1_y, q2_x, q2_y)
    o3 = orientation(p2_x, p2_y, q2_x, q2_y, p1_x, p1_y)
    o4 = orientation(p2_x, p2_y, q2_x, q2_y, q1_x, q1_y)

    # General case
    if (o1 != o2) and (o3 != o4):
        return True

    # Special Cases

    # p1 , q1 and p2 are colinear and p2 lies on segment p1q1
    if (o1 == 0) and onSegment(p1_x, p1_y, p2_x, p2_y, q1_x, q1_y):
        return True

    # p1 , q1 and q2 are colinear and q2 lies on segment p1q1
    if (o2 == 0) and onSegment(p1_x, p1_y, q2_x, q2_y, q1_x, q1_y):
        return True

    # p2 , q2 and p1 are colinear and p1 lies on segment p2q2
    if (o3 == 0) and onSegment(p2_x, p2_y, p1_x, p1_y, q2_x, q2_y):
        return True

    # p2 , q2 and q1 are colinear and q1 lies on segment p2q2
    if (o4 == 0) and onSegment(p2_x, p2_y, q1_x, q1_y, q2_x, q2_y):
        return True

    # If none of the cases
    return False


def check_self_intersection(step, steps_check_self_intersection, positions):
    new_positions = np.array(positions[step - steps_check_self_intersection :])
    old_positions = np.array(positions[: step - steps_check_self_intersection])

    # Create a copy where I append new_position elements into when they are handled.
    old_positions_temp = old_positions.copy()

    # Create segments
    for new_segment_i in range(len(new_positions) - 1):
        p1 = new_positions[new_segment_i]
        q1 = new_positions[new_segment_i + 1]

        for old_segment_j in range(len(old_positions_temp) - 1):
            p2 = old_positions_temp[old_segment_j]
            q2 = old_positions_temp[old_segment_j + 1]

            intersecting = doIntersect(
                p1[0], p1[1], q1[0], q1[1], p2[0], p2[1], q2[0], q2[1]
            )
            if intersecting:
                return True

        # Add p1 to the old positions AT THE END!!!
        if old_positions_temp.size > 0:
            old_positions_temp = np.append(old_positions_temp, np.array([p1]), axis=0)
        else:
            old_positions_temp = np.array([p1])

    # If return yes not already happened we can return that it didnt self intersect
    return False
