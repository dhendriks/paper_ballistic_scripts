"""
script that contains some plotting routines for the trajectory data
"""

import json

import astropy.units as u
import matplotlib.pyplot as plt
import numpy as np

from paper_ballistic_scripts.ballistic_integrator_functions.functions import (
    calc_mass_ratio_accretor_donor,
    calc_mu_accretor,
    calc_orbital_period,
)
from paper_ballistic_scripts.ballistic_integrator_functions.rochelobe_calculator import (
    return_phi,
)
from paper_ballistic_scripts.figure_scripts.utils import load_mpl_rc, show_and_save_plot

load_mpl_rc()

offset_text = 0.1
fontsize = 20


def return_lubow_shu_table_2_data():
    """
    Function that returns the lubow & shu 1975 data
    """

    q_acc_don_list = [
        0.067,
        0.1500,
        0.3000,
        0.5000,
        0.7500,
        1.0000,
        1.6667,
        3.0000,
        4.5000,
        5.5000,
        7.0000,
        8.5000,
        10.0000,
        15.0000,
    ]
    r_min_list = [
        0.0228,
        0.0276,
        0.0329,
        0.0382,
        0.0438,
        0.0488,
        0.0604,
        0.0793,
        0.0966,
        0.1066,
        0.1198,
        0.1316,
        0.1422,
        0.1714,
    ]
    r_circ_list = [
        0.0404,
        0.0492,
        0.0589,
        0.0683,
        0.0780,
        0.0865,
        0.1058,
        0.1363,
        0.1631,
        0.1781,
        0.1978,
        0.2147,
        0.2297,
        0.2698,
    ]

    return {
        "q_acc_don": q_acc_don_list,
        "rmin": r_min_list,
        "rcirc": r_circ_list,
    }


def ulrich_kolb(mass_donor, mass_accretor):
    """
    Function for ulrich and kolb
    """

    rmin = 0.0425 * np.power(
        ((mass_accretor / mass_donor) + (np.power(mass_accretor, 2) / mass_donor)),
        1.0 / 4.0,
    )

    rcirc = 1.7 * rmin

    return {"rmin": rmin, "rcirc": rcirc}


def fkr_rcirc(mass_donor, mass_accretor):
    """
    Function for fkr rcirc
    """

    rcirc = (1 + mass_donor / mass_accretor) * np.power(
        (0.500 - 0.227 * np.log10(mass_donor / mass_accretor)), 4
    )

    return {
        "rcirc": rcirc,
    }


def plot_stars_and_COM_function(fig, ax, mass_accretor, mass_donor, separation):
    """
    Function to plot the location of the stars and the center of mass
    TODO: depracte and replace with frame of reference handler functions
    """

    ##############
    # Calculate positions of stars
    pos_accretor = calc_pos_accretor(
        mass_accretor=mass_accretor,
        mass_donor=mass_donor,
        separation=separation,
    )
    pos_donor = calc_pos_donor(
        mass_accretor=mass_accretor,
        mass_donor=mass_donor,
        separation=separation,
    )

    ##############
    # Plot positions of stars and center of mass

    # Accretor
    ax.text(
        pos_accretor[0], pos_accretor[1] - offset_text, "M accretor", fontsize=fontsize
    )
    ax.plot(pos_accretor[0], pos_accretor[1], "rx")

    # donor
    ax.text(pos_donor[0], pos_donor[1] - offset_text, "M donor", fontsize=fontsize)
    ax.plot(pos_donor[0], pos_donor[1], "rx")

    # Plot CoM
    COM = 0
    ax.plot(COM, 0, "bx")
    ax.text(COM, -offset_text, "COM", fontsize=fontsize)

    return fig, ax


def plot_L_points_function(fig, ax, mass_accretor, mass_donor, separation):
    """
    Calculates the Lagrange points and plots these on the figure object
    """

    # Calculate the lagrange points
    lagrange_points = calculate_lagrange_points(
        mass_accretor=mass_accretor, mass_donor=mass_donor, separation=separation
    )

    # Loop over Lagrange points and plot them
    for L_point in lagrange_points:
        # Get location
        L_point_val = lagrange_points[L_point]

        # Plot
        ax.plot(
            [L_point_val[0]], [L_point_val[1]], marker="o", markersize=3, color="red"
        )
        ax.text(
            L_point_val[0], L_point_val[1] - offset_text, L_point, fontsize=fontsize
        )

    return fig, ax


def plot_trajectory_function(fig, ax, x_positions, y_positions):
    """
    Function that adds the trajectories plotting to an axis
    """

    # Plot trajectory
    ax.plot(x_positions, y_positions, marker="o", label="Trajectory")

    #
    return fig, ax


def plot_rochelobe_equipotentials_function(
    fig, ax, mass_donor, mass_accretor, separation
):
    """
    Function to plot the contour for the roche lobe
    """

    # config
    bounds_x = [-2, 2]
    bounds_y = [-2, 2]
    resolution = 1000
    # System description
    q = mass_accretor / mass_donor

    M2 = mass_accretor
    M1 = mass_donor

    # TODO: fix the M1 and M2 here
    # M2 = 1
    # M1 = M2 / q
    mu = M2 / (M1 + M2)
    mu1 = M1 / (M1 + M2)
    a = mu1 + mu

    # Get L1 location and the phi value at that point
    lagrange_points = calculate_lagrange_points(M1, M2, a)

    # Calculate potential values at those points
    phi_l1 = return_phi(lagrange_points["L1"][0], lagrange_points["L1"][1], mu)
    phi_l2 = return_phi(lagrange_points["L2"][0], lagrange_points["L2"][1], mu)
    phi_l3 = return_phi(lagrange_points["L3"][0], lagrange_points["L3"][1], mu)

    # Create a meshgrid for x and y
    xx, yy = np.meshgrid(
        np.linspace(bounds_x[0], bounds_x[1], resolution),
        np.linspace(bounds_y[0], bounds_y[1], resolution),
    )

    # Calculate all the phi values
    phi_values = return_phi(xx, yy, mu)

    if phi_l2 > phi_l3:
        phi_l2, phi_l3 = phi_l3, phi_l2

    # Draw the contours
    CS = ax.contour(
        xx,
        yy,
        phi_values,
        levels=[phi_l1, phi_l2, phi_l3],
        colors=["blue", "orange", "green"],
    )

    # Add labels for the contours
    fmt = {}
    strs = ["L1", "L2", "L3"]
    for l, s in zip(CS.levels, strs):
        fmt[l] = s

    # Label every other level using strings
    ax.clabel(CS, CS.levels, inline=True, fmt=fmt, fontsize=16)

    return fig, ax


def plot_system_information_panel_function(fig, ax, data):
    """
    Function to plot information about the system
    """

    # Config
    fontsize_info = 26

    # Read out things from the data and calculate the quantities
    mass_accretor = data["mass_accretor"] * u.Msun
    mass_donor = data["mass_donor"] * u.Msun
    mass_ratio_accretor_donor = calc_mass_ratio_accretor_donor(
        mass_accretor=mass_accretor, mass_donor=mass_donor
    )
    mu_accretor = calc_mu_accretor(
        mass_accretor=mass_accretor.value, mass_donor=mass_donor.value
    )
    separation = data["separation"] * u.Rsun
    orbital_period = calc_orbital_period(
        mass_accretor=mass_accretor.value,
        mass_donor=mass_donor.value,
        separation=separation.value,
    )
    omega = 2 * np.pi / orbital_period

    M2 = mass_accretor.value
    M1 = mass_donor.value
    a = separation.value
    mu = M2 / (M1 + M2)
    mu1 = M1 / (M1 + M2)
    a = mu1 + mu

    # Get L1 location and the phi value at that point
    lagrange_points = calculate_lagrange_points(M1, M2, a)

    # Calculate potential values at those points
    phi_l1 = return_phi(lagrange_points["L1"][0], lagrange_points["L1"][1], mu)
    phi_l2 = return_phi(lagrange_points["L2"][0], lagrange_points["L2"][1], mu)
    phi_l3 = return_phi(lagrange_points["L3"][0], lagrange_points["L3"][1], mu)

    # Construct the strings with the information
    mass_line = "Maccretor: {} [{}]".format(
        mass_accretor.value, mass_accretor.unit.to_string("latex_inline")
    ) + "\nMdonor: {} [{}]".format(
        mass_donor.value, mass_donor.unit.to_string("latex_inline")
    )
    ratio_line = r"$\mu$: {}".format(mu_accretor) + "\nq: {}".format(
        mass_ratio_accretor_donor
    )  # Ratios
    separation_line = "separation: {} [{}]".format(
        separation.value, separation.unit.to_string("latex_inline")
    )  # Distances
    period_omega_line = r"$\Omega$: {:.2E} [{}]".format(
        omega.value, omega.unit
    ) + "\nP: {:.2E} [{}]".format(
        orbital_period.value, orbital_period.unit.to_string("latex_inline")
    )
    lagrange_potential_values_line = (
        r"$\Phi_{{L1}}$: {:.2E}".format(phi_l1)
        + "\n"
        + r"$\Phi_{{L2}}$: {:.2E}".format(phi_l2)
        + "\n"
        + r"$\Phi_{{L3}}$: {:.2E}".format(phi_l3)
    )

    # Combine them
    textstr = "\n\n".join(
        (
            mass_line,
            ratio_line,
            separation_line,
            period_omega_line,
            lagrange_potential_values_line,
        )
    )

    # these are matplotlib.patch.Patch properties
    props = dict(boxstyle="round", facecolor="wheat", alpha=0.5)

    # place a text box in upper left in axes coords
    ax.text(
        0.05,
        0.95,
        textstr,
        transform=ax.transAxes,
        fontsize=fontsize_info,
        verticalalignment="top",
        bbox=props,
        wrap=True,
    )

    # Remove all shit of the axes there.
    ax.set_axis_off()

    return fig, ax


def plot_resultfile(
    resultfile,
    plot_trajectory=True,
    plot_lagrange_points=True,
    plot_stars_and_COM=True,
    plot_rochelobe_equipotentials=True,
    plot_system_information_panel=True,
    plot_settings={},
):
    """
    Function that loads the resultfile and plots the results
    """

    # Load data
    with open(resultfile, "r") as json_file:
        data = json.load(json_file)

    # Set up canvas
    fig = plt.figure(figsize=(30, 18))
    fig.subplots_adjust(hspace=0.7)

    # Set up gridspec
    gs = fig.add_gridspec(nrows=1, ncols=8)

    #
    if not plot_system_information_panel:
        ax = fig.add_subplot(gs[:, :])
    else:
        ax = fig.add_subplot(gs[:, :6])
        ax_info = fig.add_subplot(gs[:, -2:])

    # Plotting position trajectory
    x_positions = data["x_positions"]
    y_positions = data["y_positions"]

    # Plot the whole trajectory
    if plot_trajectory:
        fig, ax = plot_trajectory_function(
            fig=fig,
            ax=ax,
            x_positions=x_positions,
            y_positions=y_positions,
        )

    # Plot the L points
    if plot_lagrange_points:
        plot_L_points_function(
            fig=fig,
            ax=ax,
            mass_accretor=data["mass_accretor"],
            mass_donor=data["mass_donor"],
            separation=data["separation"],
        )

    # Plot position of stars and center of mass
    if plot_stars_and_COM:
        plot_stars_and_COM_function(
            fig=fig,
            ax=ax,
            mass_accretor=data["mass_accretor"],
            mass_donor=data["mass_donor"],
            separation=data["separation"],
        )

    if plot_rochelobe_equipotentials:
        plot_rochelobe_equipotentials_function(
            fig=fig,
            ax=ax,
            mass_accretor=data["mass_accretor"],
            mass_donor=data["mass_donor"],
            separation=data["separation"],
        )

    if plot_system_information_panel:
        plot_system_information_panel_function(fig=fig, ax=ax_info, data=data)

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


############################
# Archive

# def plot_radius(plot_obj, center, radius, label, **kwargs):
#     """
#     Plots a circle at a location with radius. kwargs contain all
#     the stuff for plotting that you want save for label

#     Uses an axes object to do this
#     """

#     theta = np.arange(0, 2 * np.pi, 0.1)
#     theta = np.append(theta, 0)

#     x_vals = radius * np.cos(theta) + center[0]
#     y_vals = radius * np.sin(theta) + center[1]

#     plot_obj.plot(x_vals, y_vals, label=label, **kwargs)
#     return plot_obj

# def plot_radii():
#     """
#     Function to plot all the radii that are involved:
#     Circularisation radius x
#     3:1 radius x
#     2:1 radius x
#     viscous outer radius
#     Primary Roche Lobe radius x
#     minimal radius x
#     Truncation radius
#     """

#     # Generate mass ratio range
#     mass_ratio_range = np.arange(0.01, 1.0, 0.01)

#     # Construct plot
#     plt.style.use("ggplot")
#     fig, ax = plt.subplots(1)

#     # R21 radius
#     ax.plot(mass_ratio_range, calc_r21_radius(mass_ratio_range), label="2:1 resonance")

#     # R31 radius
#     ax.plot(mass_ratio_range, calc_r31_radius(mass_ratio_range), label="3:1 resonance")

#     # Roche radius
#     ax.plot(
#         mass_ratio_range,
#         calc_roche_radius(mass_ratio_range),
#         label="Primary Roche radius",
#     )

#     # Circularisation radius
#     ax.plot(
#         mass_ratio_range,
#         calc_rcirc_new(mass_ratio_range),
#         label="Cirularisation radius",
#     )

#     # Rmin radius
#     ax.plot(mass_ratio_range, calc_rmin(mass_ratio_range), label="Minimal radius")

#     # viscous outer radius
#     ax.plot(
#         mass_ratio_range,
#         calc_outer_viscous_radius(mass_ratio_range),
#         label="viscous outer radius",
#     )

#     # Make up
#     ax.set_xlabel("q")
#     ax.set_ylabel("R/a")
#     ax.set_title("Radii")
#     ax.set_xlim(0.0, 0.6)
#     ax.set_ylim(0.2, 0.7)
#     ax.legend()
#     plt.show()

# def plot_system(
#     mass_accretor,
#     mass_donor,
#     separation,
#     plot_density=False,
#     plot_l_points=False,
#     plot_rmax=False,
#     plot_rcirc=False,
#     plot_rmin=False,
#     trajectory=None,
# ):
#     # from http://www.physics.usyd.edu.au/~helenj/SeniorAstro/lab4.pdf
#     # https://en.wikipedia.org/wiki/Lagrangian_point

#     # offset_text = 0.01

#     mass_accretor, mass_donor = float(mass_accretor), float(mass_donor)
#     Orb_per = orbital_period(
#         mass_accretor=mass_accretor, mass_donor=mass_donor, separation=separation
#     )

#     Z = 0  # Height value
#     COM = 0

#     # Values for the binaries:
#     mass_ratio_accretor_donor = calc_mass_ratio_accretor_donor(
#         mass_accretor=mass_accretor, mass_donor=mass_donor
#     )
#     mu_accretor = calc_mu_accretor(mass_accretor=mass_accretor, mass_donor=mass_donor)
#     mu_donor = calc_mu_donor(mass_accretor=mass_accretor, mass_donor=mass_donor)

#     ##############################################################
#     # First, create the figure
#     fig = plt.figure(1, figsize=(15, 8))

#     # Now, create the gridspec structure, as required
#     gs = gridspec.GridSpec(1, 2, width_ratios=[1, 0.2])
#     gs.update(left=0.05, right=0.95, bottom=0.08, top=0.93, wspace=0.02, hspace=0.03)

#     # First, the scatter plot
#     # Use the gridspec magic to place it
#     # --------------------------------------------------------
#     ax1 = plt.subplot(gs[0, 0])  # place it where it should be.
#     # --------------------------------------------------------

#     # Some configuration for the main axis
#     ax1.set_aspect("equal")

#     ##############################################################
#     # Plot positions of stars and center of mass
#     pos_accretor = np.array([-mu_donor, 0])
#     ax1.text(pos_accretor[0], pos_accretor[1] - offset_text, "M accretor", fontsize=8)
#     ax1.plot(pos_accretor[0], pos_accretor[1], "rx")

#     pos_donor = np.array([mu_accretor, 0])
#     print(pos_donor)
#     ax1.text(pos_donor[0], pos_donor[1] - offset_text, "M donor", fontsize=8)
#     ax1.plot(pos_donor[0], pos_donor[1], "rx")

#     # Plot CoM
#     ax1.plot(COM, 0, "bx")
#     ax1.text(COM, -offset_text, "COM", fontsize=8)
#     COM = 0

#     ##############################################################
#     # Plot information about the system

#     # --------------------------------------------------------
#     ax2 = plt.subplot(gs[0, 1])  # place it where it should be.
#     # --------------------------------------------------------

#     # Plot information about the binary
#     omega = -1

#     textstr = "\n".join(
#         (
#             r"Maccretor=%.2f M$_{\odot}$ Mdonor=%.2f M$_{\odot}$"
#             % (mass_accretor, mass_donor),  # masses
#             r"$\mu=%.2f$, q=%.2f" % (mu_accretor, mass_ratio_accretor_donor),  # Ratios
#             r"$separation(a)=%.2f$" % (separation,),  # Distances
#             r"$\Omega=%.2f$, P=%.2e yr" % (omega, Orb_per.value),
#         )
#     )  # Velocities

#     # these are matplotlib.patch.Patch properties
#     props = dict(boxstyle="round", facecolor="wheat", alpha=0.5)

#     # place a text box in upper left in axes coords
#     ax2.text(
#         0.05,
#         0.95,
#         textstr,
#         transform=ax2.transAxes,
#         fontsize=14,
#         verticalalignment="top",
#         bbox=props,
#     )

#     # Remove all shit of the axes there.
#     ax2.set_axis_off()

#     # Plot L-points
#     if plot_l_points:
#         ax1 = plot_L_points(
#             ax1,
#             mass_accretor=mass_accretor,
#             mass_donor=mass_donor,
#             separation=separation,
#         )

#     # # Plot Rmax
#     # if plot_rmax:
#     #     rmax = calc_rmax(1, mass_ratio)
#     #     ax1 = plot_radius(plot_obj=ax1, center=pos_accretor, radius=rmax, label='rmax')

#     # Plot Rmin
#     if plot_rmin:
#         rmin = calc_rmin(mass_accretor=mass_accretor, mass_donor=mass_donor)
#         ax1 = plot_radius(
#             plot_obj=ax1,
#             center=pos_accretor,
#             radius=rmin,
#             linestyle="--",
#             label="minimal stream distance",
#         )

#     # Plot Rcirc
#     if plot_rcirc:
#         rcirc = circularisation_radius(
#             mass_accretor=mass_accretor, mass_donor=mass_donor
#         )
#         ax1 = plot_radius(
#             plot_obj=ax1,
#             center=pos_accretor,
#             radius=rcirc,
#             label="Circularisation radius",
#         )

#     # if plot_outer_radius:
#     #     rout = calc_outer_viscous_radius()

#     # plot imshow
#     if plot_density:
#         xv, yv, result = calculate_potential_density(mu)
#         ax1.imshow(
#             result,
#             origin="lower",
#             interpolation="none",
#             extent=[np.min(xv), np.max(xv), np.min(yv), np.max(yv)],
#         )
#         ax1.contour(
#             xv[0],
#             yv[..., 0],
#             result,
#             # levels=levels
#         )

#     # Plot trajectory
#     if not trajectory is None:
#         x_pos, y_pos = trajectory[..., 0], trajectory[..., 1]
#         ax1.plot(x_pos, y_pos, label="Trajectory")

#     # Set legend
#     ax1.legend()

#     plt.show()

# def plot_angmom_info(resultfile):
#     with open(resultfile, "r") as f:
#         data = json.loads(f.read())

#     #
#     times = np.array(data["times"])
#     dts = times[1:] - times[:-1]

#     angmoms = np.array(data["angular_momenta"])
#     dangmoms = angmoms[1:] - angmoms[:-1]

#     fig, axes = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(16, 8))

#     plt.suptitle("Angmom and d(angmom)/dt for particle starting at L2")

#     axes[0].plot(
#         times,
#         angmoms,
#         label="q={} vel1={} pos1={}".format(
#             data["mass_accretor"] / data["mass_donor"],
#             str(data["extra_settings"]["initial_velocity_offset"]),
#             str(data["extra_settings"]["initial_position_offset"]),
#         ),
#     )
#     axes[0].set_ylabel("Distance to center [a]", fontsize=22)
#     axes[0].legend()

#     axes[1].plot(
#         times[1:],
#         dangmoms / dts,
#         label="q={} vel1={} pos1={}".format(
#             data["mass_accretor"] / data["mass_donor"],
#             str(data["extra_settings"]["initial_velocity_offset"]),
#             str(data["extra_settings"]["initial_position_offset"]),
#         ),
#     )
#     axes[1].set_ylabel(r"dr$_{to\ center}$/dt", fontsize=22)
#     axes[1].legend()

#     plt.show()

# def plot_distance_info(resultfile):
#     with open(resultfile, "r") as f:
#         data = json.loads(f.read())

#     #
#     times = np.array(data["times"])
#     dts = times[1:] - times[:-1]

#     distances = np.array(data["distances_from_center"])
#     ddistances = distances[1:] - distances[:-1]

#     fig, axes = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(16, 8))

#     plt.suptitle("Distance and d(distance)/dt for particle starting at L2")

#     axes[0].plot(
#         times,
#         distances,
#         label="q={} vel1={} pos1={}".format(
#             data["mass_accretor"] / data["mass_donor"],
#             str(data["extra_settings"]["initial_velocity_offset"]),
#             str(data["extra_settings"]["initial_position_offset"]),
#         ),
#     )
#     axes[0].set_ylabel("Distance to center [a]", fontsize=22)
#     axes[0].legend()

#     axes[1].plot(
#         times[1:],
#         ddistances / dts,
#         label="q={} vel1={} pos1={}".format(
#             data["mass_accretor"] / data["mass_donor"],
#             str(data["extra_settings"]["initial_velocity_offset"]),
#             str(data["extra_settings"]["initial_position_offset"]),
#         ),
#     )
#     axes[1].set_ylabel(r"dr$_{to\ center}$/dt", fontsize=22)
#     axes[1].legend()

#     plt.show()
