"""
Script containing utility functions for the ballistic integration code
"""

import astropy.units as u
import numpy as np
from astropy import constants as const

#################
# General functions


def dict_sum(input_dict):
    """
    Function to calculate the sum of all terms in a dictionary
    """

    total = 0
    for _, val in input_dict.items():
        total += val

    return total


# turn np array to list
def json_encoder(obj):
    """
    To handle output of new types
    """
    if type(obj).__module__ == np.__name__:
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return obj.item()
    else:
        try:
            string_version = str(obj)
            return string_version
        except:
            raise TypeError(
                "Unserializable object {} of type {}. Attempted to convert to string but that failed.".format(
                    obj, type(obj)
                )
            )

    raise TypeError("Unknown type:", type(obj))


def calc_f(omega, omega_prime):
    """
    Function to calculate the rate of asynchronicity TODO: check this
    """

    return omega_prime / omega


# general mathematical functions
def calc_dist(vec1, vec2):
    """
    Function to return distance
    """
    return calc_magn(vec2 - vec1)


def calc_magn(vec):
    """
    Function to return magnitude of vector
    """
    return np.linalg.norm(vec)


#################
#
def calc_mass_ratio_accretor_donor(mass_accretor, mass_donor):
    """
    Function to calculate

    .. math::
        q_{\mathrm{accretor/donor}} = M_{\mathrm{accretor}} / M_{\mathrm{donor}}
    """

    return mass_accretor / mass_donor


def calc_mass_ratio_donor_accretor(mass_accretor, mass_donor):
    """
    Function to calculate the mass ratio donor/accretor
    """

    return 1 / calc_mass_ratio_accretor_donor(
        mass_accretor=mass_accretor, mass_donor=mass_donor
    )


def calc_mu_accretor(mass_accretor, mass_donor):
    """
    Function to calculate the reduced mass of the accretor
    """

    return mass_accretor / (mass_accretor + mass_donor)


def calc_mu_donor(mass_accretor, mass_donor):
    """
    Function to calculate the reduced mass of the donor
    """

    return mass_donor / (mass_accretor + mass_donor)


def calculate_center_of_mass(m1, m2, x1, x2):
    """
    Function to calculate the location of the center of mass given 2 (point) masses
    """

    x_cm = (m1 * x1 + m2 * x2) / (m1 + m2)

    return x_cm


# Radii:
def calc_rmin_warner1995(mass_accretor, mass_donor):
    """
    Calculates the radius of closest approach of the stream to the center of the accreting object, according to book warner 1995/6 2.14

    Scaled to r/a (This should be multiplied by a to get proper stuff)

    Checked.
    """

    rmin = 0.0488 * np.power((mass_donor / mass_accretor), -0.464)

    return rmin


def calc_rcirc(mass_accretor, mass_donor):
    """
    Calculates the circularisation radius of the matter stream around the accreting object, according to book warner 1995/6 2.19

    Scaled to r/a (This should be multiplied by a to get proper stuff)

    Checked.
    """

    rcirc = 0.0859 * np.power((mass_donor / mass_accretor), -0.426)

    return rcirc


# def calc_rmax(q):
#     # From V.V Noeustroev + 2019
#     rmax = a * (0.6)/(1+q)
#     return rmax

# def calc_rcirc_new(q):
#     return (1+q) * (calc_rl1(q))**4


# def calc_outer_viscous_radius(q):
#     # Outer viscour radius in a steady state viscous disk
#     r_out = ((7.0/5)**2) * calc_rcirc_new(q)
#     return r_out


def calc_orbital_period(mass_accretor, mass_donor, separation):
    """
    Function to calculate the orbital period of the binary system based on the masses and the separation
    """

    total_mass = (mass_accretor + mass_donor) * u.solMass  # Input is in solarmass
    separation = separation * u.solRad  # input is in solar radii
    orbital_period = (
        2 * np.pi * np.sqrt((separation.to(u.m) ** 3) / (const.G * total_mass.to(u.kg)))
    )

    return orbital_period.to(u.yr)


def separation(mass_accretor, mass_donor, orbital_period):
    """
    Function that returns the distance based on the total mass (in units of solarmass)
    and the orbital period in days.
    """
    total_mass = (mass_accretor + mass_donor) * u.solMass  # Input is in solarmass
    orbital_period = orbital_period * u.yr  # Input is in days
    separation = np.power(
        (((orbital_period**2) / (4 * np.pi**2)) * const.G * total_mass.to(u.kg)),
        1.0 / 3.0,
    )
    return separation.to(u.solRad)


###################################
# Integration functions


def calculate_potential_density(mu):
    """
    Function to calculate the values of the roche potential and make it a density grid
    """

    # lowest potential point
    MIN_VAL_Y = -2

    # Box
    LOWER_X, UPPER_X, LOWER_Y, UPPER_Y = -2, 2.5, -2, 2

    # Stepsize of grid
    STEPSIZE = 0.01

    # Generate grid
    x_range = np.arange(LOWER_X, UPPER_X, STEPSIZE)
    y_range = np.arange(LOWER_Y, UPPER_Y, STEPSIZE)
    xv, yv = np.meshgrid(x_range, y_range)

    # generate roche potential from grid
    result = dimensionless_roche_potential(mu, xv, yv)

    # Cap the values again
    result[result < MIN_VAL_Y] = MIN_VAL_Y

    return xv, yv, result


def calc_pos_in_disk(radius, theta):
    """
    Function to calculate the position in the disk w.r.t center of the accretor
    """

    return np.array([np.cos(theta), np.sin(theta)]) * radius


def calc_keplerian_velocity(
    mass_accretor, mass_donor, separation, radius_in_disk, theta
):
    """
    Function to calculate the keplerian velocity of a particle in a disk around the accretor.

    separation is seen as input in AU, mass accretor in solarmass. Radisu in disk also in au.

    I will first do everything in cgs then express that velocity in terms of the characteristic velocity of the simulation (a omega)
    """

    G_cgs = const.G.to((u.cm**3) / (u.g * u.s**2))

    M_acc_cgs = (mass_accretor * u.Msun).to(u.g)
    M_don_cgs = (mass_donor * u.Msun).to(u.g)

    radius_in_disk_cgs = (radius_in_disk * u.AU).to(u.cm)

    # Calculate velocities
    angular_velocity_keplerian = np.sqrt((G_cgs * M_acc_cgs) / radius_in_disk_cgs**3)
    velocity = (
        radius_in_disk_cgs
        * angular_velocity_keplerian
        * np.array([-np.sin(theta), np.cos(theta)])
    )

    # Take characteristic velocity of simulation:
    # a * omega
    separation_cgs = (separation * u.AU).to(u.cm)
    angular_velocity_binary_system_cgs = np.sqrt(
        G_cgs * (M_acc_cgs + M_don_cgs) / separation_cgs**3
    )

    char_velocity = separation_cgs * angular_velocity_binary_system_cgs

    reduced_velocity = velocity / char_velocity

    return reduced_velocity.value
