"""
Rewrite of the trajectory calculation but now using an object to handle the integration

https://academic.oup.com/mnras/article-abstract/489/1/891/5548788

https://ipython-books.github.io/123-simulating-an-ordinary-differential-equation-with-scipy/
"""

import time
from enum import Enum, auto

import astropy.units as u
import numpy as np
from scipy.integrate import ode

from paper_ballistic_scripts.ballistic_integrator_functions.frame_of_reference.wrapper import (
    calculate_angular_momentum,
    calculate_energy,
    calculate_jacobi_constant,
)
from paper_ballistic_scripts.ballistic_integrator_functions.integrator.integration_functions import (
    RocheTrajectory,
    calc_distance_from_center,
    calc_orbital_angular_frequency,
    calc_orbital_period,
)
from paper_ballistic_scripts.ballistic_integrator_functions.intersect import (
    check_self_intersection,
)


class EXIT_CODES(Enum):
    """
    Enum class to containing simulaton status
    """

    PREEMPTIVE = -1
    NORMAL_FINISH = auto()
    INTEGRATOR_TERMINATION = auto()
    SELF_INTERSECTION = auto()
    FALLBACK_INTO_ROCHELOBE = auto()
    JACOBI_ERROR = auto()
    WRONG_DIRECTION = auto()
    TIMESTEP_TOO_SMALL = auto()
    EXCEEDED_MAX_STEPS = auto()
    CUSTOM_ERROR = auto()


# Define exit code messages
EXIT_CODE_MESSAGE_DICT = {
    EXIT_CODES.PREEMPTIVE.value: "Preemptive termination. RUNNING changed but not checked properly",
    EXIT_CODES.NORMAL_FINISH.value: "Integration finished succesfully. Terminated by max_time condition",
    EXIT_CODES.INTEGRATOR_TERMINATION.value: "Integrator failed with error. See integrator_return_code",
    EXIT_CODES.SELF_INTERSECTION.value: "Trajectory self-intersected",
    EXIT_CODES.FALLBACK_INTO_ROCHELOBE.value: "Trajectory fallback",
    EXIT_CODES.JACOBI_ERROR.value: "Jacobi constant not constant",
    EXIT_CODES.WRONG_DIRECTION.value: "Particle falling back to center",
    EXIT_CODES.TIMESTEP_TOO_SMALL.value: "Timestep too small",
    EXIT_CODES.EXCEEDED_MAX_STEPS.value: "Exceeded maximum number of allowed steps",
}


class trajectory_integrator:
    """
    Function that integrates the ballistic orbit of a particle in a roche potential

    Returns a dict with the following entries:

    data_dict = {
        # System parameters
        'mass_accretor': mass_accretor,                             # Mass accretor in solarmasses
        'mass_donor': mass_donor,                                   # Mass donor in solarmasses
        'separation': separation,                                   # Separation in AU
        'mass_ratio_acc_don': mass_ratio_acc_don,                   # Mass ratio m_acc/m_don
        'orbital_angular_frequency': orbital_angular_frequency,     # Orbital angular frequency (rad/s)

        # Integration results
        'positions': positions,                                     # 2d array of positions ([x,y])
        'velocities': velocities,                                   # 2d array of velocities ([vx, vy])
        'energies': energies,                                       # Array of energies
        'angular_momenta': angular_momenta,                         # Array of angular momenta
        'jacobis': jacobis,                                         # Array of jacobi constant values
        'distances_from_center': distances_from_center,             # Array of distances from center

        # Integration status results
        'exit_code': EXIT_CODE,                                     # Exit code. See exit_code_dict
        'exit_code_message': EXIT_CODE_DICT[EXIT_CODE],             # Exit code message
    }
    """

    def __init__(
        self,
        system_dict,
        frame_of_reference,
        initial_position,
        initial_velocity,
        control_settings,
        verbosity=1,
    ):
        # Initialise the passed parameters
        self.system_dict = system_dict
        self.frame_of_reference = frame_of_reference

        self.initial_position = initial_position
        self.initial_velocity = initial_velocity
        self.control_settings = control_settings
        self.integrator_reset_total = 0

        # Controller parameters
        self.verbosity = self.control_settings.get("verbosity", verbosity)
        self.max_time = self.control_settings.get("max_time", 100)
        self.max_steps = self.control_settings.get("max_steps", 100000)
        self.dt = self.control_settings.get("dt", 0.01)  # Play with this value a bit
        self.use_custom_timestep_rejection_routine = self.control_settings.get(
            "use_custom_timestep_rejection_routine", False
        )
        self.steps_check_self_intersection = self.control_settings.get(
            "steps_check_self_intersection", 10
        )
        self.jacobi_error_tol = self.control_settings.get("jacobi_error_tol", 1e-5)
        self.allow_fallback_to_center = self.control_settings.get(
            "allow_fallback_to_center", False
        )
        self.terminate_when_entering_rochelobes = self.control_settings.get(
            "terminate_when_entering_rochelobes", False
        )

        # Handle custom stopping condition
        self.use_custom_termination_routine = self.control_settings.get(
            "use_custom_termination_routine", False
        )

        # add a dictionary to handle the trajectory classification
        self.trajectory_classification_dict = {}

        # store exit codes
        self.EXIT_CODES = EXIT_CODES

        # Store exit code message dict
        self.EXIT_CODE_MESSAGE_DICT = EXIT_CODE_MESSAGE_DICT

    def _initialise(self):
        """
        Routine to initialise the integrator and set up the necessary quanities
        """

        ###############################
        # Initialisation

        # Register starting time
        self.start_time = time.time()

        # Initial conditions
        self.t0 = 0
        self.step = 0
        self.accepted_step = 0
        self.rejected_step = 0

        self.trajectory_classified = False
        self.initial_conditions = np.concatenate(
            (self.initial_position, self.initial_velocity)
        )
        self.RUNNING = True
        self.EXIT_CODE = self.EXIT_CODES.PREEMPTIVE

        # Calculate the initial things
        self.mass_ratio_acc_don = (
            self.system_dict["mass_accretor"] / self.system_dict["mass_donor"]
        )
        self.orbital_angular_frequency = calc_orbital_angular_frequency(
            mass_accretor=self.system_dict["mass_accretor"],
            mass_donor=self.system_dict["mass_donor"],
            separation=self.system_dict["separation"],
        )
        self.orbital_period = calc_orbital_period(
            orbital_angular_frequency=self.orbital_angular_frequency
        )
        self.initial_distance_from_center = calc_distance_from_center(
            x=self.initial_position[0], y=self.initial_position[1]
        )

        #
        self.initial_energy = calculate_energy(
            system_dict=self.system_dict,
            x_pos=self.initial_position[0],
            x_dot=self.initial_velocity[0],
            y_pos=self.initial_position[1],
            y_dot=self.initial_velocity[1],
            frame_of_reference=self.frame_of_reference,
            settings=self.control_settings,
        )
        self.initial_angular_momentum = calculate_angular_momentum(
            system_dict=self.system_dict,
            x_pos=self.initial_position[0],
            x_dot=self.initial_velocity[0],
            y_pos=self.initial_position[1],
            y_dot=self.initial_velocity[1],
            frame_of_reference=self.frame_of_reference,
        )
        self.initial_jacobi = calculate_jacobi_constant(
            system_dict=self.system_dict,
            x_pos=self.initial_position[0],
            x_dot=self.initial_velocity[0],
            y_pos=self.initial_position[1],
            y_dot=self.initial_velocity[1],
            frame_of_reference=self.frame_of_reference,
            settings=self.control_settings,
        )

        # Add initial values to lists
        self.positions = [self.initial_position]
        self.velocities = [self.initial_velocity]
        self.accelarations = [[0, 0]]
        self.energies = [self.initial_energy]
        self.angular_momenta = [self.initial_angular_momentum]
        self.jacobis = [self.initial_jacobi]
        self.distances_from_center = [self.initial_distance_from_center]
        self.dts = [self.dt]
        self.times = [0]
        self.distance_travelled = 0

        # Set up integrator
        self.setup_integrator(conditions=self.initial_conditions, time=self.t0)

    def setup_integrator(self, conditions, time):
        """
        Function to handle setting up the integrator and restarting

        More info at
          - https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.ode.html
          - https://stackoverflow.com/questions/12926393/using-adaptive-step-sizes-with-scipy-integrate-ode
        """

        #######
        # configure the ode
        self.integrator = ode(RocheTrajectory)

        self.integrator.set_initial_value(conditions, time)

        self.integrator.set_f_params(
            self.system_dict, self.frame_of_reference, self.control_settings
        )

        #######
        # Configure integrator type

        # dopri5
        if self.control_settings["integrator_type"] == "dopri5":
            self.integrator.set_integrator(
                "dopri5", **self.control_settings["integrator_configuration"]
            )
        # vode
        elif self.control_settings["integrator_type"] == "vode":
            self.integrator.set_integrator(
                "vode", **self.control_settings["integrator_configuration"]
            )
        # lsoda
        elif self.control_settings["integrator_type"] == "lsoda":
            self.integrator.set_integrator(
                "lsoda", **self.control_settings["integrator_configuration"]
            )
        else:
            raise ValueError(
                "Current choice ({}) of integrator type not supported".format(
                    self.control_settings["integrator_type"]
                )
            )

    def _integrate(
        self,
    ):
        """
        Function to handle the integration of the orbit of the particle
        """

        # Show some information
        if self.verbosity > 1:
            print("System initial info:")
            # System info
            print("\tMass Donor: {}".format(self.system_dict["mass_donor"]))
            print("\tMass accretor: {}".format(self.system_dict["mass_accretor"]))
            print("\tSeparation: {}".format(self.system_dict["separation"]))
            print(
                "\tSynchronicity factor: {}".format(
                    self.system_dict["synchronicity_factor"]
                )
            )
            print("\tEccentricity: {}".format(self.system_dict["eccentricity"]))
            print("\tMean anomaly: {}".format(self.system_dict["mean_anomaly"]))

            print("\tMass ratio (m_acc/m_don): {}".format(self.mass_ratio_acc_don))

            print("\tAngular frequency: {}".format(self.orbital_angular_frequency))
            print("\tOrbital period: {}".format(self.orbital_period.to(u.yr)))

            # Particle info
            print("\tinitial position: {}".format(self.initial_position))
            print("\tinitial velocity: {}".format(self.initial_velocity))
            print(
                "\tinitial angular momentum: {}".format(self.initial_angular_momentum)
            )
            print("\tinitial energy: {}".format(self.initial_energy))
            print("\tinitial Jacobi const: {}".format(self.initial_jacobi))

        # Start of main loop
        while self.RUNNING:
            #############
            # Integrate to t + dt
            self.time = self.integrator.t + self.dt
            self.integrator.integrate(self.time)

            #############
            # Select output and calculate the quantities that we need
            self.position = self.integrator.y[:2]
            self.velocity = self.integrator.y[2:]
            self.distance_from_center = calc_distance_from_center(
                x=self.position[0], y=self.position[1]
            )

            #
            self.energy = calculate_energy(
                system_dict=self.system_dict,
                x_pos=self.position[0],
                x_dot=self.velocity[0],
                y_pos=self.position[1],
                y_dot=self.velocity[1],
                frame_of_reference=self.frame_of_reference,
                settings=self.control_settings,
            )
            self.angular_momentum = calculate_angular_momentum(
                system_dict=self.system_dict,
                x_pos=self.position[0],
                x_dot=self.velocity[0],
                y_pos=self.position[1],
                y_dot=self.velocity[1],
                frame_of_reference=self.frame_of_reference,
            )
            self.jacobi = calculate_jacobi_constant(
                system_dict=self.system_dict,
                x_pos=self.position[0],
                x_dot=self.velocity[0],
                y_pos=self.position[1],
                y_dot=self.velocity[1],
                frame_of_reference=self.frame_of_reference,
                settings=self.control_settings,
            )

            self.jacobi_deviation = np.abs(
                (self.jacobi - self.initial_jacobi) / self.initial_jacobi
            )

            self.accelaration = (self.velocity - self.velocities[-1]) / self.dt

            #############
            # Handle rejection or acceptance
            self.timestep_reject = False
            if self.use_custom_timestep_rejection_routine:
                self.timestep_reject = self.custom_timestep_rejection_routine()

            #
            if not self.timestep_reject:
                #############
                # Store output
                self.positions.append(self.position)
                self.velocities.append(self.velocity)
                self.accelarations.append(self.accelaration)
                self.energies.append(self.energy)
                self.angular_momenta.append(self.angular_momentum)
                self.jacobis.append(self.jacobi)
                self.distances_from_center.append(self.distance_from_center)
                self.times.append(self.time)
                self.dts.append(self.dt)

                self.distance_travelled += np.linalg.norm(
                    self.positions[-1] - self.positions[-2]
                )

                #############
                # Print current state
                self._print_state_information()

                #############
                # Handle termination
                self._check_termination()

                # increment accepted steps
                self.accepted_step += 1

                # Increaste timestep with certain factor
                self.dt = self.dt * self.control_settings["timestep_increase_factor"]

            # Handle rejection step
            else:
                ############
                # Set time back to previous time and reset integrator
                self.time = self.integrator.t - self.dt

                # determine new initial conditions
                new_initial_conditions = np.concatenate(
                    (self.positions[-1], self.velocities[-1])
                )

                # reset integrator
                self.integrator_reset_start = time.time()
                self.setup_integrator(conditions=new_initial_conditions, time=self.time)
                self.integrator_reset_total += time.time() - self.integrator_reset_start

                # halve timestep
                self.dt = self.dt / 2

                if self.dt < self.control_settings["global_minimum_timestep"]:
                    self.set_terminate(exit_code=self.EXIT_CODES.TIMESTEP_TOO_SMALL)

                # increment rejected steps
                self.rejected_step += 1

            #
            if not self.integrator.successful():  # Check for successful integration
                self.set_terminate(exit_code=self.EXIT_CODES.INTEGRATOR_TERMINATION)

            #############
            # increment step
            self.step += 1

            if self.step > self.max_steps:
                self.set_terminate(exit_code=self.EXIT_CODES.EXCEEDED_MAX_STEPS)

    def _print_state_information(self):
        """
        Function to print information about the current state
        """

        if self.verbosity >= 2:
            print(
                "Time: {:.2e} dt: {:.2e} jacobi deviation: {:.2e} distance travelled: {:.2e} (step {} rejected: {} accepted: {})".format(
                    self.time,
                    self.dt,
                    self.jacobi_deviation,
                    self.distance_travelled,
                    self.step,
                    self.rejected_step,
                    self.accepted_step,
                )
            )

    def set_terminate(self, exit_code, additional_message=""):
        """
        Function to activate the termination of the integration
        """

        self.RUNNING = False
        self.EXIT_CODE = exit_code
        if self.verbosity > 0:
            print(
                "EXIT CODE: {} message: {}{}".format(
                    exit_code,
                    self.EXIT_CODE_MESSAGE_DICT[exit_code.value],
                    " {}".format(additional_message) if additional_message else "",
                )
            )

    def _check_termination(
        self,
    ):
        """
        Function to handle the termination checking
        """

        #
        self_intersection = False
        fallback = False
        wrong_direction = False

        ##########
        # Check for self intersection
        if not self.steps_check_self_intersection == -1:
            if (not self.step == 0) and (
                self.step % self.steps_check_self_intersection
            ) == 0:
                print("checking self intersection")
                self_intersection = check_self_intersection(
                    self.step, self.steps_check_self_intersection, self.positions
                )

        ##########
        # Check for direction of particle by looking at the d-distance (but only after some step)
        if not self.allow_fallback_to_center:
            if self.step > 10:
                if self.distances_from_center[-1] - self.distances_from_center[-2] < 0:
                    wrong_direction = True

        ##########
        # Fallback
        if self.terminate_when_entering_rochelobes:
            # TODO: calculate fallback things
            pass

        #########
        # Check for termination:
        if not self.integrator.t < self.max_time:  # Check for exceeding max time
            self.set_terminate(exit_code=self.EXIT_CODES.NORMAL_FINISH)

        elif not self.integrator.successful():  # Check for successful integration
            self.set_terminate(
                exit_code=self.EXIT_CODES.INTEGRATOR_TERMINATION,
                additional_message=self.integrator.get_return_code(),
            )

        elif self_intersection:  # Check if the trajectory is self interstecting
            self.set_terminate(exit_code=self.EXIT_CODES.SELF_INTERSECTION)

        elif fallback:  # Check for if the particle falls back into the roche lobes
            self.set_terminate(exit_code=self.EXIT_CODES.FALLBACK_INTO_ROCHELOBE)

        elif (
            self.jacobi_deviation > self.jacobi_error_tol
        ):  # Check if the jacobi constant is really constant
            self.set_terminate(exit_code=self.EXIT_CODES.JACOBI_ERROR)

        elif wrong_direction:
            self.set_terminate(exit_code=self.EXIT_CODES.WRONG_DIRECTION)

        elif self.dt < self.control_settings["global_minimum_timestep"]:
            self.set_terminate(exit_code=self.EXIT_CODES.TIMESTEP_TOO_SMALL)

        # This allows for a custom termination routine to be used. The termination routine will set the self.RUNNING and self.EXIT_CODE
        elif self.use_custom_termination_routine:
            self.custom_termination_routine()

    def _cleanup(self):
        """
        Function to clean up the simulation and handle the output
        """

        # Change shape arrays
        self.positions = np.array(self.positions)
        self.velocities = np.array(self.velocities)
        self.accelarations = np.array(self.accelarations)

        data_dict = {
            # System parameters
            "system_dict": self.system_dict,
            "mass_ratio_acc_don": self.mass_ratio_acc_don,
            "orbital_angular_frequency": self.orbital_angular_frequency.value,
            "settings": self.control_settings,
            # Integration results
            "x_positions": self.positions[:, 0],
            "y_positions": self.positions[:, 1],
            "vx_velocities": self.velocities[:, 0],
            "vy_velocities": self.velocities[:, 1],
            "x_accelarations": self.accelarations[:, 0],
            "y_accelarations": self.accelarations[:, 1],
            # Times
            "times": self.times,
            "timesteps": self.dts,
            # Other properties of the particle
            "energies": self.energies,
            "angular_momenta": self.angular_momenta,
            "jacobis": self.jacobis,
            "distances_from_center": self.distances_from_center,
            # Integration status results
            "exit_code": self.EXIT_CODE.value,
            "exit_code_name": self.EXIT_CODE.name,
            "exit_code_message": self.EXIT_CODE_MESSAGE_DICT[self.EXIT_CODE.value],
            "integrator_return_code": self.integrator.get_return_code(),
            "trajectory_classification_dict": self.trajectory_classification_dict,
        }

        # Register end time
        self.end_time = time.time()
        total_time = self.end_time - self.start_time

        #############
        # Message at the end
        if self.verbosity > 0:
            print(
                "Integration ended:\n"
                "\tSimulation Time: {:.2e}\n"
                "\tWalltime: {:.2e}. Integrator resetting took {:.2e} ({:.2e}% of total time)\n"
                "\tStep: {} (accepted: {} rejected: {})\n"
                "\tTerminated: {}\n"
                "\texit code: {} ({}): {}\n"
                "\tintegrator succesful: {} return code: {} (https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.ode.get_return_code.html)\n"
                "".format(
                    self.integrator.t,
                    total_time,
                    self.integrator_reset_total,
                    (self.integrator_reset_total / total_time) * 100,
                    self.step,
                    self.accepted_step,
                    self.rejected_step,
                    self.EXIT_CODE.value > 0,
                    self.EXIT_CODE.name,
                    self.EXIT_CODE.value,
                    self.EXIT_CODE_MESSAGE_DICT[self.EXIT_CODE.value],
                    self.integrator.successful(),
                    self.integrator.get_return_code(),
                )
            )

        return data_dict

    def custom_termination_routine(self):
        """
        Dummy placeholder for a custom termination routine
        """

        raise NotImplementedError

    def custom_timestep_rejection_routine(self):
        """
        Dummy placeholder for a custom timestep control function
        """

        raise NotImplementedError

    def evolve(
        self,
    ):
        """
        Entry point function of the class. Handles calling all the other functions
        """

        # Check
        # if self.use_custom_termination_routine:
        #     if not hasattr(self, "custom_termination_routine"):
        #         raise ValueError("Please provide a custom termination routine")

        # Initialise
        self._initialise()

        # integrate
        self._integrate()

        # clean up and return
        result = self._cleanup()

        # return output
        return result
