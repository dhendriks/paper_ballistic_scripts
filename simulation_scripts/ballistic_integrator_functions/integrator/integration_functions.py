"""
Script that contains extra functions for the trajectory calculation
"""

import astropy.constants as const
import astropy.units as u
import numpy as np

from paper_ballistic_scripts.ballistic_integrator_functions.frame_of_reference.wrapper import (
    calculate_dphi_dx,
    calculate_dphi_dy,
)


def calc_distance_from_center(x, y):
    """
    Function to calculate the distance from the center of the reference frame
    """

    distance_from_center = np.sqrt(np.power(x, 2) + np.power(y, 2))

    return distance_from_center


def calc_orbital_angular_frequency(mass_accretor, mass_donor, separation):
    """
    Function to calculate orbital angular frequency of the binary system.

    The input is in terms of solar units

    The output will be in SI units

    TODO: this is a bit tricky i've gotta say. Its easier to enforce providing astropy units than to silents assume something here
    """

    orbital_angular_frequency = np.sqrt(
        const.G
        * ((mass_accretor * u.solMass + mass_donor * u.solMass)).to(u.kg)
        / np.power((separation * u.Rsun).to(u.m), 3)
    )

    return orbital_angular_frequency


def calc_orbital_period(orbital_angular_frequency):
    """
    Function to calculate the orbital period of the binary system
    """

    return 2 * np.pi / orbital_angular_frequency


#
def RocheTrajectory(t, Y, system_dict, frame_of_reference, settings):
    """
    Computes the derivative of the state vector y according to the equation of motion:
    Y is the state vector (x, y, u, v) === (position, velocity).
    returns dY/dt. (dx/dt, dy/dt, dvx/dt, dvy/dt)

    This function does the real evolution of the trajectory in the co-rotating frame.\

    Uses eq 3a and 3b from hubova

    # See paper hubova pejcha 2019
    # TODO: generalise this and put into a function
    """

    # Extract positions and velocities
    x, y = Y[0], Y[1]
    vx, vy = Y[2], Y[3]

    # Determine accelarations
    dphi_dx = calculate_dphi_dx(
        system_dict=system_dict,
        x_pos=x,
        y_pos=y,
        frame_of_reference=frame_of_reference,
        settings=settings,
    )
    ax = -dphi_dx + 2 * vy

    dphi_dy = calculate_dphi_dy(
        system_dict=system_dict,
        x_pos=x,
        y_pos=y,
        frame_of_reference=frame_of_reference,
        settings=settings,
    )
    ay = -dphi_dy - 2 * vx

    return [vx, vy, ax, ay]
