"""
Module that calculates the lagrangepoints for a given system.

Copied from binary_c's lagrangepoint function.

Lagrange point positions where (x,y)=(0,0) is the
centre of mass.

Units of M1,M2 are arbitrary.

The distances are returned in the same
units as the separation.

Based on http://www.tp.umu.se/space/Proj_08/T_Munch.pdf
page 5 etc. (which is also in doc/T_Munch.pdf)

The centre of mass is (0,0,0).

The positions a1 and a2 are the x-coordinates of the stars
(which both have y = z = 0). NB a1,a2 can be NULL pointers,
in which case they are not set.

L1 is between the stars.
L2 is on star 2's side.
L3 is on star 1's side.
L4,5 are at y!=0.

We require M1>M2. If this is not the case, the masses (and corresponding results)
are flipped.
"""

import math

import numpy as np


def cuberoot(x):
    if 0 <= x:
        return x ** (1.0 / 3.0)
    return -((-x) ** (1.0 / 3.0))


def Quadratic(x, a, b, c):
    return a + b * x + c * pow(x, 2)


def Cubic(x, a, b, c, d):
    return Quadratic(x, a, b, c) + d * pow(x, 3)


def Quartic(x, a, b, c, d, e):
    return Cubic(x, a, b, c, d) + e * pow(x, 4)


def quintic(x, a, b, c, d, e):
    return Quartic(x, a, b, c, d, e) + f * pow(x, 5)


def calculate_lagrange_points(M1, M2, separation):
    """
    Function that calculates the lagrange points and returns a dictionary of tuples.

    x1 and x2 are the x-coordinates of the stars.

    See description above for more info

    TODO: fix this function to just work for mass donor mass accretor
    """

    flip = False

    if M2 > M1:
        M1, M2 = M2, M1
        flip = True

    mtot = M1 + M2
    mu1 = M1 / mtot
    mu2 = M2 / mtot
    alpha = cuberoot(mu2 / (3.0 * mu1))
    beta = mu2 / mu1

    # L1
    L1 = np.array(
        (
            Quartic(alpha, mu1, -1.0, 1.0 / 3.0, 1.0 / 9.0, 23.0 / 81.0)
            * (-1 if not flip else 1),  # +O(alpha^5)
            0,
            0,
        )
    )

    # L2
    L2 = np.array(
        (
            Quartic(
                alpha, mu1, +1.0, 1.0 / 3.0, -1.0 / 9.0, -31.0 / 81.0
            ),  # +O(alpha^5)
            0,
            0,
        )
    )

    # L3
    L3 = np.array(
        (
            mu1
            - Cubic(
                beta, 2.0, -7.0 / 12.0, +7.0 / 12.0, -13223.0 / 20736.0
            ),  # +O(beta^4)
            0,
            0,
        )
    )

    # L4
    L4 = np.array((0.5 - mu2, math.sqrt(3.0) / 2.0, 0))

    # L5
    L5 = np.array((0.5 - mu2, -math.sqrt(3.0) / 2.0, 0))

    # Locations of stars:
    a1 = -mu2 if not flip else mu1
    a2 = mu1 if not flip else -mu2

    # if flip:
    #     a1 = +mu1
    #     a2 = -mu2
    # else:
    #     a1 = -mu2
    #     a2 = +mu1
    a1 *= separation
    a2 *= separation

    # Scale everything by the separation
    L1 *= separation
    L2 *= separation
    L3 *= separation
    L4 *= separation
    L5 *= separation

    # Make dict: note here the order of the L2 and L3 change based on the flip

    L_dict = {}

    L_dict["L1"] = L1
    L_dict["L2"] = L2 if not flip else L3
    L_dict["L3"] = L3 if not flip else L3
    L_dict["L4"] = L4
    L_dict["L5"] = L5

    if flip:
        L_dict["L2"] = L3
        L_dict["L3"] = L2

    return L_dict


def calculate_lagrange_points_binary_c(mass_accretor, mass_donor, separation):
    """
    Wrapper function for the lagrange point calculation with the binary_c functions
    """

    L_dict = calculate_lagrange_points(
        M1=mass_accretor, M2=mass_donor, separation=separation
    )

    return L_dict
