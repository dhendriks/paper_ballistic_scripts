"""
Routines to calculate the lagrange points based on sepinski 2007.

In this paper, the frame of reference is centered on the donor, and rotates with the rotational velocity of the donor

Star 1 is the donor, star 2 is the accretor.

TODO: add the calculation for L4 and L5
TODO: clean up the functions here and make the names of the arguments more clear
"""

import warnings
from functools import lru_cache

import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import bisect

from paper_ballistic_scripts.ballistic_integrator_functions.functions import (
    calculate_center_of_mass,
)
from paper_ballistic_scripts.figure_scripts.utils import align_axes, load_mpl_rc

warnings.filterwarnings("ignore", message="invalid value encountered in double_scalars")

load_mpl_rc()


SMALL = 1e-10
LARGE = 1e10


def A_formula(f=1, e=0, nu=0):
    """
    Function to calculate the A factor
    """

    A = ((f**2) * (1 + e) ** 4) / np.power((1 + e * np.cos(nu)), 3)

    return A


@lru_cache
def calculate_ratio_RL_RLegg(mass_accretor, mass_donor, f=1, e=0, nu=0):
    """
    Function from sepinsky 2007 to calculate the ratio between the roche lobe radius(q, f, e, v)/ RL_egg(q)
    """

    A = A_formula(f, e, nu)

    logq = np.log(mass_accretor / mass_donor)
    logA = np.log(A)

    if logq > 0 and logA <= -0.1:
        ratio = (
            1.226
            - 0.21 * A
            - 0.15 * (1 - A) * np.exp((0.25 * A - 0.3) * (logq**1.55))
        )
    elif logq <= 0 and logA <= -0.1:
        ratio = (
            1 + 0.11 * (1 - A) - 0.05 * (1 - A) * np.exp(-((0.5 * (1 + A) + logq) ** 2))
        )
    elif logq <= 0 and -0.1 < logA < 0.2:
        g0 = 0.9978 - 0.1229 * logA - 0.1273 * logA**2
        g1 = 0.001 + 0.02556 * logA
        g2 = 0.0004 + 0.0021 * logA
        ratio = g0 + g1 * logq + g2 * logq**2
    elif logq > 0 and -0.1 < logA < 0.2:
        h0 = 1.0071 - 0.0907 * logA - 0.0495 * logA**2
        h1 = -0.004 - 0.163 * logA - 0.214 * logA**2
        h2 = 0.00022 - 0.0108 * logA - 0.02718 * logA**2
        ratio = h0 + h1 * logq + h2 * logq**2
    elif logq <= 0 and logA >= 0.2:
        i0 = (6.3014 * logA ** (1.3643)) / (
            np.exp(2.3644 * logA ** (0.70748))
            - 1.4413 * np.exp(-0.0000184 * logA ** (-4.5693))
        )
        i1 = logA / (0.0015 * np.exp(8.84 * logA**0.282) + 15.78)
        i2 = (1 + 0.036 * np.exp(8.01 * logA**0.879)) / (
            0.105 * np.exp(7.91 * logA ** (0.879))
        )
        i3 = (0.991) / (
            1.38 * np.exp(-0.035 * logA ** (0.76))
            + 23.0 * np.exp(-2.89 * logA ** (0.76))
        )
        ratio = i0 + i1 * np.exp(-i2 * (logq + i3) ** 2)
    elif logq > 0 and logA >= 0.2:
        j0 = (1.895 * logA**0.837) / (np.exp(1.636 * logA**0.789) - 1)
        j1 = (4.3 * logA**0.98) / (np.exp(2.5 * logA**0.66) + 4.7)
        j2 = (1) / (
            8.8 * np.exp(-2.95 * logA**0.76) + 1.64 * np.exp(-0.03 * logA ** (0.76))
        )
        j3 = (
            0.256 * np.exp(-1.33 * logA**2.9) * (5.5 * np.exp(1.33 * logA**2.9) + 1)
        )

        ratio = j0 + j1 * np.exp(-j2 * logq**j3)

    else:
        raise ValueError("logq and logA not covered")

    return ratio


def L_formula(x, q, f=1, e=0, nu=0):
    """
    Function to find the L1 lagrange point
    """

    A = A_formula(f=f, e=e, nu=nu)

    t1 = q * x / (np.abs(x) ** 3)
    t2 = (x - 1) / (np.abs(x - 1) ** 3)
    t3 = x * (1 + q) * A
    t4 = 1

    return t1 + t2 - t3 + t4


def calc_L1_pos(f, q):
    """
    Function to calculate the L1 lagrange point.
    """

    lower_interval_boundary = 0 + SMALL
    upper_interval_boundary = 1

    root = bisect(
        L_formula, lower_interval_boundary, upper_interval_boundary, args=(q, f)
    )

    L1 = np.array([root, 0, 0])

    return L1


def calc_L2_pos(f, q):
    """
    Function to calculate the L1 lagrange point
    """

    lower_interval_boundary = 1 + SMALL
    upper_interval_boundary = LARGE

    root = bisect(
        L_formula, lower_interval_boundary, upper_interval_boundary, args=(q, f)
    )

    L2 = np.array([root, 0, 0])

    return L2


def calc_L3_pos(f, q):
    """
    Function to calculate the L3 lagrange point
    """

    lower_interval_boundary = -LARGE
    upper_interval_boundary = 0 - SMALL

    root = bisect(
        L_formula, lower_interval_boundary, upper_interval_boundary, args=(q, f)
    )

    L3 = np.array([root, 0, 0])

    return L3


@lru_cache
def calculate_lagrange_points_sepinsky(
    mass_accretor, mass_donor, f=1, e=0, eta=0, separation=1, original=False
):
    """
    Function that handles calculation of all the lagrange points
    """

    massratio = mass_accretor / mass_donor

    # In the original it seems to be flipped so we flip it too
    if not original:
        massratio = mass_donor / mass_accretor

    # Get lagrange points
    L_dict = {
        "L1": calc_L1_pos(f=f, q=massratio),
        "L2": calc_L2_pos(f=f, q=massratio),
        "L3": calc_L3_pos(f=f, q=massratio),
    }

    # Shift all the lagrange points s.t. the system is centered on the center of mass
    if not original:
        # Calculate the center of mass in x
        x_cm = calculate_center_of_mass(m1=mass_donor, m2=mass_accretor, x1=0, x2=1)

        # Shift the system s.t. it is centered on the center of mass. The original L points are measured in reference frame where donor is the center.
        L_dict["L1"][0] = L_dict["L1"][0] - x_cm
        L_dict["L2"][0] = L_dict["L2"][0] - x_cm
        L_dict["L3"][0] = L_dict["L3"][0] - x_cm

    return L_dict


def get_locations_for_given_f(massratios_accretor_donor, f, original):
    """
    FUnction that handles calculation all the Lagrange points
    """

    L1_locations = []
    L2_locations = []
    L3_locations = []

    for massratio_accretor_donor in massratios_accretor_donor:
        # Calculate L points
        lagrange_points = calculate_lagrange_points_sepinsky(
            mass_accretor=massratio_accretor_donor, mass_donor=1, f=f, original=original
        )

        # Store
        L1_locations.append(lagrange_points["L1"])
        L2_locations.append(lagrange_points["L2"])
        L3_locations.append(lagrange_points["L3"])

    # Transform to arrays
    L1_locations = np.array(L1_locations)
    L2_locations = np.array(L2_locations)
    L3_locations = np.array(L3_locations)

    Lagrange_locations = {"L1": L1_locations, "L2": L2_locations, "L3": L3_locations}

    return Lagrange_locations


def plot_sepinsky_with_f_values(massratio_accretor_donor_array, result_dicts, original):
    """
    Function to plot the lagrange point
    """

    ##################
    # Set up figure
    fig = plt.figure(figsize=(25, 25))
    gs = fig.add_gridspec(nrows=3, ncols=9)

    axl2 = fig.add_subplot(gs[0, :])
    axl1 = fig.add_subplot(gs[1, :], sharex=axl2)
    axl3 = fig.add_subplot(gs[2, :], sharex=axl2)

    for result_dict in result_dicts:
        # Plot locations of L points: points
        axl1.plot(
            massratio_accretor_donor_array,
            result_dict["lagrange_points"]["L1"][:, 0],
            "o",
            label="A = {}".format(result_dict["A"]),
        )

        # Plot locations of L points: points
        axl2.plot(
            massratio_accretor_donor_array,
            result_dict["lagrange_points"]["L2"][:, 0],
            "o",
            label="A = {}".format(result_dict["A"]),
        )

        # Plot locations of L points: points
        axl3.plot(
            massratio_accretor_donor_array,
            result_dict["lagrange_points"]["L3"][:, 0],
            "o",
            label="A = {}".format(result_dict["A"]),
        )

    axl2.set_xscale("log")
    axl1.set_xscale("log")
    axl3.set_xscale("log")

    axl2.set_ylabel(r"$X_{L_{2}}$")
    axl3.set_ylabel(r"$X_{L_{3}}$")
    axl1.set_ylabel(r"$X_{L_{1}}$")

    axl1.legend()

    align_axes(fig=fig, axes_list=[axl2, axl1, axl3], which_axis="x")

    if original:
        titletext = "Lagrange point locations centered with frame of reference centered on the donor (as in the original paper)"
        xlabel_text = r"Massratio $q = M_{don}/M_{acc}$"
    else:
        titletext = "Lagrange point locations centered with frame of reference centered on the center of mass"
        xlabel_text = r"Massratio $q = M_{acc}/M_{don}$"
    axl2.set_title(titletext)
    axl3.set_xlabel(xlabel_text)

    plt.setp(axl2.get_xticklabels(), visible=False)
    plt.setp(axl1.get_xticklabels(), visible=False)

    plt.show()


if __name__ == "__main__":
    resolution = 0.01
    bounds = 4
    massratios_accretor_donor = 10 ** np.arange(-bounds, bounds, resolution)
    mass_donor = 1
    original = False

    f064 = get_locations_for_given_f(
        massratios_accretor_donor=massratios_accretor_donor,
        f=np.sqrt(0.64),
        original=original,
    )
    f1 = get_locations_for_given_f(
        massratios_accretor_donor=massratios_accretor_donor, f=1, original=original
    )
    fsqrt5 = get_locations_for_given_f(
        massratios_accretor_donor=massratios_accretor_donor,
        f=np.sqrt(5),
        original=original,
    )
    f5 = get_locations_for_given_f(
        massratios_accretor_donor=massratios_accretor_donor, f=5, original=original
    )
    fqsrt1000 = get_locations_for_given_f(
        massratios_accretor_donor=massratios_accretor_donor,
        f=np.sqrt(1000),
        original=original,
    )
    plot_sepinsky_with_f_values(
        massratios_accretor_donor,
        result_dicts=[
            {"lagrange_points": f064, "A": 0.64},
            {"lagrange_points": f1, "A": 1**2},
            {"lagrange_points": fsqrt5, "A": 5},
            {"lagrange_points": f5, "A": 25},
            {"lagrange_points": fqsrt1000, "A": 1000},
        ],
        original=original,
    )
