"""
Langrange point calculation based on Hubova 19
"""

import numpy as np
from scipy.optimize import newton

####
# L points based on hubova's paper


def L1_formula(x, mu, f):
    """
    Function to find the L1 lagrange point
    """

    t1 = f * x
    t2 = (mu) / ((x - 1 + mu) ** 2)
    t3 = -(1 - mu) / ((x + mu) ** 2)
    return t1 + t2 + t3


def L2_formula(x, mu, f):
    """
    Function to calculate the L2 lagrange point
    """

    t1 = f * x
    t2 = -(mu) / ((x - 1 + mu) ** 2)
    t3 = -(1 - mu) / ((x + mu) ** 2)
    value = t1 + t2 + t3

    return value


def L3_formula(x, mu, f):
    """
    Function to find the L3 lagrange point
    """

    t1 = f * x
    t2 = -(mu) / ((x - 1 + mu) ** 2)
    t3 = (1 - mu) / ((x + mu) ** 2)

    return t1 + t2 + t3


def calc_L1_pos(f, mu):
    """
    Function to calculate the L1 lagrange point
    """

    root = newton(L1_formula, 0, args=(mu, f))
    L1 = np.array([root, 0, 0])

    return L1


def calc_L2_pos(f, mu):
    """
    Function to calculate the L2 lagrange point
    """

    root = newton(L2_formula, 1, args=(mu, f))
    L2 = np.array([root, 0, 0])

    return L2


def calc_L3_pos(f, mu):
    """
    Function to calculate the L2 lagrange point
    """

    root = newton(L3_formula, -1, args=(mu, f))
    L3 = np.array([root, 0, 0])

    return L3


def calculate_lagrange_points_hubova(mass_accretor, mass_donor, separation, f):
    """
    Wrapper function for the lagrange point calculation with the binary_c functions
    """

    mu = (
        mass_accretor / mass_donor
        if mass_accretor < mass_donor
        else mass_donor / mass_accretor
    )

    L1_pos = calc_L1_pos(f, mu) * separation
    L2_pos = calc_L2_pos(f, mu) * separation
    L3_pos = calc_L3_pos(f, mu) * separation

    L_dict = {}
    L_dict["L1"] = L1_pos
    L_dict["L2"] = L2_pos
    L_dict["L3"] = L3_pos

    return L_dict
