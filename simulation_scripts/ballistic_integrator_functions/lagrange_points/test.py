"""
Routines to calculate the lagrange points based on sepinski 2007
"""

import numpy as np
from ballistic_integrator.functions.functions import calc_pos_donor
from david_phd_functions.plotting import custom_mpl_settings
from scipy.optimize import bisect
from sympy import Abs, Symbol, nsolve

custom_mpl_settings.load_mpl_rc()


def L_formula(x, q, f):
    """
    Function to find the L1 lagrange point
    """

    A = f**2

    t1 = q * x / (np.abs(x) ** 3)
    t2 = (x - 1) / (np.abs(x - 1) ** 3)
    t3 = x * (1 + q) * A + 1

    return t1 + t2 + t3


def calc_L1_pos(f, q):
    """
    Function to calculate the L1 lagrange point
    """

    lower_interval_boundary = 0
    upper_interval_boundary = 1

    root = 1 - bisect(
        L_formula, lower_interval_boundary, upper_interval_boundary, args=(q, f)
    )

    L1 = np.array([root, 0, 0])

    return L1


def return_solutions_symbolic_equation_L_formula():
    """
    Function to calculate equation 16 of the two equations to solve for theta and T
    """

    q = 0.5
    f = 1
    x = Symbol("x", real=True)

    A = f**2

    # t1 = q * x / (Abs(x) ** 3)
    # t2 = (x - 1) / (Abs(x - 1) ** 3)
    # t3 = x * (1 + q) * A
    # t4 = 1

    # solveset(
    #     q * x / (Abs(x) ** 3) + (x - 1) / (Abs(x - 1) ** 3) - x * (1 + q) * A + 1,
    #     domain=[0, 1],
    # )

    res = nsolve(
        q * x / (Abs(x) ** 3) + (x - 1) / (Abs(x - 1) ** 3) - x * (1 + q) * A + 1,
        x,
        -0.5,
    )

    # res = solve(t1 + t2 - t3 + t4)

    return res


result = return_solutions_symbolic_equation_L_formula()

print(res)
