"""
Script containing a routine that finds the equipotentials of the roche lobe (at different potential values. i.e. for L1 L2 L3)

system is centered on CM (i.e. x = 0 is location of CM)
"""

import numpy as np


def return_phi(x, y, mu):
    """
    Function that returns the potential value (as described in Hubova+'19')

    quick reminder that everything is in terms of non-dimensionalized units.
    If you want real values then you should scale everything up by:
    TODO: this should be moved to the normal location that takes into account the frame of reference
    """

    phi = (
        -mu / ((x - 1 + mu) ** 2 + y**2) ** 0.5
        - (1 - mu) / ((x + mu) ** 2 + y**2) ** 0.5
        - 0.5 * (x**2 + y**2)
    )

    return phi


def plot_rochelobe_equipotentials_function(
    fig, ax, mass_donor, mass_accretor, separation
):
    """
    Function to plot the contour for the roche lobe
    """

    # System description
    q = mass_accretor / mass_donor

    M2 = 1
    M1 = M2 / q
    mu = M2 / (M1 + M2)
    mu1 = M1 / (M1 + M2)
    a = mu1 + mu

    # Get L1 location and the phi value at that point
    lagrange_points = calculate_lagrange_points(M1, M2, a)

    # Calculate potential values at those points
    phi_l1 = return_phi(lagrange_points["L1"][0], lagrange_points["L1"][1], mu)
    phi_l2 = return_phi(lagrange_points["L2"][0], lagrange_points["L2"][1], mu)
    phi_l3 = return_phi(lagrange_points["L3"][0], lagrange_points["L3"][1], mu)

    # # for ease, choose a lagrange point now
    # L_point = lagrange_points['L1']

    # #
    # theta_arr = np.linspace(0, 2*np.pi, 3600)
    # r_arr = np.linspace(L_point[0]-0.001, 2 * L_point[0], 200)

    # x_results = np.zeros(theta_arr.shape)
    # y_results = np.zeros(theta_arr.shape)
    # phi_results = np.zeros(theta_arr.shape)

    # for i, theta in enumerate(theta_arr):
    #     x_coords = r_arr * np.cos(theta)
    #     y_coords = r_arr * np.sin(theta)

    #     # Take the desired phi_value, and subtract that from the found phi list:
    #     phi_values = return_phi(x_coords, y_coords, mu)
    #     diff = phi_values-phi_l1
    #     idx = np.abs(diff).argmin()

    #     x_results[i] = x_coords[idx]
    #     y_results[i] = y_coords[idx]
    #     phi_results[i] = phi_values[idx]

    # Create a meshgrid for x and y
    xx, yy = np.meshgrid(np.linspace(-2, 2, 1000), np.linspace(-2, 2, 1000))

    # Calculate all the phi values
    phi_values = return_phi(xx, yy, mu)

    # Draw the contours
    CS = ax.contour(phi_values, levels=[phi_l1, phi_l2, phi_l3])

    fmt = {}
    strs = ["L1", "L2", "L3"]
    for l, s in zip(CS.levels, strs):
        fmt[l] = s

    # Label every other level using strings
    ax.clabel(CS, CS.levels, inline=True, fmt=fmt, fontsize=16)

    return fig, ax


if __name__ == "__main__":
    import matplotlib.pyplot as plt

    from paper_ballistic_scripts.figure_scripts.utils import (
        load_mpl_rc,
        show_and_save_plot,
    )

    load_mpl_rc()

    fig, ax = plt.subplots()
    plot_rochelobe_contours(fig, ax, mass_donor=1, mass_accretor=0.5, separation=1)
    plt.show()
