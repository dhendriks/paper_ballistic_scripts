"""
Functions to calculate quantities in the frame of reference centered on the center of the donor star.
"""

import numpy as np

from paper_ballistic_scripts.ballistic_integrator_functions.lagrange_points.lagrange_points_sepinski import (
    calculate_lagrange_points_sepinsky,
)


# Potential
def calculate_potential():
    """
    Function to calculate the roche potential in the frame of reference centered on the center of the donor star.
    """

    raise NotImplementedError


# Ballistic equations
def calculate_dphidx(mu_accretor, x, y):
    """
    Function to calculate the derivative with respect to x of the roche potential in the frame of reference centered on the center of the donor star.
    """

    raise NotImplementedError


def calculate_dphidy(mu_accretor, x, y):
    """
    Function to calculate the derivative with respect to y of the roche potential in the frame of reference centered on the center of the donor star.
    """

    raise NotImplementedError


def calculate_position_accretor(separation):
    """
    Function to calculate the position of the accretor w.r.t center of the donor
    """

    pos_accretor = np.array([1, 0]) * separation

    return pos_accretor


def calculate_position_donor(separation):
    """
    Function to calculate the position of the donor w.r.t center of the donor
    """

    pos_donor = np.array([0, 0]) * separation

    return pos_donor


def calculate_lagrange_points(
    mass_accretor,
    mass_donor,
    separation,
    synchronicity_factor=1,
    eccentricity=0,
    mean_anomaly=0,
):
    """
    Function to calculate the Lagrange points in frame of reference centered on donor using the generalized method of Sepinksy 2007
    """

    lagrange_points = calculate_lagrange_points_sepinsky(
        mass_accretor=mass_accretor,
        mass_donor=mass_donor,
        f=synchronicity_factor,
        e=eccentricity,
        eta=mean_anomaly,
        separation=separation,
        original=True,
    )

    return lagrange_points


# Energy and angular momentum
def calculate_energy(phi, x, xdot, y, ydot):
    """
    Energy in a stationary frame (i think. check this)

    Hubova eq:4
    """

    raise NotImplementedError


def calculate_angular_momentum(x, xdot, y, ydot):
    """
    Angmom in stationary frame (i think. check this)

    hubova eq:5
    """

    raise NotImplementedError


def calculate_jacobi_constant(phi, xdot, ydot):
    """
    Jacobi constant.

    Hubova eq:6
    """

    raise NotImplementedError
