"""
Functions to calculate quantities in the frame of reference centered on the center of mass of the system.

TODO: I want to make a consistent set or arguments that are passed to each of the functions, perhaps i should handle that in the wrapper functions though
TODO: Consider moving to an autograd based setup that calculates the derivatives automatically.
TODO: I can also use sympy for this as its mainly a symbolic analytical thing:
- starting from some base potential we can derive all the equations that are important: derivatives, lagrange points. Easily, in either frame
"""

import numpy as np

from paper_ballistic_scripts.ballistic_integrator_functions.functions import (
    calc_mu_accretor,
    calc_mu_donor,
)
from paper_ballistic_scripts.ballistic_integrator_functions.lagrange_points.lagrange_points_sepinski import (
    calculate_lagrange_points_sepinsky,
)


#################
# Potential and individual parts
def calculate_potential(system_dict, x, y, z):
    """
    Function to calculate the potential of the particle in the frame of reference centered on the center of mass

    Args:
        mu_accretor: reduced mass M_acc/(M_don+M_acc)
        x: x_position of particle
        y: y_position of particle
        z: z_position of particle

    Returns:
        - dimensionless potential value expressed in terms of a^2 w^2
    """

    # Calculate mu accretor
    mu_accretor = calc_mu_accretor(
        mass_accretor=system_dict["mass_accretor"],
        mass_donor=system_dict["mass_donor"],
    )

    # Get donor contribution
    potential_donor = calculate_potential_donor(mu_accretor=mu_accretor, x=x, y=y, z=z)

    # Get accretor contribution
    potential_accretor = calculate_potential_accretor(
        mu_accretor=mu_accretor, x=x, y=y, z=z
    )

    # Get contribution due to corotating frame (coriolis force)
    potential_centrifugal = calculate_potential_centrifugal(
        x=x, y=y, f=system_dict["synchronicity_factor"]
    )

    # Get contribution due to correction term on the coriolis force due to async rotation
    potential_correction = calculate_potential_correction(
        mu_accretor=mu_accretor, x=x, f=system_dict["synchronicity_factor"]
    )

    # Return dictionary with each term
    return {
        "donor": potential_donor,
        "accretor": potential_accretor,
        "centrifugal": potential_centrifugal,
        "correction": potential_correction,
    }


def calculate_potential_donor(mu_accretor, x, y, z):
    """
    Function to calculate the donor's potential contribution
    """

    # Term 2
    term_2_num = -(1 - mu_accretor)
    term_2_den = np.sqrt(
        np.power((x + mu_accretor), 2) + np.power(y, 2) + np.power(z, 2)
    )
    term_2 = term_2_num / term_2_den

    return term_2


def calculate_potential_accretor(mu_accretor, x, y, z):
    """
    Function to calculate the donor's potential contribution

    TODO: allow for the inclusion of quasi-newtonian potential
    """

    # Calculate terms
    term_1_num = -mu_accretor
    term_1_den = np.sqrt(
        np.power((x - 1 + mu_accretor), 2) + np.power(y, 2) + np.power(z, 2)
    )
    term_1 = term_1_num / term_1_den

    return term_1


def calculate_potential_centrifugal(x, y, f):
    """
    Function to get the contribution due to centrifugal force (because we are in a corotating frame)
    """

    # Term 3
    term_3 = -0.5 * (np.power(x, 2) + np.power(y, 2)) * np.power(f, 2)

    return term_3


def calculate_potential_correction(mu_accretor, x, f):
    """
    Function to calculate the contribution of the correction term (see derivations)
    """

    term_4 = (1 - np.power(f, 2)) * mu_accretor * x

    return term_4


#################
# Ballistic equations


###
# dV/dX
def calculate_dphidx(system_dict, x, y):
    """
    Function to calculate the derivative with respect to x of the roche potential in the reference frame centered on the center of mass
    """

    # Calculate mu accretor
    mu_accretor = calc_mu_accretor(
        mass_accretor=system_dict["mass_accretor"],
        mass_donor=system_dict["mass_donor"],
    )

    # Get accretor contribution
    dphidx_accretor = calculate_dphidx_accretor(mu_accretor=mu_accretor, x=x, y=y)

    # Get donor contribution
    dphidx_donor = calculate_dphidx_donor(mu_accretor=mu_accretor, x=x, y=y)

    # Get contribution due to corotating frame (centrifugal force)
    dphidx_centrifugal = calculate_dphidx_centrifugal(
        x=x, f=system_dict["synchronicity_factor"]
    )

    # Get contribution due to correction of synchronicty
    dphidx_correction = calculate_dphidx_correction(
        mu_accretor=mu_accretor, f=system_dict["synchronicity_factor"]
    )

    # Return dictionary with each term
    return {
        "accretor": dphidx_accretor,
        "donor": dphidx_donor,
        "centrifugal": dphidx_centrifugal,
        "correction": dphidx_correction,
    }


def calculate_dphidx_accretor(mu_accretor, x, y):
    """
    Function to calculate the accretor's dphi_dx contribution
    """

    #
    term_1_num = mu_accretor * (x - 1 + mu_accretor)
    term_1_den = ((x - 1 + mu_accretor) ** 2 + y**2) ** (3.0 / 2.0)
    term_1 = term_1_num / term_1_den

    return term_1


def calculate_dphidx_donor(mu_accretor, x, y):
    """
    Function to calculate the donor's dphi_dx contribution
    """

    #
    term_2_num = (x + mu_accretor) * (1 - mu_accretor)
    term_2_den = ((x + mu_accretor) ** 2 + y**2) ** (3.0 / 2.0)
    term_2 = term_2_num / term_2_den

    return term_2


def calculate_dphidx_centrifugal(x, f):
    """
    Function to calculate the coriolis's dphi_dx contribution
    """

    #
    term_3 = -x * np.power(f, 2)

    return term_3


def calculate_dphidx_correction(mu_accretor, f):
    """
    Function to calculate the coriolis's dphi_dx contribution
    """

    #
    term_4 = (1 - np.power(f, 2)) * mu_accretor

    return term_4


# dV/dY
def calculate_dphidy(system_dict, x, y):
    """
    Function to calculate the derivative with respect to y of the roche potential in the reference frame centered on the center of mass
    """

    # Calculate mu accretor
    mu_accretor = calc_mu_accretor(
        mass_accretor=system_dict["mass_accretor"],
        mass_donor=system_dict["mass_donor"],
    )

    # Get accretor contribution
    dphidy_accretor = calculate_dphidy_accretor(mu_accretor=mu_accretor, x=x, y=y)

    # Get donor contribution
    dphidy_donor = calculate_dphidy_donor(mu_accretor=mu_accretor, x=x, y=y)

    # Get contribution due to corotating frame
    dphidy_centrifugal = calculate_dphidy_centrifugal(
        y=y, f=system_dict["synchronicity_factor"]
    )

    # Get contribution due to correction of synchronicty
    dphidy_correction = calculate_dphidy_correction()

    # Return dictionary with each term
    return {
        "accretor": dphidy_accretor,
        "donor": dphidy_donor,
        "centrifugal": dphidy_centrifugal,
        "correction": dphidy_correction,
    }


def calculate_dphidy_accretor(mu_accretor, x, y):
    """
    Function to calculate the accretor's dphi_dy contribution
    """

    #
    term_1_num = mu_accretor * y
    term_1_den = ((x - 1.0 + mu_accretor) ** 2 + y**2) ** (3.0 / 2.0)
    term_1 = term_1_num / term_1_den

    return term_1


def calculate_dphidy_donor(mu_accretor, x, y):
    """
    Function to calculate the donor's dphi_dy contribution
    """

    #
    term_2_num = (1.0 - mu_accretor) * y
    term_2_den = ((x + mu_accretor) ** 2 + y**2) ** (3.0 / 2.0)
    term_2 = term_2_num / term_2_den

    return term_2


def calculate_dphidy_centrifugal(y, f):
    """
    Function to calculate the coriolis's dphi_dy contribution
    """

    #
    term_3 = -y * np.power(f, 2)

    return term_3


def calculate_dphidy_correction():
    """
    Function to calculate the coriolis's dphi_dx contribution
    """

    #
    term_4 = 0

    return term_4


##############
# Positions
def calculate_position_accretor(mass_accretor, mass_donor, separation):
    """
    Function to calculate the position of the accretor w.r.t center of mass
    """

    mu_donor = calc_mu_donor(mass_accretor=mass_accretor, mass_donor=mass_donor)
    pos_accretor = np.array([mu_donor, 0]) * separation

    return pos_accretor


def calculate_position_donor(mass_accretor, mass_donor, separation):
    """
    Function to calculate the position of the donor w.r.t center of mass
    """

    mu_accretor = calc_mu_accretor(mass_accretor=mass_accretor, mass_donor=mass_donor)
    pos_donor = np.array([-mu_accretor, 0]) * separation

    return pos_donor


# Lagrange points
def calculate_lagrange_points(
    mass_accretor,
    mass_donor,
    separation,
    synchronicity_factor=1,
    eccentricity=0,
    mean_anomaly=0,
):
    """
    Function to calculate the Lagrange points in frame of reference centered on the center of mass of the system using the generalized method of Sepinksy 2007
    """

    lagrange_points = calculate_lagrange_points_sepinsky(
        mass_accretor=mass_accretor,
        mass_donor=mass_donor,
        f=synchronicity_factor,
        e=eccentricity,
        eta=mean_anomaly,
        separation=separation,
        original=False,
    )

    return lagrange_points


###############
# TODO: we cannot calculate the equations like this.


# Energy and angular momentum
def calculate_energy(phi, x, xdot, y, ydot):
    """
    Function to calculate the energy of the particle in the stationary (observer) frame, calculated with quantities defined in the frame of reference centered on the center of mass

    Based on equation 4 in Hubova 2019
    """

    return (
        phi
        + 0.5 * (np.power(xdot, 2) + np.power(ydot, 2))
        + np.power(x, 2)
        + np.power(y, 2)
        + x * ydot
        - xdot * y
    )


def calculate_angular_momentum(x, xdot, y, ydot):
    """
    Function to calculate the angular momentum of the particle in the stationary (observer) frame, calculated with quantities defined in the frame of reference centered on the center of mass

    Based on equation 5 in Hubova 2019
    """

    return np.power(x, 2) + np.power(y, 2) + x * ydot - xdot * y


def calculate_jacobi_constant(phi, xdot, ydot):
    """
    Function to calculate the jacobi constant of the particle in the stationary (observer) frame, calculated with quantities defined in the frame of reference centered on the center of mass

    Based on equation 6 in Hubova 2019
    """

    return phi + 0.5 * (np.power(xdot, 2) + np.power(ydot, 2))
