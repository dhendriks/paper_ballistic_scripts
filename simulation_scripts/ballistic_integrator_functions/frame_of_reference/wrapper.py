"""
Wrapper functions that handle the choice of frame of reference.

There are two options for the frame of reference:
- "center_of_donor": Frame of reference centered on the center of the donor
- "center_of_mass": Frame of reference centered on the center of mass of the system

TODO: make sure that the z is propagated throughout the code
TODO: make sure we propagate the settings throughout the code
"""

# Import functions from Center of Donor reference frame
from paper_ballistic_scripts.ballistic_integrator_functions.frame_of_reference.center_of_donor import (
    calculate_angular_momentum as calculate_angular_momentum_center_of_donor,
)
from paper_ballistic_scripts.ballistic_integrator_functions.frame_of_reference.center_of_donor import (
    calculate_dphidx as calculate_dphi_dx_center_of_donor,
)
from paper_ballistic_scripts.ballistic_integrator_functions.frame_of_reference.center_of_donor import (
    calculate_dphidy as calculate_dphi_dy_center_of_donor,
)
from paper_ballistic_scripts.ballistic_integrator_functions.frame_of_reference.center_of_donor import (
    calculate_energy as calculate_energy_center_of_donor,
)
from paper_ballistic_scripts.ballistic_integrator_functions.frame_of_reference.center_of_donor import (
    calculate_jacobi_constant as calculate_jacobi_constant_center_of_donor,
)
from paper_ballistic_scripts.ballistic_integrator_functions.frame_of_reference.center_of_donor import (
    calculate_lagrange_points as calculate_lagrange_points_center_of_donor,
)
from paper_ballistic_scripts.ballistic_integrator_functions.frame_of_reference.center_of_donor import (
    calculate_position_accretor as calculate_position_accretor_center_of_donor,
)
from paper_ballistic_scripts.ballistic_integrator_functions.frame_of_reference.center_of_donor import (
    calculate_position_donor as calculate_position_donor_center_of_donor,
)
from paper_ballistic_scripts.ballistic_integrator_functions.frame_of_reference.center_of_donor import (
    calculate_potential as calculate_potential_center_of_donor,  # Potential; positions; lagrange points; energy; angular momentum; jacobi constant
)

# Import functions from Center of Mass reference frame
from paper_ballistic_scripts.ballistic_integrator_functions.frame_of_reference.center_of_mass import (
    calculate_angular_momentum as calculate_angular_momentum_center_of_mass,
)
from paper_ballistic_scripts.ballistic_integrator_functions.frame_of_reference.center_of_mass import (
    calculate_dphidx as calculate_dphi_dx_center_of_mass,
)
from paper_ballistic_scripts.ballistic_integrator_functions.frame_of_reference.center_of_mass import (
    calculate_dphidy as calculate_dphi_dy_center_of_mass,
)
from paper_ballistic_scripts.ballistic_integrator_functions.frame_of_reference.center_of_mass import (
    calculate_energy as calculate_energy_center_of_mass,
)
from paper_ballistic_scripts.ballistic_integrator_functions.frame_of_reference.center_of_mass import (
    calculate_jacobi_constant as calculate_jacobi_constant_center_of_mass,
)
from paper_ballistic_scripts.ballistic_integrator_functions.frame_of_reference.center_of_mass import (
    calculate_lagrange_points as calculate_lagrange_points_center_of_mass,
)
from paper_ballistic_scripts.ballistic_integrator_functions.frame_of_reference.center_of_mass import (
    calculate_position_accretor as calculate_position_accretor_center_of_mass,
)
from paper_ballistic_scripts.ballistic_integrator_functions.frame_of_reference.center_of_mass import (
    calculate_position_donor as calculate_position_donor_center_of_mass,
)
from paper_ballistic_scripts.ballistic_integrator_functions.frame_of_reference.center_of_mass import (
    calculate_potential as calculate_potential_center_of_mass,  # Potential functions; positions; lagrange points; energy; angular momentum; jacobi constant
)
from paper_ballistic_scripts.ballistic_integrator_functions.functions import dict_sum


######
# Potential
def calculate_potential(
    settings,
    system_dict,
    frame_of_reference,
    x_pos,
    y_pos,
    z_pos=0,
):
    """
    Wrapper function that handles calculating the potential value dict and returns its sum

    Args:
        system_dict: dictionary containing all the system properties
        x_pos, y_pos: position of particle
        frame_of_reference: choice of reference frame
        settings: global settings used for multiplying the terms

    Returns:
        dictionary of the terms of the potential in the given frame of reference
    """

    potential_dict = calculate_potential_dict(
        settings=settings,
        system_dict={
            **system_dict,
            "synchronicity_factor": system_dict["synchronicity_factor"]
            if settings["include_asynchronicity_donor_during_integration"]
            else 1,
        },
        frame_of_reference=frame_of_reference,
        x_pos=x_pos,
        y_pos=y_pos,
        z_pos=z_pos,
    )

    return dict_sum(potential_dict)


def calculate_potential_dict(
    settings,
    system_dict,
    frame_of_reference,
    x_pos,
    y_pos,
    z_pos,
):
    """
    Wrapper function to calculate the potential value of the particle in the chosen frame of reference

    Args:
        system_dict: dictionary containing all the system properties
        x_pos, y_pos: position of particle
        frame_of_reference: choice of reference frame
        settings: global settings used for multiplying the terms

    Returns:
        dictionary of the terms of the potential in the given frame of reference
    """

    # Get dictionary of terms
    if frame_of_reference == "center_of_mass":
        potential_dict = calculate_potential_center_of_mass(
            system_dict=system_dict,
            x=x_pos,
            y=y_pos,
            z=z_pos,
        )
    elif frame_of_reference == "center_of_donor":
        potential_dict = calculate_potential_center_of_donor()
    else:
        raise ValueError("Choice of frame of reference unknown")

    # Apply multipliers
    if settings is not None:
        potential_dict["donor"] *= settings.get("potential_multiplier_donor", 1)
        potential_dict["accretor"] *= settings.get("potential_multiplier_accretor", 1)
        potential_dict["centrifugal"] *= settings.get(
            "potential_multiplier_centrifugal", 1
        )
        potential_dict["correction"] *= settings.get(
            "potential_multiplier_correction", 1
        )

    return potential_dict


#############
# Derivatives
def calculate_dphi_dx(
    system_dict, frame_of_reference, x_pos, y_pos, z_pos=0, settings=None
):
    """
    Wrapper function to analytically calculate the derivative of the potential with respect to x value dict and returns its sum

    Args:
        system_dict: dictionary containing all the system properties
        x_pos, y_pos: position of particle
        frame_of_reference: choice of reference frame
        settings: global settings used for multiplying the terms

    Returns:
        dictionary of dphi_dx terms
    """

    dphi_dx_dict = calculate_dphi_dx_dict(
        system_dict={
            **system_dict,
            "synchronicity_factor": system_dict["synchronicity_factor"]
            if settings["include_asynchronicity_donor_during_integration"]
            else 1,
        },
        frame_of_reference=frame_of_reference,
        x_pos=x_pos,
        y_pos=y_pos,
        settings=settings,
    )

    return dict_sum(dphi_dx_dict)


def calculate_dphi_dx_dict(
    system_dict, frame_of_reference, x_pos, y_pos, settings=None
):
    """
    Wrapper function to analytically calculate the derivative of the potential with respect to x

    Args:
        system_dict: dictionary containing all the system properties
        x_pos, y_pos: position of particle
        frame_of_reference: choice of reference frame
        settings: global settings used for multiplying the terms

    Returns:
        dictionary of dphi_dx terms
    """

    # Get the derivative dictionary with all the
    if frame_of_reference == "center_of_mass":
        dphi_dx_dict = calculate_dphi_dx_center_of_mass(
            system_dict=system_dict,
            x=x_pos,
            y=y_pos,
        )
    elif frame_of_reference == "center_of_donor":
        dphi_dx_dict = calculate_dphi_dx_center_of_donor()
    else:
        raise ValueError("Choice of frame of reference unknown")

    # Apply multipliers
    if settings is not None:
        dphi_dx_dict["donor"] *= settings.get("potential_multiplier_donor", 1)
        dphi_dx_dict["accretor"] *= settings.get("potential_multiplier_accretor", 1)
        dphi_dx_dict["centrifugal"] *= settings.get(
            "potential_multiplier_centrifugal", 1
        )
        dphi_dx_dict["correction"] *= settings.get("potential_multiplier_correction", 1)

    return dphi_dx_dict


def calculate_dphi_dy(system_dict, x_pos, y_pos, frame_of_reference, settings=None):
    """
    Wrapper function to analytically calculate the derivative of the potential with respect to y value dict and returns its sum

    Args:
        system_dict: dictionary containing all the system properties
        x_pos, y_pos: position of particle
        frame_of_reference: choice of reference frame
        settings: global settings used for multiplying the terms

    Returns:
        dictionary of dphi_dy terms
    """

    dphi_dy_dict = calculate_dphi_dy_dict(
        system_dict={
            **system_dict,
            "synchronicity_factor": system_dict["synchronicity_factor"]
            if settings["include_asynchronicity_donor_during_integration"]
            else 1,
        },
        x_pos=x_pos,
        y_pos=y_pos,
        frame_of_reference=frame_of_reference,
        settings=settings,
    )

    return dict_sum(dphi_dy_dict)


def calculate_dphi_dy_dict(
    system_dict, x_pos, y_pos, frame_of_reference, settings=None
):
    """
    Wrapper function to analytically calculate the derivative of the potential with respect to y

    Args:
        system_dict: dictionary containing all the system properties
        x_pos, y_pos: position of particle
        frame_of_reference: choice of reference frame
        settings: global settings used for multiplying the terms

    Returns:
        dictionary of dphi_dy terms
    """

    # Get the derivative dictionary with all the terms
    if frame_of_reference == "center_of_mass":
        dphi_dy_dict = calculate_dphi_dy_center_of_mass(
            system_dict=system_dict, x=x_pos, y=y_pos
        )
    elif frame_of_reference == "center_of_donor":
        dphi_dy_dict = calculate_dphi_dy_center_of_donor()
    else:
        raise ValueError("Choice of frame of reference unknown")

    # Apply multipliers
    if settings is not None:
        dphi_dy_dict["donor"] *= settings.get("potential_multiplier_donor", 1)
        dphi_dy_dict["accretor"] *= settings.get("potential_multiplier_accretor", 1)
        dphi_dy_dict["centrifugal"] *= settings.get(
            "potential_multiplier_centrifugal", 1
        )
        dphi_dy_dict["correction"] *= settings.get("potential_multiplier_correction", 1)

    return dphi_dy_dict


#####
# Energy
def calculate_energy(
    system_dict, x_pos, y_pos, x_dot, y_dot, frame_of_reference, settings, z_pos=0
):
    """
    Wrapper function to handle the calculation of the energy of the particle, as calculated in an inertial reference frame (not in the co-rotating frame)

    Args:
        system_dict: dictionary containing all the system properties
        frame_of_reference: choice of reference frame
        x_pos, y_pos: position of particle
        x_dot, y_dot: velocity of the particle

    Returns:
        Value of the energy of the particle
    """

    if frame_of_reference == "center_of_mass":
        potential = calculate_potential(
            settings=settings,
            system_dict={
                **system_dict,
                "synchronicity_factor": system_dict["synchronicity_factor"]
                if settings["include_asynchronicity_donor_during_integration"]
                else 1,
            },
            frame_of_reference=frame_of_reference,
            x_pos=x_pos,
            y_pos=y_pos,
            z_pos=z_pos,
        )
        energy = calculate_energy_center_of_mass(
            phi=potential, x=x_pos, xdot=x_dot, y=y_pos, ydot=y_dot
        )
    elif frame_of_reference == "center_of_donor":
        energy = calculate_energy_center_of_donor()
    else:
        raise ValueError("Choice of frame of reference unknown")

    return energy


#####
# Angular momentum
def calculate_angular_momentum(
    system_dict, x_pos, y_pos, x_dot, y_dot, frame_of_reference
):
    """
    Wrapper function to calculate the angular momentum of the particle, as calculated in an inertial reference frame (not in the co-rotating frame)

    Args:
        system_dict: dictionary containing all the system properties
        frame_of_reference: choice of reference frame

    Returns:
        Value of the angular momentum of the particle
    """

    if frame_of_reference == "center_of_mass":
        angular_momentum = calculate_angular_momentum_center_of_mass(
            x=x_pos, xdot=x_dot, y=y_pos, ydot=y_dot
        )
    elif frame_of_reference == "center_of_donor":
        angular_momentum = calculate_angular_momentum_center_of_donor()
    else:
        raise ValueError("Choice of frame of reference unknown")

    return angular_momentum


#####
# Jacobi constant
def calculate_jacobi_constant(
    system_dict, x_pos, y_pos, x_dot, y_dot, frame_of_reference, settings, z_pos=0
):
    """
    Wrapper function to calculate the jacobi constant of the particle, as calculated in an inertial reference frame (not in the co-rotating frame)

    Args:
        system_dict: dictionary containing all the system properties
        frame_of_reference: choice of reference frame

    Returns:
        value of the jacobi constant of the particle
    """

    # calculate energy
    energy = calculate_energy(
        system_dict=system_dict,
        x_pos=x_pos,
        y_pos=y_pos,
        x_dot=x_dot,
        y_dot=y_dot,
        frame_of_reference=frame_of_reference,
        settings=settings,
    )

    # calculate angular momentum
    angular_momentum = calculate_angular_momentum(
        system_dict=system_dict,
        x_pos=x_pos,
        y_pos=y_pos,
        x_dot=x_dot,
        y_dot=y_dot,
        frame_of_reference=frame_of_reference,
    )

    # calculate jacobi constant:
    jacobi_constant = energy - angular_momentum

    return jacobi_constant

    # if frame_of_reference == "center_of_mass":
    #     potential = calculate_potential(
    #         settings=settings,
    #         system_dict=system_dict,
    #         frame_of_reference=frame_of_reference,
    #         x_pos=x_pos,
    #         y_pos=y_pos,
    #         z_pos=z_pos,
    #     )
    #     jacobi_constant = calculate_jacobi_constant_center_of_mass(
    #         phi=potential, xdot=x_dot, ydot=y_dot
    #     )
    # elif frame_of_reference == "center_of_donor":
    #     jacobi_constant = calculate_jacobi_constant_center_of_donor()
    # else:
    #     raise ValueError("Choice of frame of reference unknown")

    # return jacobi_constant


######
# Positions
def calculate_position_accretor(system_dict, frame_of_reference):
    """
    Wrapper function to calculate the position of the accretor

    Args:
        system_dict: dictionary containing all the system properties
        frame_of_reference: choice of reference frame

    Returns:
        Array with location of accretor
    """

    if frame_of_reference == "center_of_mass":
        position_accretor = calculate_position_accretor_center_of_mass(
            mass_accretor=system_dict["mass_accretor"],
            mass_donor=system_dict["mass_donor"],
            separation=system_dict["separation"],
        )
    elif frame_of_reference == "center_of_donor":
        position_accretor = calculate_position_accretor_center_of_donor(
            separation=system_dict["separation"]
        )
    else:
        raise ValueError("Choice of frame of reference unknown")

    return position_accretor


def calculate_position_donor(system_dict, frame_of_reference):
    """
    Wrapper function to calculate the position of the donor

    Args:
        system_dict: dictionary containing all the system properties
        frame_of_reference: choice of reference frame

    Returns:
        Array with location of donor
    """

    if frame_of_reference == "center_of_mass":
        position_donor = calculate_position_donor_center_of_mass(
            mass_accretor=system_dict["mass_accretor"],
            mass_donor=system_dict["mass_donor"],
            separation=system_dict["separation"],
        )
    elif frame_of_reference == "center_of_donor":
        position_donor = calculate_position_donor_center_of_donor(
            separation=system_dict["separation"]
        )
    else:
        raise ValueError("Choice of frame of reference unknown")

    return position_donor


######
# Lagrange points
def calculate_lagrange_points(
    system_dict,
    frame_of_reference,
):
    """
    Wrapper function to calculate the lagrange points of the system

    Args:
        system_dict: dictionary containing all the system properties
        frame_of_reference: choice of reference frame

    Returns:
        A dictionary containing the Lagrange points
    """

    if frame_of_reference == "center_of_mass":
        lagrange_points = calculate_lagrange_points_center_of_mass(
            mass_accretor=system_dict["mass_accretor"],
            mass_donor=system_dict["mass_donor"],
            separation=system_dict["separation"],
            eccentricity=system_dict["eccentricity"],
            mean_anomaly=system_dict["mean_anomaly"],
            synchronicity_factor=system_dict["synchronicity_factor"],
        )
    elif frame_of_reference == "center_of_donor":
        lagrange_points = calculate_lagrange_points_center_of_donor(
            mass_accretor=system_dict["mass_accretor"],
            mass_donor=system_dict["mass_donor"],
            separation=system_dict["separation"],
            eccentricity=system_dict["eccentricity"],
            mean_anomaly=system_dict["mean_anomaly"],
            synchronicity_factor=system_dict["synchronicity_factor"],
        )
    else:
        raise ValueError("Choice of frame of reference unknown")

    return lagrange_points
