"""
File containing some global settings for this project
"""

from david_phd_functions.binaryc.personal_defaults import personal_defaults

#
project_specific_bse_settings = {
    "accretion_limit_thermal_multiplier": 10,  # Use the multiplier that was used in Hurley as default
    "save_pre_events_stardata": 1,  # To catch pre-event stuff
    "minimum_timestep": 1e-8,  # This is required by the new Hurley winds
    "BH_prescription": "BH_FRYER12_DELAYED",  # Discussed that the delayed would probably be more appropriate
    "PPISN_prescription": "PPISN_HENDRIKS23",  # On default we should use out _new_ fits
    "wind_mass_loss": "WIND_ALGORITHM_HENDRIKS_2022",  # This is Schneider but using the hurley LBV again
    "lambda_ce": "LAMBDA_CE_DEWI_TAURIS",  # Use the dewi & tauris as the default for us
    "accretion_limit_thermal_multiplier": 10,  # Use the multiplier that was used in Hurley as default
    # "multiplicity": 2,  # Multiplicity
    "david_ppisn_logging": 1,  # Enable the PPISN logging
    # # update the q-crit values
    # "qcrit_LMMS": "QCRIT_GE2020",
    # "qcrit_MS": "QCRIT_GE2020",
    # "qcrit_HG": "QCRIT_GE2020",
    # "qcrit_GB": "QCRIT_GE2020",
    # "qcrit_EAGB": "QCRIT_GE2020",
    # "qcrit_TPAGB": "QCRIT_GE2020",
    # "qcrit_degenerate_LMMS": "QCRIT_GE2020",
    # "qcrit_degenerate_MS": "QCRIT_GE2020",
    # "qcrit_degenerate_HG": "QCRIT_GE2020",
    # "qcrit_degenerate_GB": "QCRIT_GE2020",
    # "qcrit_degenerate_EAGB": "QCRIT_GE2020",
    # "qcrit_degenerate_TPAGB": "QCRIT_GE2020",
}


#
project_specific_grid_settings = {
    # binarycpython control
    "max_queue_size": 20000,  # Allow the queue to be filled alot
    "failed_systems_threshold": 200000,  # Log as many failed systems as we can
}

#
project_specific_settings = {
    **personal_defaults,  # Put the personal settings in first, then override
    **project_specific_bse_settings,
    **project_specific_grid_settings,
}


#
variation_name_dicts = {
    "overspin_algorithm": {0: "BSE", 1: "OVERSPIN_MASSLOSS"},
    "distributions": {
        "normal": "SANA",
        "ms": "MOE_DISTEFANO",
    },
}
