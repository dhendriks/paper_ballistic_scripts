"""
Function that runs the commandsto generate the data and the plots
"""

import os

from binarycpython import Population
from david_phd_functions.backup_functions.functions import backup_if_exists
from david_phd_functions.binaryc.personal_defaults import personal_defaults
from david_phd_functions.repo_info.functions import get_git_info_and_time
from event_based_logging_functions.functions import split_event_types_to_files
from grav_waves.run_population_scripts.functions import combine_resultfiles
from RLOF.run_populations.ensemble_population_and_rlof_episodes.functions import (
    add_distribution_grid_variables,
    handle_ensemble_results,
)
from RLOF.run_populations.ensemble_population_and_rlof_episodes.parse_function import (
    parse_function,
)
from RLOF.settings import project_specific_settings, variation_name_dicts


def generate_simname(simname_base, variation_dict):
    """
    Function to generate the simname
    """

    # Add project and year
    simname = simname_base + "_{}".format("RLOF_2023")

    ###############
    # custom names

    # Add distribution_name
    simname = simname + "_{}".format(
        variation_name_dicts["distributions"][variation_dict["distributions"]]
    )

    ###############
    # Macros

    # Add variation naming
    if "overspin_algorithm" in variation_dict:
        # # Add distribution_name
        # simname += "_overspin_algorithm_{}".format(
        #     variation_name_dicts["overspin_algorithm"][
        #         variation_dict["overspin_algorithm"]
        #     ]
        # )

        simname += "_overspin_algorithm_{}".format(variation_dict["overspin_algorithm"])

    #
    if "RLOF_rmin_prescription" in variation_dict:
        # # Add distribution_name
        # simname += (
        #     "_RLOF_rmin_prescription_{}".format(
        #         variation_name_dicts["RLOF_rmin_prescription"][
        #             variation_dict["RLOF_rmin_prescription"]
        #         ]
        #     )
        #     if variation_dict["RLOF_rmin_prescription"] != 0
        #     else ""
        # )

        simname += "_RLOF_rmin_prescription_{}".format(
            variation_dict["RLOF_rmin_prescription"]
        )

    if "RLOF_rcirc_prescription" in variation_dict:
        # # Add distribution_name
        # simname += (
        #     "_RLOF_rcirc_prescription_{}".format(
        #         variation_name_dicts["RLOF_rcirc_prescription"][
        #             variation_dict["RLOF_rcirc_prescription"]
        #         ]
        #     )
        #     if variation_dict["RLOF_rcirc_prescription"] != 0
        #     else ""
        # )

        simname += "_RLOF_rcirc_prescription_{}".format(
            variation_dict["RLOF_rcirc_prescription"]
        )

    ###############
    # numbers
    if "RLOF_enable_stream_integration_project" in variation_dict:
        # Add distribution_name
        simname += (
            "_RLOF_enable_stream_integration_project_{}".format(
                variation_dict["RLOF_enable_stream_integration_project"]
            )
            # if variation_dict["RLOF_enable_stream_integration_project"] != 0
            # else ""
        )

    if "tidal_strength_factor" in variation_dict:
        # Add distribution_name
        simname += (
            "_tidal_strength_factor_{}".format(variation_dict["tidal_strength_factor"])
            if variation_dict["tidal_strength_factor"] != 0
            else ""
        )

    return simname


def run_all(
    run_populations,
    simname_base,
    backup_if_data_exists,
    generate_plots,
    metallicity_values,
    local_population_settings,
    root_result_dir,
    resolution,
    variation_dict,
    population_postprocessing_function=None,
    simulation_postprocessing_function=None,
    MS_population_range=None,
    RLOF_episode_logging=True,
    inflate_ensembles=True,
    remove_process_files=False,
):
    """
    Function to run the populations and run the plots
    """

    ######
    # Set the simname
    simname = generate_simname(simname_base=simname_base, variation_dict=variation_dict)
    simname_dir = os.path.join(root_result_dir, simname)

    ################
    # Run the populations
    if run_populations:
        # Check if the directory of the current simulation exists. If it does, then delete the old dir
        if backup_if_data_exists:
            backup_if_exists(simname_dir, remove_old_directory_after_backup=True)

        # Create directory
        os.makedirs(simname_dir, exist_ok=True)

        # Git info:
        git_info_dict = get_git_info_and_time()

        #######
        ## Make population and set value
        population_object = Population(verbosity=1)

        ranges = population_object.moe_distefano_options_defaults_dict["ranges"]

        # Load project defaults
        population_object.set(**project_specific_settings)

        # Load local defaults
        population_object.set(**local_population_settings)

        # add gitinfo
        population_object.set(binary_c_python_scripts_git_info=git_info_dict)

        # Set variation dict values
        if "RLOF_enable_stream_integration_project" in variation_dict:
            population_object.set(
                RLOF_enable_stream_integration_project=variation_dict[
                    "RLOF_enable_stream_integration_project"
                ]
            )
        if "RLOF_rmin_prescription" in variation_dict:
            population_object.set(
                RLOF_rmin_prescription=variation_dict["RLOF_rmin_prescription"]
            )
        if "RLOF_rcirc_prescription" in variation_dict:
            population_object.set(
                RLOF_rcirc_prescription=variation_dict["RLOF_rcirc_prescription"]
            )
        if "overspin_algorithm" in variation_dict:
            population_object.set(
                overspin_algorithm=variation_dict["overspin_algorithm"]
            )
        if "tidal_strength_factor" in variation_dict:
            population_object.set(
                tidal_strength_factor=variation_dict["tidal_strength_factor"]
            )

        #
        if RLOF_episode_logging:
            population_object.set(parse_function=parse_function)

        ######
        # Set distributions
        population_object = add_distribution_grid_variables(
            population_object,
            variation_dict,
            resolution,
            MS_population_range=MS_population_range,
        )

        # Loop over metallicities
        for metallicity in metallicity_values:
            population_object.set(
                metallicity=metallicity,
                data_dir=os.path.join(
                    simname_dir, "population_results", "Z{}".format(metallicity)
                ),
            )

            # create local tmp_dir
            population_object.set(
                tmp_dir=os.path.join(
                    population_object.custom_options["data_dir"], "local_tmp_dir"
                ),
                event_based_logging_output_directory=os.path.join(
                    population_object.custom_options["data_dir"], "events"
                ),
            )
            population_object.set(
                log_args_dir=os.path.join(
                    population_object.population_options["tmp_dir"], "current_system"
                )
            )
            population_object.set(
                log_failed_systems_dir=os.path.join(
                    population_object.population_options["tmp_dir"], "failed_systems"
                )
            )

            os.makedirs(population_object.population_options["tmp_dir"], exist_ok=True)
            os.makedirs(
                population_object.population_options["log_args_dir"], exist_ok=True
            )
            os.makedirs(
                population_object.population_options["log_failed_systems_dir"],
                exist_ok=True,
            )

            # Export settings:
            population_object.export_all_info(use_datadir=True)

            # Evolve grid
            population_object.evolve()

            # Handle the writing of the ensemble results
            population_object = handle_ensemble_results(
                population_object,
                simname,
                metallicity,
                inflate_ensembles=inflate_ensembles,
            )

            # # Combine and split event types
            # combine_resultfiles(
            #     population_object.custom_options["data_dir"],
            #     "events",
            #     "total_events.dat",
            #     check_duplicates_and_all_present=False,
            #     remove_individual_files=remove_process_files,
            # )
            # split_event_types_to_files(
            #     input_file=os.path.join(
            #         population_object.custom_options["data_dir"],
            #         "total_events.dat",
            #     ),
            #     remove_original_file=remove_process_files,
            # )

            # Clean the internals so we can run it again safely
            population_object.clean()

            # Post-process ensemble results
            if population_postprocessing_function:
                population_postprocessing_function(
                    local_population_settings, population_object
                )

    ################
    # Post-sim hook
    if population_postprocessing_function:
        population_postprocessing_function(local_population_settings, population_object)

    ################
    # Plot the result
    if generate_plots:
        raise ValueError("Currenly not an option to automatically generate the plots")

        # try:
        #     population_datasets_sana = generate_dataset_for_simname(simname_dir)
        #     general_plot_routine(
        #         sim_dict=population_datasets_sana,
        #         global_query_dict={},
        #         output_dir=os.path.join(simname_dir, 'plots/general/'),
        #         verbose=1,
        #         testing=False
        #     )
        # except Exception as e:
        #     print(e.args)
        #     print(e.with_traceback(e.__traceback__))
