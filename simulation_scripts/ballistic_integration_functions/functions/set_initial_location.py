"""
Script containing function to set the initial location of the particle
"""

import numpy as np
from ballistic_integration_code.scripts.L1_stream.functions.calculate_position_offset_L1_tiny import (
    calculate_position_offset_L1_tiny,
)
from ballistic_integrator.functions.frame_of_reference.wrapper import (
    calculate_lagrange_points,
)


def set_initial_location(settings, L1_area_offset=0):
    """
    Function to set the initial locaton of the particle
    """

    # Get base position
    settings["L_points"] = calculate_lagrange_points(
        system_dict={
            **settings["system_dict"],
            "synchronicity_factor": settings["system_dict"]["synchronicity_factor"]
            if settings["include_asynchronicity_donor_for_lagrange_points"]
            else 1,
        },
        frame_of_reference=settings["frame_of_reference"],
    )
    settings["initial_position_base"] = np.array(settings["L_points"]["L1"][:2])

    # Set particle L_points
    settings["L_points_particle"] = calculate_lagrange_points(
        system_dict={
            **settings["system_dict"],
            "synchronicity_factor": 1,
        },
        frame_of_reference=settings["frame_of_reference"],
    )
    settings["L_points_donor"] = calculate_lagrange_points(
        system_dict={
            **settings["system_dict"],
            "synchronicity_factor": settings["system_dict"]["synchronicity_factor"],
        },
        frame_of_reference=settings["frame_of_reference"],
    )

    # Calculate offset based on tiny shift from L1
    settings["position_offset_L1_tiny"] = calculate_position_offset_L1_tiny(
        system_dict={
            **settings["system_dict"],
            "synchronicity_factor": settings["system_dict"]["synchronicity_factor"]
            if settings["include_asynchronicity_donor_for_lagrange_points"]
            else 1,
        },
        frame_of_reference=settings["frame_of_reference"],
        fraction_sep=settings["fraction_offset"],
    )

    # Combine the position offsets
    settings["initial_position_offset"] = settings["position_offset_L1_tiny"]

    if L1_area_offset:
        # Set offset based on L1 sampling: this is only in y since we have the line of centers on X axis
        settings["position_offset_L1_area"] = np.array([0, L1_area_offset])

        settings["initial_position_offset"] += settings["position_offset_L1_area"]

    # Calculate initial position
    settings["initial_position"] = (
        settings["initial_position_base"] + settings["initial_position_offset"]
    )

    return settings
