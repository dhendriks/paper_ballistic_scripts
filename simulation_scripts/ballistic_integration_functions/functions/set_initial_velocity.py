"""
Function to set the initial velocity
"""

import numpy as np
from ballistic_integration_code.scripts.L1_stream.functions.calculate_velocity_offset_L1_non_synchronous import (
    calculate_velocity_offset_L1_non_synchronous,
)
from ballistic_integration_code.scripts.L1_stream.functions.calculate_velocity_offset_L1_soundspeed import (
    calculate_velocity_offset_L1_soundspeed,
)


def set_initial_velocity(settings):
    """
    Function to set the initial velocity
    """

    # Velocity offset based on the synchronicity rate
    settings[
        "initial_velocity_offset_non_synchronous"
    ] = calculate_velocity_offset_L1_non_synchronous(
        settings=settings,
        system_dict=settings["system_dict"],
        frame_of_reference=settings["frame_of_reference"],
    )

    # Velocity offset based on initial velocity of material at L1
    settings[
        "initial_velocity_offset_soundspeed"
    ] = calculate_velocity_offset_L1_soundspeed(settings)

    # Combine the velocity offsets
    settings["initial_velocity_offset"] = (
        settings["initial_velocity_offset_non_synchronous"]
        + settings["initial_velocity_offset_soundspeed"]
    )

    # Set initial velocity
    settings["initial_velocity"] = np.array(settings["initial_velocity_offset"])

    return settings
