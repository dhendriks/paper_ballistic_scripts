"""
Example of multiprocessing via a queue based on functions

setproctitle is used for the proces title

TODO: perhaps make this a class based approach, easier to pass around info
"""

import datetime
import json
import multiprocessing
import os
import time
import traceback

import setproctitle
from ballistic_integrator.functions.functions import json_encoder


def write_status(job_dict, ID, status):
    """
    Function to write the status of the processes to a file
    """

    status_file = os.path.join(
        job_dict["worker_status_dir"], "worker_{}_status.txt".format(ID)
    )
    with open(status_file, "w") as f:
        f.write(status)


def write_current_system(job_dict, ID):
    """
    Function to write the current system of the process to a file
    """

    current_system = {
        "system_dict": job_dict["system_dict"],
        "grid_point": job_dict["grid_point"],
    }

    current_system_file = os.path.join(
        job_dict["current_system_dir"], "worker_{}_current_system.txt".format(ID)
    )
    with open(current_system_file, "w") as f:
        f.write(json.dumps(current_system))


def setup_grid_status_analysis_structure(configuration):
    """
    Function to handle setting up the grid status analysis structure
    """

    # make main analysis dir
    configuration["grid_status_main_dir"] = os.path.join(
        configuration["result_dir"], "grid_status"
    )
    os.makedirs(configuration["grid_status_main_dir"], exist_ok=True)

    # make worker status dir
    configuration["worker_status_dir"] = os.path.join(
        configuration["grid_status_main_dir"], "worker_status"
    )
    os.makedirs(configuration["worker_status_dir"], exist_ok=True)

    # make current system dir
    configuration["current_system_dir"] = os.path.join(
        configuration["grid_status_main_dir"], "current_system"
    )
    os.makedirs(configuration["current_system_dir"], exist_ok=True)

    return configuration


def get_total_N_grid(job_dict):
    """
    Function to return the total number of systems in the entire grid
    """

    tot_q = len(job_dict["q_range"]) if "q_range" in job_dict else 1
    tot_f = len(job_dict["f_range"]) if "f_range" in job_dict else 1
    tot_v = (
        len(job_dict["log10normalized_thermal_velocity_dispersion_range"])
        if "log10normalized_thermal_velocity_dispersion_range" in job_dict
        else 1
    )
    tot_area = (
        len(job_dict["log10normalized_stream_area_range"])
        if "log10normalized_stream_area_range" in job_dict
        else 1
    )

    return tot_q * tot_f * tot_v * tot_area


def print_progress(job_dict, worker_ID):
    """
    Function to print the progress of the grid
    """

    #########
    # Print current worker status
    if job_dict["verbosity"] > 0:
        system_string = return_system_string(job_dict)

        print(
            "Worker {} handling {}".format(
                worker_ID,
                #
                system_string,
            )
        )

    #########
    # Print global grid status
    num_cores = job_dict["num_cores"]
    if (job_dict["job_i"] > 0) and (
        job_dict["job_i"] % job_dict["print_frequency"] == 0
    ):
        # Get total number of systems:
        total_N = get_total_N_grid(job_dict)
        remaining_systems = total_N - job_dict["job_i"]

        # Calculated elapsed time
        cur_time = time.time()
        elapsed_time = cur_time - job_dict["grid_start_time"]

        # Calculate time per gridpoint
        average_time_per_gridpoint = elapsed_time / job_dict["job_i"]

        # Calculate time remaining
        estimate_time_remaining = remaining_systems / average_time_per_gridpoint

        print(
            "[{datetime_string}] Progress: {job_i}/{tot_N} time elapsed: {elapsed_time:.1E} avg per gridpoint: {average_time_per_gridpoint:.1E} avg per gridpoint per core: {average_time_per_gridpoint_per_core:.1E} estimated time remaining: {estimate_time_remaining:.1E}.\nCurrent system: {current_system}".format(
                datetime_string=datetime.datetime.strftime(
                    datetime.datetime.now(), "%Y-%m-%d %H:%M:%S"
                ),
                job_i=job_dict["job_i"],
                tot_N=total_N,
                elapsed_time=elapsed_time,
                average_time_per_gridpoint=average_time_per_gridpoint,
                average_time_per_gridpoint_per_core=average_time_per_gridpoint
                * num_cores,
                estimate_time_remaining=estimate_time_remaining,
                current_system=return_system_string(job_dict),
            )
        )


def return_system_string(job_dict):
    """
    Function to return the system handling string printed by the worker
    """

    #
    system_string = ""

    # Add grid_point info
    system_string += " ".join(
        "{}: {}".format(key, value)
        for (key, value) in job_dict["grid_point"].items()
        if value != 0
    )

    return system_string


def convolution_job_worker(job_queue, worker_ID, configuration, target_function):
    """
    Function that handles running the job
    """

    # Set name worker
    setproctitle.setproctitle(
        "Ballistic integration worker process {}".format(worker_ID)
    )

    # write status
    write_status(job_dict=configuration, ID=worker_ID, status="running")

    # Get items from the job_queue
    for job_dict in iter(job_queue.get, "STOP"):
        #########
        # Stopping or working
        if job_dict == "STOP":
            # write status
            write_status(job_dict=job_dict, ID=worker_ID, status="stopping")

            return None

        ##########
        # Print progress
        print_progress(job_dict=job_dict, worker_ID=worker_ID)

        # write current system
        write_current_system(job_dict=job_dict, ID=worker_ID)

        # Call the target function with the appropriate job
        try:
            if configuration["actually_run_system"]:
                target_function(job_dict)
        except BaseException as exception:
            print("An exception occurred: {}".format(exception))
            print(f"Exception Name: {type(exception).__name__}")
            print(f"Exception Desc: {exception}")
            print(f"Exception traceback: {traceback.print_exc()}")

    # write status
    write_status(job_dict=configuration, ID=worker_ID, status="stopping")

    return None


def multiprocessing_queue_filler(job_queue, num_cores, configuration, job_iterable):
    """
    Function to handle filling the queue for the multiprocessing
    """

    # Continuously fill the queue
    for job_i, job_dict in enumerate(job_iterable):
        # Put job in queue
        job_dict["job_i"] = job_i
        job_queue.put(job_dict)

    # Signal stop to workers
    for _ in range(num_cores):
        job_queue.put("STOP")


def multiprocessing_routine(configuration, job_iterable, target_function):
    """
    Main process to handle the multiprocessing
    """

    print("Starting multiprocessing of grid with the following configuration:")
    print(json.dumps(configuration, indent=4, default=json_encoder))

    ###################
    # Run the convolution through multiprocessing
    start = time.time()
    configuration["grid_start_time"] = start

    # Set up directories to store grid status details
    configuration = setup_grid_status_analysis_structure(configuration)

    # Set process name
    setproctitle.setproctitle("Ballistic integration parent process")

    # Set up the manager object that can share info between processes NOTE: you can add a result queue here
    manager = multiprocessing.Manager()
    job_queue = manager.Queue(configuration["max_job_queue_size"])

    # Create process instances
    processes = []
    for worker_ID in range(configuration["num_cores"]):
        processes.append(
            multiprocessing.Process(
                target=convolution_job_worker,
                args=(job_queue, worker_ID, configuration, target_function),
            )
        )

        # write status
        write_status(job_dict=configuration, ID=worker_ID, status="starting")

    # Activate the processes
    for p in processes:
        p.start()

    # Set up the system_queue
    multiprocessing_queue_filler(
        job_queue=job_queue,
        num_cores=configuration["num_cores"],
        configuration=configuration,
        job_iterable=job_iterable,
    )

    # Join the processes
    for ID, p in enumerate(processes):
        p.join()

        # write status
        write_status(job_dict=configuration, ID=ID, status="stopped")

    ######
    #
    print(
        "Finished multiprocessing all the tasks. This took {}".format(
            time.time() - start
        )
    )


if __name__ == "__main__":
    configuration = {
        "output_dir": os.path.join(os.getcwd(), "results"),
        "num_cores": 4,
        "max_job_queue_size": 10,
    }

    os.makedirs(configuration["output_dir"], exist_ok=True)

    multiprocessing_stage_1(configuration)
