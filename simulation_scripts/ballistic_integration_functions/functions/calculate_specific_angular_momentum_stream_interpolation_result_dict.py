"""
Main file containing the functionality to calculate the specific angular momentum of the stream at a set of fixed distances from the accretor
"""

import numpy as np
from ballistic_integration_code.scripts.L1_stream.functions.angular_momentum_calculations import (
    calculate_specific_angular_momenta_with_respect_to,
)
from ballistic_integration_code.scripts.lagrange_points.calculate_roche_lobe_volume import (
    rochelobe_radius_accretor,
)
from scipy import interpolate


def calculate_specific_angular_momentum_stream_interpolation_result_dict(
    system_dict,
    settings,
    frame_of_reference,
    distances_to_accretor,
    positions,
    velocities,
    key_format="specific_angmom_interpolation_radius_{}",
):
    """
    Main function to handle the calculation of the specific angular momentum of the stream at a set of fixed distances from the accretor
    """

    dummy_value = 0
    specific_angular_momentum_stream_interpolation_result_dict = {}

    # Combine data
    positions_and_velocities = np.zeros((positions.shape[0], positions.shape[1] * 2))
    positions_and_velocities[:, : positions.shape[1]] = positions
    positions_and_velocities[
        :, positions.shape[1] : positions.shape[1] + velocities.shape[1]
    ] = velocities

    # Calculate the roche-lobe radius of the accretor
    RL_accretor = rochelobe_radius_accretor(
        mass_donor=system_dict["mass_donor"], mass_accretor=system_dict["mass_accretor"]
    )

    ########
    # Set up interpolation table
    specific_angular_momentum_stream_interpolator = interpolate.interp1d(
        distances_to_accretor,
        positions_and_velocities.T,
    )

    ########
    # Interpolate and store in dict

    # set up array of radii
    interpolation_radius_fractions = np.linspace(
        settings["specific_angular_momentum_stream_interpolation_lower_bound"],
        settings["specific_angular_momentum_stream_interpolation_upper_bound"],
        settings["specific_angular_momentum_stream_interpolation_num_radii"],
    )

    # loop over radii
    for interpolation_radius_i, interpolation_radius_fraction in enumerate(
        interpolation_radius_fractions
    ):
        # get interpolated data
        try:
            # calculate actual radius
            interpolation_radius = interpolation_radius_fraction * RL_accretor

            # interpolate and get the data
            interpolated_data = specific_angular_momentum_stream_interpolator(
                interpolation_radius
            )

            # unpack data
            position_data_at_interpolated_radius = interpolated_data[
                : positions.shape[1]
            ]
            velocity_data_at_interpolated_radius = interpolated_data[
                positions.shape[1] : positions.shape[1] + velocities.shape[1]
            ]

            # Calculate specific angular momentum at given radius
            specific_angular_momentum_at_interpolated_radius = (
                calculate_specific_angular_momenta_with_respect_to(
                    to="accretor",
                    system_dict=system_dict,
                    reference_frame=frame_of_reference,
                    position_vector=position_data_at_interpolated_radius,
                    velocity_vector=velocity_data_at_interpolated_radius,
                )
            )

        # Catch error when out of bounds
        except ValueError:
            if settings["verbosity"] >= 2:
                print(
                    "interpolation radius fraction {} is out of bounds".format(
                        interpolation_radius_fraction
                    )
                )
            specific_angular_momentum_at_interpolated_radius = dummy_value

        # Store value
        specific_angular_momentum_stream_interpolation_result_dict[
            key_format.format(interpolation_radius_i)
        ] = specific_angular_momentum_at_interpolated_radius

    return specific_angular_momentum_stream_interpolation_result_dict


def return_dummy_specific_angular_momentum_stream_interpolation_result_dict(
    settings, dummy_value, key_format="specific_angmom_interpolation_radius_{}"
):
    """
    Function to return a dummy result dict. We use this to fill the return array when the trajectory does not accrete onto the accretor
    """

    specific_angular_momentum_stream_interpolation_result_dict = {}

    # set up array of radii
    interpolation_radii = np.linspace(
        settings["specific_angular_momentum_stream_interpolation_lower_bound"],
        settings["specific_angular_momentum_stream_interpolation_upper_bound"],
        settings["specific_angular_momentum_stream_interpolation_num_radii"],
    )

    # loop over radii
    for interpolation_radius_i, _ in enumerate(interpolation_radii):
        specific_angular_momentum_stream_interpolation_result_dict[
            key_format.format(interpolation_radius_i)
        ] = dummy_value

    return specific_angular_momentum_stream_interpolation_result_dict


if __name__ == "__main__":
    settings = {
        "specific_angular_momentum_stream_interpolation_enabled": False,
        "specific_angular_momentum_stream_interpolation_lower_bound": 0.1,  # In terms of roche lobe radius of accretor
        "specific_angular_momentum_stream_interpolation_upper_bound": 0.9,  # in terms of roche lobe radius of accretor
        "specific_angular_momentum_stream_interpolation_num_radii": 9,  # number of radii to calculate properties at
    }

    distances_to_accretor = np.array([1, 2, 3])
    positions = np.array([[1, 1], [2, 2], [3, 3]])
    velocities = np.array([[4, 4], [5, 5], [6, 6]])

    specific_angular_momentum_stream_interpolation_result_dict = (
        calculate_specific_angular_momentum_stream_interpolation_result_dict(
            system_dict={"mass_donor": 1, "mass_accretor": 1},
            settings=settings,
            frame_of_reference="center_of_mass",
            distances_to_accretor=distances_to_accretor,
            positions=positions,
            velocities=velocities,
        )
    )

    print(specific_angular_momentum_stream_interpolation_result_dict)

    ding = [
        "interpolation_radius_{}".format(stream_interpolation_radius_i)
        for stream_interpolation_radius_i in range(
            settings["specific_angular_momentum_stream_interpolation_num_radii"]
        )
    ]
    print(ding)
