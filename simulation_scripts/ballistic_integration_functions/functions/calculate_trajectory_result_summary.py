"""
Functions that are related to the output structure and the particle trajectory

TODO: calculate a fixed set of information for each given situation:
- energies, angular momenta etc etc all defined in all situations
- so a general function that can calculate all values with respect to everything for a given moment
"""

import json

import astropy.constants as const
import astropy.units as u
import numpy as np
from ballistic_integration_code.scripts.L1_stream.functions.angular_momentum_calculations import (
    calculate_circularisation_radius,
    calculate_specific_angular_momenta_with_respect_to,
)
from ballistic_integration_code.scripts.L1_stream.functions.calculate_specific_angular_momentum_stream_interpolation_result_dict import (
    calculate_specific_angular_momentum_stream_interpolation_result_dict,
    return_dummy_specific_angular_momentum_stream_interpolation_result_dict,
)
from ballistic_integration_code.scripts.L1_stream.project_settings import EXIT_CODES
from ballistic_integrator.functions.frame_of_reference import (
    calculate_position_accretor,
)

dummy_value = 1e-99
dummy_vector = np.array([dummy_value, dummy_value, dummy_value])


def calculate_specific_torque_orbit_onto_accretor(
    system_dict, reference_frame, h_init_don, h_final_acc
):
    """
    Function to calculate the specific torque on the orbit for trajectories that accrete onto the accretor
    """

    return h_init_don - h_final_acc


def calculate_specific_torque_orbit_onto_donor(
    system_dict, reference_frame, h_init_don, h_final_don
):
    """
    Function to calculate the specific torque on the orbit for trajectories that accrete onto the accretor

    TODO: fix this equation
    """

    return h_init_don - h_final_don


def calculate_eccentricity_vector(
    system_dict,
    normalized_velocity_vector,
    normalized_angmom_vector,
    normalized_position_vector,
):
    """
    Function to calculate the eccentricity vector

    Sources:
    - https://space.stackexchange.com/questions/57512/why-does-the-eccentricity-vector-always-point-towards-the-periapsis-of-an-orbit
    - https://en.wikipedia.org/wiki/Eccentricity_vector
    - https://orbital-mechanics.space/classical-orbital-elements/orbital-elements-and-the-state-vector.html


    Input is in normalised units. Function handles de-normalising and assigning actual units
    """

    # get units to work with
    separation = system_dict["separation"] * u.Rsun
    mass_accretor = system_dict["mass_accretor"] * u.Msun
    mass_donor = system_dict["mass_donor"] * u.Msun
    omega = np.sqrt(const.G * (mass_donor + mass_accretor) / separation**3)

    # Create 3d vectors with appropriate units
    velocity_vector = np.zeros(3) * (separation * omega)
    position_vector = np.zeros(3) * separation

    # Fill in the vectors
    velocity_vector[:2] = normalized_velocity_vector * (separation * omega)
    angmom_vector = normalized_angmom_vector * (separation**2 * omega)
    position_vector[:2] = normalized_position_vector * separation

    #
    term_1 = np.cross(velocity_vector, angmom_vector) / ((mass_accretor) * const.G)
    term_2 = position_vector / np.linalg.norm(position_vector)

    #
    eccentricity_vector = term_1 - term_2

    return eccentricity_vector


def calculate_true_anomaly(
    system_dict, eccentricity_vector, normalized_position_vector
):
    """
    Function to calculate the true anomaly

    https://orbital-mechanics.space/classical-orbital-elements/orbital-elements-and-the-state-vector.html#step-7true-anomaly
    """

    # Set up units
    separation = system_dict["separation"] * u.Rsun
    position_vector = np.zeros(3) * separation
    position_vector[:2] = normalized_position_vector * separation

    # Calculate true anomaly
    true_anomaly = np.arccos(
        np.dot(position_vector, eccentricity_vector)
        / (np.linalg.norm(position_vector) * np.linalg.norm(eccentricity_vector))
    )

    return np.rad2deg(true_anomaly)


def calculate_stream_orientation(y_position_at_rmin, velocity_at_rmin):
    """
    Function to calculate the mass stream orientation for the accretion-onto-accretor scenario.

    We use the direction of the velocity as the indicator of the orientation.

    returns:
        orientation: 1 if normal stream orientation (leading to spin-up of accretor), -1 if opposite orientation (leading to spin-down of accretor)
    """

    # NOTE: old method was this
    # if velocity_at_rmin[1] > 0:
    #     orientation = 1
    # elif velocity_at_rmin[1] < 0:
    #     orientation = -1
    # else:
    #     raise ValueError("y-velocity is 0")

    # NOTE: New method based on position
    if y_position_at_rmin < 0:
        orientation = 1
    elif y_position_at_rmin > 0:
        orientation = -1
    else:
        raise ValueError("y_position_at_rmin is 0")

    return orientation


def calculate_mach_number(speed_particle, thermal_speed):
    """
    Function to calculate the mach number of the flow. Here we use the speed of the particle and the initial thermal velocity.

    Critical here is that we assume that the thermal velocity does not change significantly.

    https://en.wikipedia.org/wiki/Mach_number#Classification_of_Mach_regimes
    """

    return speed_particle / thermal_speed


def calculate_trajectory_result_summary(trajectory_results):
    """
    Function to calculate the trajectory result summary, including extra quantities based on the trajectory evolution
    """

    ######
    # read out data
    system_dict = trajectory_results["system_dict"]
    settings = trajectory_results["settings"]
    frame_of_reference = settings["frame_of_reference"]

    # with respect to what
    pos_accretor = calculate_position_accretor(
        system_dict=system_dict, frame_of_reference=frame_of_reference
    )

    #############
    # Get the properties of the trajectory
    positions = np.array(
        [trajectory_results["x_positions"], trajectory_results["y_positions"]]
    ).T
    velocities = np.array(
        [trajectory_results["vx_velocities"], trajectory_results["vy_velocities"]]
    ).T
    accelarations = np.array(
        [trajectory_results["x_accelarations"], trajectory_results["y_accelarations"]]
    ).T
    energies = trajectory_results["energies"]
    angular_momenta = trajectory_results[
        "angular_momenta"
    ]  # with respect to the center of mass

    distances_to_accretor = np.linalg.norm(positions - pos_accretor, axis=1)

    # mach number of flow
    speeds = np.linalg.norm(velocities, axis=1)
    mach_numbers = calculate_mach_number(
        speed_particle=speeds,
        thermal_speed=10 ** settings["log10normalized_thermal_velocity_dispersion"],
    )

    ##########
    # set dummy values
    if settings["specific_angular_momentum_stream_interpolation_enabled"]:
        specific_angular_momentum_stream_interpolation_result_dict = (
            return_dummy_specific_angular_momentum_stream_interpolation_result_dict(
                settings=settings, dummy_value=dummy_value
            )
        )

    #############
    # find out what type of trajectory
    accretion_onto_accretor = (
        trajectory_results["exit_code"] == EXIT_CODES.ACCRETION_ONTO_ACCRETOR.value
    )
    accretion_onto_donor = (
        trajectory_results["exit_code"] == EXIT_CODES.ACCRETION_ONTO_DONOR.value
    )
    # lost_from_system = trajectory_results["exit_code"] == EXIT_CODES.ACCRETION_ONTO_ACCRETOR.value

    #############
    # Calculate angular momenta for starting point
    initial_position = positions[0]
    initial_velocity = velocities[0]
    initial_energy = energies[0]
    initial_angmom = angular_momenta[0]

    #
    h_init_com = calculate_specific_angular_momenta_with_respect_to(
        to="center_of_mass",
        system_dict=system_dict,
        reference_frame=frame_of_reference,
        position_vector=initial_position,
        velocity_vector=initial_velocity,
    )
    h_init_acc = calculate_specific_angular_momenta_with_respect_to(
        to="accretor",
        system_dict=system_dict,
        reference_frame=frame_of_reference,
        position_vector=initial_position,
        velocity_vector=initial_velocity,
    )
    h_init_don = calculate_specific_angular_momenta_with_respect_to(
        to="donor",
        system_dict=system_dict,
        reference_frame=frame_of_reference,
        position_vector=initial_position,
        velocity_vector=initial_velocity,
    )

    #############
    # Calculate values specifically for each outcome

    #######
    # accretion onto accretor
    min_distance_index = dummy_value
    angmom_at_rmin = dummy_value
    energy_at_rmin = dummy_value
    rmin = dummy_value
    rcirc = dummy_value
    stream_orientation = dummy_value

    h_min_com = dummy_value
    h_min_acc = dummy_value
    h_min_don = dummy_value

    specific_torque_orbit_onto_accretor = dummy_value
    specific_torque_orbit_onto_donor = dummy_value

    magnitude_eccentricity_onto_accretor = dummy_value
    true_anomaly_onto_accretor = dummy_value

    # ecc_vector_at_rmin = dummy_vector
    if accretion_onto_accretor:
        ############
        # Calculate rmin: radius of closest approach
        rmin = distances_to_accretor.min()
        rmin_index = np.argmin(distances_to_accretor)

        # Calculate position and velocity at rmin
        position_at_rmin = positions[rmin_index]
        velocity_at_rmin = velocities[rmin_index]

        if rmin != distances_to_accretor[-2]:
            print("MIN_DISTANCE is not the penultimate output")

        # Calculate circularisation radius
        rcirc = calculate_circularisation_radius(
            system_dict=system_dict,
            reference_frame=frame_of_reference,
            position_vector_min=position_at_rmin,
            velocity_vector_min=velocity_at_rmin,
        )

        # Calculate the orientation of the stream
        stream_orientation = calculate_stream_orientation(
            y_position_at_rmin=position_at_rmin[1], velocity_at_rmin=velocity_at_rmin
        )

        # Calculate the angular momenta
        h_min_com = calculate_specific_angular_momenta_with_respect_to(
            to="center_of_mass",
            system_dict=system_dict,
            reference_frame=frame_of_reference,
            position_vector=position_at_rmin,
            velocity_vector=velocity_at_rmin,
        )
        h_min_acc = calculate_specific_angular_momenta_with_respect_to(
            to="accretor",
            system_dict=system_dict,
            reference_frame=frame_of_reference,
            position_vector=position_at_rmin,
            velocity_vector=velocity_at_rmin,
        )
        h_min_don = calculate_specific_angular_momenta_with_respect_to(
            to="donor",
            system_dict=system_dict,
            reference_frame=frame_of_reference,
            position_vector=position_at_rmin,
            velocity_vector=velocity_at_rmin,
        )

        # calculate torque on orbit
        specific_torque_orbit_onto_accretor = (
            calculate_specific_torque_orbit_onto_accretor(
                system_dict=system_dict,
                reference_frame=frame_of_reference,
                h_init_don=h_init_don,
                h_final_acc=h_min_acc,
            )
        )

        ######
        # calculate eccentricity vector
        pos_accretor = calculate_position_accretor(
            system_dict=system_dict, frame_of_reference=frame_of_reference
        )
        position_at_rmin_wrt_accretor = position_at_rmin - pos_accretor

        h_vector_at_rmin_wrt_accretor = np.array([0, 0, h_min_acc])

        ecc_vector_at_rmin = calculate_eccentricity_vector(
            system_dict=system_dict,
            normalized_velocity_vector=velocity_at_rmin,
            normalized_angmom_vector=h_vector_at_rmin_wrt_accretor,
            normalized_position_vector=position_at_rmin_wrt_accretor,
        )
        magnitude_eccentricity_onto_accretor = np.linalg.norm(ecc_vector_at_rmin)
        true_anomaly_onto_accretor = calculate_true_anomaly(
            system_dict=system_dict,
            eccentricity_vector=ecc_vector_at_rmin,
            normalized_position_vector=position_at_rmin_wrt_accretor,
        )

        #####
        # Calculate
        if settings["specific_angular_momentum_stream_interpolation_enabled"]:
            specific_angular_momentum_stream_interpolation_result_dict = (
                calculate_specific_angular_momentum_stream_interpolation_result_dict(
                    system_dict=system_dict,
                    settings=settings,
                    frame_of_reference=frame_of_reference,
                    distances_to_accretor=distances_to_accretor,
                    positions=positions,
                    velocities=velocities,
                )
            )

        # print("Angular momenta for accretion onto donor:")
        # print(
        #     "wrt center of mass: h_init: {} h_min: {} diff: {}".format(
        #         h_init_com, h_min_com, h_init_com - h_min_com
        #     )
        # )
        # print(
        #     "wrt accretor: h_init: {} h_min: {} diff: {}".format(
        #         h_init_acc, h_min_acc, h_init_acc - h_min_acc
        #     )
        # )
        # print(
        #     "wrt donor: h_init: {} h_min: {} diff: {}".format(
        #         h_init_don, h_min_don, h_init_don - h_min_don
        #     )
        # )

    #######
    # accretion onto donor
    # angle_donor_accretion = dummy_value
    specific_angular_momentum_multiplier_self_accretion = dummy_value
    if accretion_onto_donor:
        # Calculate position and velocity at accretion onto donor
        position_at_donor_accretion = positions[-1]
        velocity_at_donor_accretion = velocities[-1]

        # Calculate the angular momenta
        h_final_com = calculate_specific_angular_momenta_with_respect_to(
            to="center_of_mass",
            system_dict=system_dict,
            reference_frame=frame_of_reference,
            position_vector=position_at_donor_accretion,
            velocity_vector=velocity_at_donor_accretion,
        )
        h_final_acc = calculate_specific_angular_momenta_with_respect_to(
            to="accretor",
            system_dict=system_dict,
            reference_frame=frame_of_reference,
            position_vector=position_at_donor_accretion,
            velocity_vector=velocity_at_donor_accretion,
        )
        h_final_don = calculate_specific_angular_momenta_with_respect_to(
            to="donor",
            system_dict=system_dict,
            reference_frame=frame_of_reference,
            position_vector=position_at_donor_accretion,
            velocity_vector=velocity_at_donor_accretion,
        )

        #
        specific_angular_momentum_multiplier_self_accretion = h_final_don / h_init_don
        specific_torque_orbit_onto_donor = calculate_specific_torque_orbit_onto_donor(
            system_dict=system_dict,
            reference_frame=frame_of_reference,
            h_init_don=h_init_don,
            h_final_don=h_final_don,
        )

    ########################
    # Set up the return dict
    return_dict = {
        #####################
        # Pass along global settings
        "settings": settings,
        "exit_code": trajectory_results["exit_code"],
        ###########
        # system dict and components
        "times": trajectory_results["times"],
        "timesteps": trajectory_results["timesteps"],
        # Store dynamics data w.r.t rotating reference frame centred on center of mass
        "positions": positions,
        "velocities": velocities,
        "accelarations": accelarations,
        "energies": energies,
        "angular_momenta": angular_momenta,
        "distances_to_accretor": distances_to_accretor,
        ##
        "h_init_don": h_init_don,
        # Quantities that depend on the outcome
        # Accretion onto accretor
        "rmin": rmin,
        "rcirc": rcirc,
        "h_min_acc": h_min_acc,
        "stream_orientation": stream_orientation,
        "specific_torque_orbit_onto_accretor": specific_torque_orbit_onto_accretor,
        "magnitude_eccentricity_onto_accretor": magnitude_eccentricity_onto_accretor,
        "true_anomaly_onto_accretor": true_anomaly_onto_accretor,
        # Accretion onto donor
        "specific_angular_momentum_multiplier_self_accretion": specific_angular_momentum_multiplier_self_accretion,
        "specific_torque_orbit_onto_donor": specific_torque_orbit_onto_donor,
    }

    if settings["specific_angular_momentum_stream_interpolation_enabled"]:
        return_dict = {
            **return_dict,
            **specific_angular_momentum_stream_interpolation_result_dict,
        }

    return return_dict


def readout_resultfile_and_calculate_trajectory_result_summary(filename):
    """
    Function to read out the dataset, calculate extra quantities and return the quantities we want
    """

    # Read out file
    with open(filename, "r") as f:
        trajectory_results = json.loads(f.read())

    # Calculate result summary
    trajectory_result_summary = calculate_trajectory_result_summary(trajectory_results)

    return trajectory_result_summary
