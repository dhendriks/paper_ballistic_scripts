"""
Function to handle running the simulations and all the surrounding tasks. Uses imports and a dict to map the correct function for the correct stage
"""

import copy
import os

from ballistic_integration_code.scripts.L1_stream.functions.build_latex_table import (
    build_latex_table,
)

# Project imports
from ballistic_integration_code.scripts.L1_stream.functions.functions import (
    write_settings_to_file,
)
from ballistic_integration_code.scripts.L1_stream.functions.multiprocessing_routines import (
    multiprocessing_routine,
)
from ballistic_integration_code.scripts.L1_stream.functions.rerun_failed_systems_functions import (
    re_run_failed_systems_main,
)
from ballistic_integration_code.scripts.L1_stream.project_settings import parameter_dict

# Stage 1 imports
from ballistic_integration_code.scripts.L1_stream.stage_1.generate_data_header_stage_1 import (
    generate_data_header_stage_1,
)
from ballistic_integration_code.scripts.L1_stream.stage_1.generate_report_stage_1 import (
    generate_report_stage_1,
)
from ballistic_integration_code.scripts.L1_stream.stage_1.grid_generator_stage_1 import (
    grid_generator_stage_1,
)
from ballistic_integration_code.scripts.L1_stream.stage_1.integrate_trajectories_from_L1_stage_1 import (
    integrate_trajectories_from_L1_stage_1,
)

# Stage 2 imports
from ballistic_integration_code.scripts.L1_stream.stage_2.generate_data_header_stage_2 import (
    generate_data_header_stage_2,
)
from ballistic_integration_code.scripts.L1_stream.stage_2.generate_report_stage_2 import (
    generate_report_stage_2,
)
from ballistic_integration_code.scripts.L1_stream.stage_2.grid_generator_stage_2 import (
    grid_generator_stage_2,
)
from ballistic_integration_code.scripts.L1_stream.stage_2.integrate_trajectories_from_L1_stage_2 import (
    integrate_trajectories_from_L1_stage_2,
)
from ballistic_integration_code.scripts.L1_stream.stage_3.generate_csv_datafile_stage_3 import (
    generate_csv_datafile_stage_3,
)
from ballistic_integration_code.scripts.L1_stream.stage_3.generate_data_header_stage_3 import (
    generate_data_header_stage_3,
)
from ballistic_integration_code.scripts.L1_stream.stage_3.generate_report_stage_3 import (
    generate_report_stage_3,
)

# Stage 3
from ballistic_integration_code.scripts.L1_stream.stage_3.grid_generator_stage_3 import (
    grid_generator_stage_3,
)
from ballistic_integration_code.scripts.L1_stream.stage_3.integrate_trajectories_from_L1_stage_3 import (
    integrate_trajectories_from_L1_stage_3,
)
from ballistic_integration_code.scripts.lagrange_points.run_all import (
    run_all as rochelobe_interpolation_grid_run_all,
)
from david_phd_functions.backup_functions.functions import backup_if_exists

# Set up dictionary containing all the stage functions
stage_function_dict = {
    "stage_1": {
        "grid_generator": grid_generator_stage_1,
        "integrate_trajectories_from_L1": integrate_trajectories_from_L1_stage_1,
        "generate_report": generate_report_stage_1,
        "generate_data_header": generate_data_header_stage_1,
    },
    "stage_2": {
        "grid_generator": grid_generator_stage_2,
        "integrate_trajectories_from_L1": integrate_trajectories_from_L1_stage_2,
        "generate_report": generate_report_stage_2,
        "generate_data_header": generate_data_header_stage_2,
    },
    "stage_3": {
        "grid_generator": grid_generator_stage_3,
        "integrate_trajectories_from_L1": integrate_trajectories_from_L1_stage_3,
        "generate_report": generate_report_stage_3,
        "generate_data_header": generate_data_header_stage_3,
        "generate_csv_interpolation_table": generate_csv_datafile_stage_3,
    },
}


def run_all(
    settings,
    stage,
    run_trajectories=True,
    generate_report=True,
    generate_output_interpolation_files=True,
    generate_rochelobe_interpolation_data=True,
    generate_description_table=True,
    re_run_failed_systems=False,
    re_run_settings={},
):
    """
    Function to handle running the simulations and all the surrounding tasks. Uses imports and a dict to map the correct function for the correct stage
    """

    ##############################################
    # Get the correct set of functions
    function_dict = stage_function_dict[stage]

    ##############################################
    # Run simulations and handle processing of data
    if run_trajectories:
        ##############################################
        # Check if the directory of the current simulation exists. If it does, then delete the old dir
        result_dir = settings["result_dir"]
        if settings.get("backup_if_data_exists", True):
            backup_if_exists(result_dir, remove_old_directory_after_backup=True)

        #####################
        # Write settings to file
        write_settings_to_file(
            settings,
            output_filename=os.path.join(settings["result_dir"], "settings.json"),
        )

        ######################
        # Set up grid generator
        grid_generator = function_dict["grid_generator"](settings)

        ######################
        # Call the multiprocessing routine and let it handle everything
        multiprocessing_routine(
            configuration=settings,
            job_iterable=grid_generator,
            target_function=function_dict["integrate_trajectories_from_L1"],
        )

    ######################
    # Call functionality to make a report on the summarized results
    if generate_report:
        report_filename = os.path.join(settings["result_dir"], "simulation_report.txt")
        print("Writing report to {}".format(report_filename))
        function_dict["generate_report"](
            data_file=os.path.join(
                settings["result_dir"], "trajectory_summary_data.txt"
            ),
            report_file=report_filename,
        )

    #############################################
    # Output interpolation files
    if generate_output_interpolation_files:
        trajectory_data_file = os.path.join(
            settings["result_dir"], settings["trajectory_data_file_basename"]
        )
        trajectory_csv_interpolation_table_filename = os.path.join(
            settings["result_dir"],
            settings["trajectory_csv_interpolation_table_file_basename"],
        )
        trajectory_header_interpolation_table_filename = os.path.join(
            settings["result_dir"],
            settings["trajectory_header_interpolation_table_file_basename"],
        )

        #############################################
        # Generate dataframe -formatted table from the data
        print("Generating CSV output interpolation file")
        function_dict["generate_csv_interpolation_table"](
            input_interpolation_textfile=trajectory_data_file,
            output_interpolation_textfile=trajectory_csv_interpolation_table_filename,
            settings=settings,
            parameter_dict=parameter_dict,
        )

        ######################
        # Load as dataframe and write the interpolation header
        print("Generating data header output interpolation file")
        function_dict["generate_data_header"](
            input_interpolation_textfile=trajectory_data_file,
            output_interpolation_textfile=trajectory_header_interpolation_table_filename,
            settings=settings,
            parameter_dict=parameter_dict,
        )

    ######################
    # Call functionality to run the rochelobe size calculations
    if generate_rochelobe_interpolation_data:
        print("Generating Roche-lobe volume interpolation data")
        #
        RL_settings = copy.deepcopy(settings)
        if "log10normalized_thermal_velocity_dispersion_range" in RL_settings.keys():
            del RL_settings["log10normalized_thermal_velocity_dispersion_range"]

        rochelobe_interpolation_grid_run_all(
            settings={**settings, "backup_if_data_exists": False},
            parameter_dict=parameter_dict,
            generate_lagrange_point_and_roche_lobe_information=True,
            generate_output_interpolation_files=True,
        )

    ######################
    # Call functionality to build the description table for the paper
    if generate_description_table:
        ######################
        # Load as dataframe and write the interpolation table format
        print("Generating description_table header")
        description_table_output_filename = os.path.join(
            settings["result_dir"], "description_table.tex"
        )
        build_latex_table(
            input_parameters=settings["input_parameter_list_data_header"],
            output_parameters=settings["output_parameter_list_data_header"],
            parameter_dict=parameter_dict,
            output_filename=description_table_output_filename,
        )

    ######################
    # Call functionality to re-run failed systems with an updated configuration
    if re_run_failed_systems:
        print("Re-running failed systems")
        re_run_failed_systems_main(
            target_function=function_dict["integrate_trajectories_from_L1"],
            simulation_dir=os.path.join(settings["result_dir"]),
            re_run_settings=re_run_settings,
        )
