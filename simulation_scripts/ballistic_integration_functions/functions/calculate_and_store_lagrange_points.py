"""
Function to calculate and store the lagrange points
"""

from ballistic_integrator.functions.frame_of_reference.wrapper import (
    calculate_lagrange_points,
)


def calculate_and_store_lagrange_points(settings, result_dict):
    """
    Function to calculate and store the lagrange points
    """

    # Calculate the lagrange point locations for the first 3
    L_points = calculate_lagrange_points(
        system_dict=settings["system_dict"],
        frame_of_reference=settings["frame_of_reference"],
    )

    # store x-coordinate of the three
    result_dict["L1_x"] = L_points["L1"][0]
    result_dict["L2_x"] = L_points["L2"][0]
    result_dict["L3_x"] = L_points["L3"][0]

    return result_dict
