"""
Function to write the description header string
"""

from ballistic_integration_code.scripts.L1_stream.project_settings import (
    dimensionless_unit,
    parameter_dict,
)


def add_unit(description_string, parameter_dict):
    """
    Function to add the unit to a string if necessary
    """

    # construct
    unit_string = ""
    if "unit" in parameter_dict:
        try:
            if parameter_dict["unit"] != dimensionless_unit:
                unit_string = " Unit: [{}]".format(
                    parameter_dict["unit"].to_string("latex_inline")
                )
        except:
            if parameter_dict["unit"].unit != dimensionless_unit:
                unit_string = " Unit: [{}]".format(
                    parameter_dict["unit"].unit.to_string("latex_inline")
                )

    # add
    description_string += unit_string

    return description_string


def build_description_headerstring(
    input_parameters, output_parameters, parameter_dict, comment_symbol="//"
):
    """
    Function to build the description headerstring
    """

    headerstring = ""

    # # Decide what info we include
    # include_params = ["shortname", "longname", "description", "reference", "parameter_type"]

    #######################
    # Loop over input params
    headerstring += "{comment_symbol} INPUT PARAMETERS DESCRIPTION\n{comment_symbol}\n{comment_symbol}".format(
        comment_symbol=comment_symbol
    )
    for input_parameter_i, input_parameter in enumerate(input_parameters):
        input_parameter_dict = parameter_dict[input_parameter]

        # build string
        input_parameter_string = "{} (index: {}) {}: {} description: {}.".format(
            comment_symbol,
            input_parameter_i,
            input_parameter_dict["shortname"],
            input_parameter_dict["longname"],
            input_parameter_dict["description"],
        )

        # add unit if necessary
        input_parameter_string = add_unit(input_parameter_string, input_parameter_dict)

        #
        headerstring += input_parameter_string + "\n"

    headerstring += "{} \n".format(comment_symbol)

    #######################
    # Loop over output params
    headerstring += "{comment_symbol} OUTPUT PARAMETERS DESCRIPTION\n{comment_symbol}\n{comment_symbol}\n".format(
        comment_symbol=comment_symbol
    )
    for output_parameter_i, output_parameter in enumerate(output_parameters):
        output_parameter_dict = parameter_dict[output_parameter]

        output_parameter_string = "{} (index: {}) {}: {} description: {}.".format(
            comment_symbol,
            output_parameter_i,
            output_parameter_dict["shortname"],
            output_parameter_dict["longname"],
            output_parameter_dict["description"],
        )

        # add unit if necessary
        output_parameter_string = add_unit(
            output_parameter_string, output_parameter_dict
        )

        #
        headerstring += output_parameter_string + "\n"

    #
    headerstring += "{} \n".format(comment_symbol)

    #
    return headerstring


if __name__ == "__main__":
    input_parameters = ["A_factor", "mass_ratio"]
    output_parameters = ["rmin", "rcirc"]

    #
    build_description_headerstring(
        input_parameters=input_parameters,
        output_parameters=output_parameters,
        parameter_dict=parameter_dict,
    )
