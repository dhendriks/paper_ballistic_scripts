"""
Script containing the function to write the output to a file
"""

import os


def write_to_file(settings, trajectory_summary_data_filename, result_dict):
    """
    Function to write the (averaged) results of the trajectory calculation for the current gridpoint to a file.
    """

    # write headerline
    if not os.path.isfile(trajectory_summary_data_filename):
        with open(trajectory_summary_data_filename, "w") as f:
            headerline = settings["separator"].join(sorted(result_dict.keys()))
            f.write(headerline + "\n")

    # Write results
    with open(trajectory_summary_data_filename, "a") as f:
        valueline = settings["separator"].join(
            [str(result_dict[key]) for key in sorted(result_dict.keys())]
        )
        f.write(valueline + "\n")
