"""
Function to calculate and store the roche lobe information
"""

from ballistic_integration_code.scripts.lagrange_points.calculate_roche_lobe_volume import (
    calculate_volume_roche_lobe_information,
)


def calculate_and_store_roche_lobe_information(settings, result_dict):
    """
    Function to calculate and store the roche lobe information
    """

    # Calculate the roche lobe volumes
    roche_lobe_volume_dict = calculate_volume_roche_lobe_information(
        system_dict=settings["system_dict"],
        frame_of_reference=settings["frame_of_reference"],
        settings=settings,
        plot_collection=False,
        print_info=False,
    )

    # Store info
    result_dict["roche_lobe_volume"] = roche_lobe_volume_dict["roche_lobe_volume"]
    result_dict["ratio_new_eggleton"] = roche_lobe_volume_dict["ratio_new_eggleton"]

    return result_dict
