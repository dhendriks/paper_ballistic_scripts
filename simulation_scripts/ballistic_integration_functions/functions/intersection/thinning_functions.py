"""
Functions for thinning the trajectory

TODO: these functions can be tidied up and made more general with a central function that handles the thinning
"""

import numpy as np
from ballistic_integration_code.scripts.L1_stream.functions.intersection.utility import (
    calculate_angle_segments,
    calculate_angle_segments_vectorized,
)
from ballistic_integration_code.scripts.L1_stream.functions.utils import timing


def get_thinned_indices(value_array, threshold_value):
    """
    Function that extracts the thinned indices based on an array of either distances or angles between each index and a threshold value.

    Args:
        value_array (np.float array): numpy array that contains distances or angles between the elements in the original trajectory (midsection)
        threshold_value (TYPE): minimum value for the newly thinned segments.

    Returns:
        thinned_indices (np.int array: indices of the thinned trajectory (midsection)

    """

    # print("Number of nan values: ", np.count_nonzero(np.isnan(value_array)))

    # Create cumsum array
    # cumsum_array = np.cumsum(value_array)
    # print("cumsum_array: ", cumsum_array)

    nancumsum_array = np.nancumsum(value_array)
    # print("nancumsum_array: ", nancumsum_array)

    # divide by threshold
    divided_array = nancumsum_array / threshold_value

    # ensure integer
    integer_array = np.floor(divided_array).astype(int)

    # Take the diff
    diff_array = np.diff(integer_array)

    # ensure max = 1
    diff_array[diff_array > 1] = 1

    # get indices
    thinned_indices = np.where(diff_array == 1)

    #
    return thinned_indices


def thin_trajectory_by_index(trajectory, settings):
    """
    Function to thin the trajectory by index slicing.

    The configuration is to set a total new length (in terms of elements) of the trajectory. Index slicing is handled to achieve the chosen length (ish)
    """

    # store star and end position
    start_position = trajectory[0]
    end_position = trajectory[-1]

    # Take midsection and perform thinning
    midsection = trajectory[1:-1]

    ###########
    # Construct thinned trajectory

    # thin midsection
    if len(midsection) > settings["trajectory_thinning_length"]:
        # get steps to thin
        steps = int(np.ceil(len(midsection) / settings["trajectory_thinning_length"]))

        # thin midsection
        midsection = midsection[::steps]

    # failsafe for empty lists
    if not len(midsection) > 0:
        return trajectory

    # construct midsection again
    thinned_trajectory = np.zeros((len(midsection) + 2, 2))
    thinned_trajectory[0] = start_position
    thinned_trajectory[1:-1] = midsection
    thinned_trajectory[-1] = end_position

    return thinned_trajectory


def thin_trajectory_by_angle(trajectory, settings):
    """
    Function to thin the trajectory by the distance spanned by segments

    Args:
        trajectory (float np.array): 2-d array that contains the positions of the trajectory
        settings (dict): settings dictionary

    Returns:
        TYPE: thinned trajectory
    """

    # store star and end position
    start_position = trajectory[0]
    end_position = trajectory[-1]

    # Take midsection and perform thinning
    midsection = trajectory[1:-1]

    # set up segments
    start_position_segments = midsection[:-1]
    end_position_segments = midsection[1:]

    # Construct segments
    segment_arr = np.zeros((len(end_position_segments), 2, 2))
    segment_arr[:, 0] = start_position_segments
    segment_arr[:, 1] = end_position_segments

    #############
    # Calculate the angle segments
    angles_segments = calculate_angle_segments_vectorized(segment_arr=segment_arr)

    #############
    # Thin segments by cumulative angle change
    new_thinned_indices = get_thinned_indices(
        value_array=angles_segments,
        threshold_value=settings["trajectory_thinning_angle_threshold"],
    )
    new_thinned_indices = new_thinned_indices[0]
    thinned_elements = midsection[new_thinned_indices]

    # failsafe for empty lists
    if not len(thinned_elements) > 0:
        return trajectory

    # construct midsection again
    thinned_trajectory = np.zeros((len(thinned_elements) + 2, 2))
    thinned_trajectory[0] = start_position
    thinned_trajectory[1:-1] = thinned_elements
    thinned_trajectory[-1] = end_position

    return thinned_trajectory


def thin_trajectory_by_distance(trajectory, settings):
    """
    Function to thin the trajectory by the distance spanned by segments

    Args:
        trajectory (float np.array): 2-d array that contains the positions of the trajectory
        settings (dict): settings dictionary

    Returns:
        TYPE: thinned trajectory
    """

    # store star and end position
    start_position = trajectory[0]
    end_position = trajectory[-1]

    # Take midsection and perform thinning
    midsection = trajectory[1:-1]

    # calculate distances between each point
    distances = np.linalg.norm(midsection[1:] - midsection[:-1], axis=1)

    # Get indices and construct new segments
    new_thinned_indices = get_thinned_indices(
        value_array=distances,
        threshold_value=settings["trajectory_thinning_distance_threshold"],
    )
    thinned_elements = midsection[new_thinned_indices]

    # failsafe for empty lists
    if not len(midsection) > 0:
        return trajectory

    # construct midsection again
    thinned_trajectory = np.zeros((len(thinned_elements) + 2, 2))
    thinned_trajectory[0] = start_position
    thinned_trajectory[1:-1] = thinned_elements
    thinned_trajectory[-1] = end_position

    return thinned_trajectory


def thin_trajectory(trajectory, settings):
    """
    Function to thin the trajectory
    """

    if settings["thin_trajectory"]:
        if settings["thin_by_index"]:
            thinned_trajectory = thin_trajectory_by_index(trajectory, settings)
        elif settings["thin_by_distance"]:
            thinned_trajectory = thin_trajectory_by_distance(trajectory, settings)
        elif settings["thin_by_angle"]:
            thinned_trajectory = thin_trajectory_by_angle(trajectory, settings)
        else:
            raise ValueError("thinning method unknown")

    return thinned_trajectory
