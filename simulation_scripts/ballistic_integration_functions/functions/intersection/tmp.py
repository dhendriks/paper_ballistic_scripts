import itertools

import numpy as np

listo = [0, 1, 2, 3]


from ballistic_integration_code.scripts.L1_stream.functions.intersection.utility import (
    calculate_angle_segments,
)

for el in [1, 2, 3]:
    print(el)
    continue
    print(el**2)


quit()


def calculate_angle_segments(segment1, segment2):
    """
    Function to calculate the angle between two line segments.

    Returns the angle in degrees

    # TODO: can we create a method to do this in 1 go for arrays of segments?
    """

    print("segment1:\n\t{}\nsegment2:\n\t{}".format(segment1, segment2))

    # make vectors
    print("Creating vectors")
    vec_1 = (segment1[1][0] - segment1[0][0], segment1[1][1] - segment1[0][1])
    vec_2 = (segment2[1][0] - segment2[0][0], segment2[1][1] - segment2[0][1])
    print("vec_1:\n\t{}\nvec_2:\n\t{}".format(vec_1, vec_2))

    # calculate lengths
    print("Calculating lengths of vectors")
    len_vec_1 = np.linalg.norm(vec_1)
    len_vec_2 = np.linalg.norm(vec_2)
    print("\tlen_vec_1: {}".format(len_vec_1))
    print("\tlen_vec_2: {}".format(len_vec_2))

    # Calculate dotproduct
    print("Calculating dotproduct vectors")
    dot = np.dot(vec_1, vec_2)
    print("\tdot: {}".format(dot))

    # calculate angle
    print("Calculting angle")
    angle = np.arccos((dot) / (len_vec_1 * len_vec_2))
    print("\tAngle: {}".format(angle))

    # Convert to deg
    print("Converting to deg")
    deg = np.rad2deg(angle)
    print("\tdeg: {}".format(deg))
    print("\n")
    return deg


def calculate_angle_segments_vectorized(segment_arr):
    """
    Function to calculate the angle between two line segments.

    Returns the angle in degrees
    """

    #
    segment1_arr = segment_arr[:-1]
    segment2_arr = segment_arr[1:]

    # Create the vectors
    print("Creating vectors")
    vec_1_arr = np.zeros((len(segment1_arr), 2))
    vec_1_arr[:, 0] = segment1_arr[:, 1, 0] - segment1_arr[:, 0, 0]
    vec_1_arr[:, 1] = segment1_arr[:, 1, 1] - segment1_arr[:, 0, 1]

    vec_2_arr = np.zeros((len(segment1_arr), 2))
    vec_2_arr[:, 0] = segment2_arr[:, 1, 0] - segment2_arr[:, 0, 0]
    vec_2_arr[:, 1] = segment2_arr[:, 1, 1] - segment2_arr[:, 0, 1]

    print("vec_1_arr:\n{}".format(vec_1_arr))
    print("vec_2_arr:\n{}".format(vec_2_arr))

    #
    print("Calculating lengths")

    # calculate lengths
    print("Calculating lengths of vectors")
    len_vec_1_arr = np.linalg.norm(vec_1_arr, axis=1)
    len_vec_2_arr = np.linalg.norm(vec_2_arr, axis=1)
    print("\tlen_vec_1: {}".format(len_vec_1_arr))
    print("\tlen_vec_2: {}".format(len_vec_2_arr))

    # Calculate dotproduct
    print("Calculating dotproduct vectors")
    dot_arr = (vec_1_arr * vec_2_arr).sum(axis=1)
    print("\tdot_arr: {}".format(dot_arr))

    # calculate angle
    print("Calculting angle")
    angle_arr = np.arccos((dot_arr) / (len_vec_1_arr * len_vec_2_arr))
    print("\tangle_arr: {}".format(angle_arr))

    # Convert to deg
    print("Converting to deg")
    deg_arr = np.rad2deg(angle_arr)
    print("\tdeg: {}".format(deg_arr))
    print("\n")
    return deg_arr


if __name__ == "__main__":
    arr = np.array([[0, 1], [0, 1]])

    positions = [
        [0, 0],
        [1, 1],
        [2, 2],
        [3, 3],
        [4, 8],
        [5, 5],
        [6, 6],
        [7, 7],
    ]
    positions_arr = np.array(positions)

    #
    start_positions_segments = positions[:-1]
    end_positions_segments = positions[1:]

    #
    segment_arr = np.zeros((len(end_positions_segments), 2, 2))

    #
    segment_arr[:, 0] = start_positions_segments
    segment_arr[:, 1] = end_positions_segments

    #
    for seg1, seg2 in zip(segment_arr[:-1], segment_arr[1:]):
        calculate_angle_segments(seg1, seg2)

    calculate_angle_segments_vectorized(segment_arr=segment_arr)
