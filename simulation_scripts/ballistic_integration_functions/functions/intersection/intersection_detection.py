"""
Main file for the intersection functionality

we'll make use of the sweepintersector algorithm from https://github.com/prochitecture/sweep_intersector

useful explanation video:
https://www.youtube.com/watch?v=qkhUNzCGDt0
"""

from itertools import combinations

import numpy as np
from ballistic_integration_code.scripts.L1_stream.functions.intersection.filter_functions import (
    filter_based_on_angle,
    filter_based_on_filter_dict,
)
from ballistic_integration_code.scripts.L1_stream.functions.intersection.intersection_algorithms.sweep_intersector.SweepIntersectorLib.SweepIntersector import (
    SweepIntersector,
)
from ballistic_integration_code.scripts.L1_stream.functions.intersection.thinning_functions import (
    thin_trajectory,
)
from ballistic_integration_code.scripts.L1_stream.functions.intersection.utility import (
    calculate_angle_segments,
    create_list_of_lists,
)


def create_segmentlist(start_x_list, start_y_list, end_x_list, end_y_list):
    """
    Function to create the segment list
    """

    segList = []

    # Add start and end points to list
    for start_x, start_y, end_x, end_y in zip(
        start_x_list, start_y_list, end_x_list, end_y_list
    ):
        segment = ((start_x, start_y), (end_x, end_y))
        segList.append(segment)

    return segList


def sweepintersector(
    start_x_list,
    start_y_list,
    end_x_list,
    end_y_list,
    settings,
    angle_threshold=0,
    filter_dict=None,
):
    """
    Call to sweep intersector

    TODO: move everything to individual functions
    """

    # Create segment list
    segList = create_segmentlist(
        start_x_list=start_x_list,
        start_y_list=start_y_list,
        end_x_list=end_x_list,
        end_y_list=end_y_list,
    )

    # Set up the SweepIntersector
    isector = SweepIntersector()

    # Find all the intersections
    isectDic = isector.findIntersections(segList)

    #######
    # Extract intersection points and the associated tuples of segments
    #   the way sweep intersector works is to print out that the segment has an intersection,
    #   but it doesnt necesarilly add the associated other segment.
    #
    #   So we first find all the unique locations where lines intersect.
    #   And then loop over everything again to get all the segments that are associated with that point.
    #
    #   Finally, for every unique location of intersection we make a list of tuples of segments that intersect there.

    ####
    # Get unique points of intersection and the segments that are associated with that point
    unique_intersection_location_dict = {}

    # loop over all segment tuples that have some intersection
    for seg, isects in isectDic.items():
        # Ignore endpoints
        for p in isects[1:-1]:
            # Store point of intersection
            if not p in unique_intersection_location_dict:
                unique_intersection_location_dict[p] = []

            # Store segments associated with that point
            unique_intersection_location_dict[p].append(seg)

    # Make list of tuples that intersect
    intersecting_segments_per_unique_intersection_location = {}

    # Find all combinations for each intersection location
    for point, segments in unique_intersection_location_dict.items():
        # generate all combinations
        intersecting_segments_per_unique_intersection_location[point] = list(
            combinations(segments, 2)
        )

    #######
    # Calculate the angle of intersection and filter out the segment-pairs that intersect at an angle below a certain threshold
    filtered_intersecting_segments_per_unique_intersection_location = (
        filter_based_on_angle(
            intersection_dict=intersecting_segments_per_unique_intersection_location,
            angle_threshold=angle_threshold,
        )
    )

    #######
    # Filter based on other intersection dicts (useful to filter out the self-intersection results of two tracks)
    filtered_intersecting_segments_per_unique_intersection_location = filter_based_on_filter_dict(
        intersection_dict=filtered_intersecting_segments_per_unique_intersection_location,
        filter_dict=filter_dict,
    )

    #######
    # based on the filtered list, determine whether there are intersections
    intersections_found = False
    if (
        filtered_intersecting_segments_per_unique_intersection_location
        and len(filtered_intersecting_segments_per_unique_intersection_location) > 0
    ):
        intersections_found = True

    return (
        intersections_found,
        filtered_intersecting_segments_per_unique_intersection_location,
    )


def combine_intersection_dicts(intersection_dicts, settings):
    """
    Function to combine intersection dicts
    """

    combined_intersection_dict = {}

    #
    for intersection_dict in intersection_dicts:
        for (
            intersection_point,
            intersecting_segment_tuples,
        ) in intersection_dict.items():
            # Add point to combined dict and add empty list
            if not intersection_point in combined_intersection_dict:
                combined_intersection_dict[
                    intersection_point
                ] = intersecting_segment_tuples
            else:
                # Add all unique segment tuples
                for intersecting_segment_tuple in intersecting_segment_tuples:
                    if (
                        not intersecting_segment_tuple
                        in combined_intersection_dict[intersection_point]
                    ):
                        combined_intersection_dict[intersection_point].append(
                            intersecting_segment_tuple
                        )

    return combined_intersection_dict


def check_intersection_with_sweepintersector(
    trajectory_list, angle_threshold, settings, filter_intersection_dicts=None
):
    """
    Function to track intersection of a list of tracks
    """

    start_x_list = []
    start_y_list = []

    end_x_list = []
    end_y_list = []

    ###########
    # Construct the data as start and endpoints
    for track in trajectory_list:
        start_x_list += list(track["x"][:-1])
        start_y_list += list(track["y"][:-1])

        end_x_list += list(track["x"][1:])
        end_y_list += list(track["y"][1:])

    ###########
    # check if there are filter intersection dicts
    if not filter_intersection_dicts is None:
        combined_filter_intersection_dict = combine_intersection_dicts(
            intersection_dicts=filter_intersection_dicts, settings=settings
        )
    else:
        combined_filter_intersection_dict = None

    ###########
    # call to sweep intersector
    intersection_found, intersection_dict = sweepintersector(
        start_x_list=start_x_list,
        start_y_list=start_y_list,
        end_x_list=end_x_list,
        end_y_list=end_y_list,
        angle_threshold=angle_threshold,
        settings=settings,
        filter_dict=combined_filter_intersection_dict,
    )

    return intersection_found, intersection_dict


def prepare_trajectory(trajectory_dict, settings):
    """
    Function to prepare the trajectory data:
    - extract the trajectory
    - thin the trajectory
    - provide correct structure
    """

    # Extract the trajectory
    trajectory = trajectory_dict["result_summary"]["positions"]

    # Thin trajectory
    trajectory = thin_trajectory(trajectory=trajectory, settings=settings)

    # Create correct structure for array
    trajectory_restructured = {"x": trajectory[:, 0], "y": trajectory[:, 1]}

    return trajectory_restructured


def get_all_self_and_other_intersections(trajectory_set, settings):
    """
    Function to get all the self and other intersections in the form of two combined dictionaries
    """

    all_self_intersections = []
    all_other_intersections = []

    for trajectory_dict in trajectory_set:
        if trajectory_dict["intersection_data"]["self_intersection_data"][
            "self_intersection_found"
        ]:
            all_self_intersections.append(
                trajectory_dict["intersection_data"]["self_intersection_data"][
                    "self_intersection_dict"
                ]
            )

        if trajectory_dict["intersection_data"]["other_intersection_data"][
            "other_intersection_found"
        ]:
            for intersection in trajectory_dict["intersection_data"][
                "other_intersection_data"
            ]["other_intersections_list"]:
                all_other_intersections.append(intersection["intersection_dict"])

    #
    combined_all_self_intersections = combine_intersection_dicts(
        intersection_dicts=all_self_intersections, settings=settings
    )
    combined_all_other_intersections = combine_intersection_dicts(
        intersection_dicts=all_other_intersections, settings=settings
    )

    return combined_all_self_intersections, combined_all_other_intersections


def handle_intersecting_trajectories(trajectory_set, settings):
    """
    Main function for the intersection of the trajectories
    """

    # Extra information about the intersections
    num_trajectories = len(trajectory_set)

    # Create specific dict to store the intersection
    for trajectory_i, trajectory_dict in enumerate(trajectory_set):
        trajectory_dict["intersection_data"] = {
            "other_intersection_data": {
                "other_intersections_list": [],
                "other_intersection_found": False,
            },
            "self_intersection_data": {
                "self_intersection_found": False,
                "self_intersection_dict": {},
            },
        }

        # if the trajectory is actually terminated then we should skip this one.
        if trajectory_dict["result_dict"]["terminated"] == 1:
            continue

        #
        trajectory_dict["prepared_trajectory"] = prepare_trajectory(
            trajectory_dict=trajectory_dict, settings=settings
        )

    #
    total_weight_trajectory_set = np.sum(
        [
            trajectory_dict["result_summary"]["weight"]
            for trajectory_dict in trajectory_set
        ]
    )

    ########
    # Calculate self-intersections:
    #   - Pass single trajectory and calculate intersection properties
    for trajectory_dict in trajectory_set:
        # if the trajectory is actually terminated then we should skip this one.
        if trajectory_dict["result_dict"]["terminated"] == 1:
            continue

        # Create trajectory list
        current_trajectory_list = [trajectory_dict["prepared_trajectory"]]

        # Find the self-intersections
        (
            self_intersection_found,
            self_intersection_dict,
        ) = check_intersection_with_sweepintersector(
            trajectory_list=current_trajectory_list,
            angle_threshold=settings[
                "self_intersection_angle_threshold"
            ],  # NOTE: self-intersections are stricter
            settings=settings,
        )

        # store results
        if self_intersection_found:
            trajectory_dict["intersection_data"]["self_intersection_data"][
                "self_intersection_found"
            ] = True
            trajectory_dict["intersection_data"]["self_intersection_data"][
                "self_intersection_dict"
            ] = self_intersection_dict

    ########
    # Calculate other-intersections
    #   - create arrays to store data in
    #   - calculate intersections in lower-triangle
    #   - store results in both upper and lower triangle
    #   - calculate aggregate results

    # Set up arrays
    intersection_boolean_array = np.zeros((num_trajectories, num_trajectories))
    intersection_weight_array = np.zeros((num_trajectories, num_trajectories))
    intersection_data_list = create_list_of_lists(
        shape=(num_trajectories, num_trajectories), default_value={}
    )

    # construct combinations
    trajectory_index_combinations = list(combinations(range(num_trajectories), 2))

    # loop over all combinations
    for trajectory_index_combination in trajectory_index_combinations:
        #
        index_left = trajectory_index_combination[0]
        index_right = trajectory_index_combination[1]

        #
        trajectory_dict_left = trajectory_set[index_left]
        trajectory_dict_right = trajectory_set[index_right]

        # if the trajectory is actually terminated then we should skip this one.
        if trajectory_dict_left["result_dict"]["terminated"] == 1:
            continue
        if trajectory_dict_right["result_dict"]["terminated"] == 1:
            continue

        #
        weight_left = trajectory_dict_left["result_summary"]["weight"]
        weight_right = trajectory_dict_right["result_summary"]["weight"]

        # construct trajectory list
        current_trajectory_list = [
            trajectory_dict_left["prepared_trajectory"],
            trajectory_dict_right["prepared_trajectory"],
        ]

        # Find the intersections
        (
            intersection_found,
            intersection_dict,
        ) = check_intersection_with_sweepintersector(
            trajectory_list=current_trajectory_list,
            angle_threshold=settings["intersection_angle_threshold"],
            settings=settings,
            filter_intersection_dicts=[
                trajectory_dict_left["intersection_data"]["self_intersection_data"][
                    "self_intersection_dict"
                ],
                trajectory_dict_right["intersection_data"]["self_intersection_data"][
                    "self_intersection_dict"
                ],
            ],
        )

        #####
        # fill the data in the arrays

        # store booleans
        intersection_boolean_array[index_left][index_right] = intersection_found
        intersection_boolean_array[index_right][index_left] = intersection_found

        if intersection_found:
            # store intersection data
            intersection_data_list[index_left][index_right] = intersection_dict
            intersection_data_list[index_right][
                index_left
            ] = intersection_dict  # TODO: consider reversing all the tuples

            #
            trajectory_dict_left["intersection_data"]["other_intersection_data"][
                "other_intersections_list"
            ].append(
                {
                    "index": index_right,
                    "intersection_dict": intersection_dict,
                    "weight": weight_right,
                }
            )
            trajectory_dict_right["intersection_data"]["other_intersection_data"][
                "other_intersections_list"
            ].append(
                {
                    "index": index_left,
                    "intersection_dict": intersection_dict,
                    "weight": weight_left,
                }
            )

            #
            trajectory_dict_left["intersection_data"]["other_intersection_data"][
                "other_intersection_found"
            ] = True
            trajectory_dict_right["intersection_data"]["other_intersection_data"][
                "other_intersection_found"
            ] = True

    #############
    # after the loop we need to store the summary of all the intersection data (i.e. sum columns etc)
    return trajectory_set
