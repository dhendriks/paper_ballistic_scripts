import numpy as np
from ballistic_integration_code.scripts.L1_stream.functions.intersection.utility import (
    calculate_angle_segments,
)


def filter_based_on_angle(intersection_dict, angle_threshold):
    """
    Function to filter out intersecting segments based on the angle of incidence of the intersection.
    """

    #######
    # Calculate the angle of intersection and filter out the segment-pairs that intersect at an angle below a certain threshold
    filtered_intersection_dict = {}

    #
    for point, segment_tuple_list in intersection_dict.items():
        for segment_tuple in segment_tuple_list:
            # calculate angle between the lines
            angle_segments = calculate_angle_segments(
                segment1=segment_tuple[0], segment2=segment_tuple[1]
            )
            # print("Intersection point: {} Segment tuple: {}\n\tangle: {}".format(point, segment_tuple, angle_segments))

            # Check if the reverse of the angle is shallower. If so, use that
            # NOTE: not sure if we should do this.
            angle_reverse = angle_segments - 360
            if np.abs(angle_reverse) < angle_segments:
                angle_segments = np.abs(angle_reverse)

            # Check if the angle exceeds the threshold
            if angle_segments > angle_threshold:
                # print("\tAngle exceeds allowed threshold")

                if not point in filtered_intersection_dict:
                    filtered_intersection_dict[point] = []
                filtered_intersection_dict[point].append(segment_tuple)

    return filtered_intersection_dict


def filter_based_on_filter_dict(intersection_dict, filter_dict):
    """
    Function to filter the intersection dict based on a different dict that contains intersection points and segments.

    Mostly used to filter out the self-intersections
    """

    # if the filter dict doesn't exist, just return the original
    if filter_dict is None:
        return intersection_dict

    # actually perform filter
    filtered_intersection_dict = {}

    # Check if the intersection point is present in the filter dict
    for intersection_point, intersecting_segment_tuples in intersection_dict.items():
        # if not, just add all the segment tuples
        if intersection_point not in filter_dict:
            filtered_intersection_dict[intersection_point] = intersecting_segment_tuples

        # if it is, then look at the segment tuples
        else:
            for intersecting_segment_tuple in intersecting_segment_tuples:
                add_to_dict = True

                # check if the tuple (or its reverse) exists in the tuple list of the filter dict
                if intersecting_segment_tuple in filter_dict[intersection_point]:
                    add_to_dict = False
                if intersecting_segment_tuple[::-1] in filter_dict[intersection_point]:
                    add_to_dict = False

                # add
                if add_to_dict:
                    if not intersection_point in filtered_intersection_dict:
                        filtered_intersection_dict[intersection_point] = []
                    filtered_intersection_dict[intersection_point].append(
                        intersecting_segment_tuple
                    )

    return filtered_intersection_dict
