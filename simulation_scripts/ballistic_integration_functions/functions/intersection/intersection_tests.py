"""
Testing the intersection functionality
"""

import unittest

import numpy as np
from ballistic_integration_code.scripts.L1_stream.functions.intersection.intersection_detection import (
    check_intersection_with_sweepintersector,
)


class test_self_intersection(unittest.TestCase):
    """ """

    def test_self_intersection_tmp(self):
        #
        trajectory_list = [
            {
                "x": np.array([0, 1, 0, 1, 2, 2, 1, 1, 1.9]),
                "y": np.array([1, 0, 0, 1, 2, 1, 2, 1.5, 1.5]),
            }
        ]

        # calculate the locations of intersection
        (
            intersections_found,
            intersection_dict,
        ) = check_intersection_with_sweepintersector(
            trajectory_list=trajectory_list, angle_threshold=0, settings={}
        )

        #####
        # First location (one set of segments intersect here)
        # Check intersection point
        self.assertIn((0.5, 0.5), intersection_dict)

        # check tuple of segments
        self.assertIn(
            (((0.0, 0.0), (1.0, 1.0)), ((0.0, 1.0), (1.0, 0.0))),
            intersection_dict[(0.5, 0.5)],
        )

        #####
        # second point
        self.assertIn((1.5, 1.5), intersection_dict)

        # check tuple of segments
        self.assertIn(
            (((1.0, 1.0), (2.0, 2.0)), ((1.0, 1.5), (1.9, 1.5))),
            intersection_dict[(1.5, 1.5)],
        )
        self.assertIn(
            (((1.0, 1.0), (2.0, 2.0)), ((2.0, 1.0), (1.0, 2.0))),
            intersection_dict[(1.5, 1.5)],
        )
        self.assertIn(
            (((1.0, 1.5), (1.9, 1.5)), ((2.0, 1.0), (1.0, 2.0))),
            intersection_dict[(1.5, 1.5)],
        )

        #

    # def test_angle_filter(self):
    #     """
    #     TODO: implement function to handle filtering based on the angle
    #     """

    def test_both(self):
        """
        Function to test a set of tracks with both 2 self intersecting tracks that also intersect with eachother
        """

        #############
        # Set up track one
        track_one = {
            "x": np.array([1, 2, 1, 3]),
            "y": np.array([2, 1, 1, 3]),
        }

        # calculate the locations of intersection
        (
            track_one_intersections_found,
            track_one_intersection_dict,
        ) = check_intersection_with_sweepintersector(
            trajectory_list=[track_one], angle_threshold=0, settings={}
        )

        #
        self.assertTrue(track_one_intersections_found)
        self.assertIn((1.5, 1.5), track_one_intersection_dict)
        self.assertIn(
            (((1, 1), (3, 3)), ((1, 2), (2, 1))),
            track_one_intersection_dict[(1.5, 1.5)],
        )

        #############
        # Set up track one
        track_two = {
            "x": np.array([4, 3, 4, 2]),
            "y": np.array([2, 1, 1, 3]),
        }

        # calculate the locations of intersection
        (
            track_two_intersections_found,
            track_two_intersection_dict,
        ) = check_intersection_with_sweepintersector(
            trajectory_list=[track_two], angle_threshold=0, settings={}
        )

        #
        self.assertTrue(track_two_intersections_found)
        self.assertIn((3.5, 1.5), track_two_intersection_dict)
        self.assertIn(
            (((4, 2), (3, 1)), ((4, 1), (2, 3))),
            track_two_intersection_dict[(3.5, 1.5)],
        )

        ############
        # Set up intersection with eachother but don't filter out the self-intersections
        (
            unfiltered_intersections_found,
            unfiltered_intersection_dict,
        ) = check_intersection_with_sweepintersector(
            trajectory_list=[track_one, track_two],
            angle_threshold=0,
            settings={},
        )

        #
        self.assertTrue(unfiltered_intersections_found)
        self.assertIn((1.5, 1.5), unfiltered_intersection_dict)
        self.assertIn(
            (((1, 1), (3, 3)), ((1, 2), (2, 1))),
            unfiltered_intersection_dict[(1.5, 1.5)],
        )

        #
        self.assertIn((2.5, 2.5), unfiltered_intersection_dict)
        self.assertIn(
            (((1, 1), (3, 3)), ((4, 1), (2, 3))),
            unfiltered_intersection_dict[(2.5, 2.5)],
        )

        #
        self.assertIn((3.5, 1.5), unfiltered_intersection_dict)
        self.assertIn(
            (((4, 1), (2, 3)), ((4, 2), (3, 1))),
            unfiltered_intersection_dict[(3.5, 1.5)],
        )

        ############
        # Set up intersection with eachother and filter out the self-intersections
        (
            filtered_intersections_found,
            filtered_intersection_dict,
        ) = check_intersection_with_sweepintersector(
            trajectory_list=[track_one, track_two],
            angle_threshold=0,
            settings={},
            filter_intersection_dicts=[
                track_one_intersection_dict,
                track_two_intersection_dict,
            ],
        )

        #
        self.assertTrue(filtered_intersections_found)
        self.assertIn((2.5, 2.5), filtered_intersection_dict)
        self.assertIn(
            (((1, 1), (3, 3)), ((4, 1), (2, 3))), filtered_intersection_dict[(2.5, 2.5)]
        )

        ############
        # Set up intersection with eachother but don't filter out the self-intersections
        (
            dummy_intersections_found,
            dummy_intersection_dict,
        ) = check_intersection_with_sweepintersector(
            trajectory_list=[track_one, track_two],
            angle_threshold=0,
            settings={},
            filter_intersection_dicts=[
                {},
                {},
            ],
        )

        #
        self.assertTrue(dummy_intersections_found)
        self.assertIn((1.5, 1.5), dummy_intersection_dict)
        self.assertIn(
            (((1, 1), (3, 3)), ((1, 2), (2, 1))),
            dummy_intersection_dict[(1.5, 1.5)],
        )

        #
        self.assertIn((2.5, 2.5), dummy_intersection_dict)
        self.assertIn(
            (((1, 1), (3, 3)), ((4, 1), (2, 3))),
            dummy_intersection_dict[(2.5, 2.5)],
        )

        #
        self.assertIn((3.5, 1.5), dummy_intersection_dict)
        self.assertIn(
            (((4, 1), (2, 3)), ((4, 2), (3, 1))),
            dummy_intersection_dict[(3.5, 1.5)],
        )

    def test_other_intersection_only(self):
        """
        Function to test a set of tracks with both 2 self intersecting tracks that also intersect with eachother
        """

        #############
        # Set up track one
        track_one = {
            "x": np.array([1, 3]),
            "y": np.array([1, 3]),
        }

        # calculate the locations of intersection
        (
            track_one_intersections_found,
            track_one_intersection_dict,
        ) = check_intersection_with_sweepintersector(
            trajectory_list=[track_one], angle_threshold=0, settings={}
        )

        #
        self.assertFalse(track_one_intersections_found)

        #############
        # Set up track one
        track_two = {
            "x": np.array([4, 2]),
            "y": np.array([1, 3]),
        }

        # calculate the locations of intersection
        (
            track_two_intersections_found,
            track_two_intersection_dict,
        ) = check_intersection_with_sweepintersector(
            trajectory_list=[track_two], angle_threshold=0, settings={}
        )

        #
        self.assertFalse(track_two_intersections_found)

        ############
        # Set up intersection with eachother and filter out the self-intersections
        (
            filtered_intersections_found,
            filtered_intersection_dict,
        ) = check_intersection_with_sweepintersector(
            trajectory_list=[track_one, track_two],
            angle_threshold=0,
            settings={},
            filter_intersection_dicts=[
                track_one_intersection_dict,
                track_two_intersection_dict,
            ],
        )

        #
        self.assertTrue(filtered_intersections_found)
        self.assertIn((2.5, 2.5), filtered_intersection_dict)
        self.assertIn(
            (((1, 1), (3, 3)), ((4, 1), (2, 3))), filtered_intersection_dict[(2.5, 2.5)]
        )

    def test_no_intersection(self):
        """
        Function to test a set of tracks with both 2 self intersecting tracks that also intersect with eachother
        """

        #############
        # Set up track one
        track_one = {
            "x": np.array([0, 1]),
            "y": np.array([0, 1]),
        }

        # calculate the locations of intersection
        (
            track_one_intersections_found,
            track_one_intersection_dict,
        ) = check_intersection_with_sweepintersector(
            trajectory_list=[track_one], angle_threshold=0, settings={}
        )

        #
        self.assertFalse(track_one_intersections_found)

        #############
        # Set up track one
        track_two = {
            "x": np.array([3, 2]),
            "y": np.array([0, 1]),
        }

        # calculate the locations of intersection
        (
            track_two_intersections_found,
            track_two_intersection_dict,
        ) = check_intersection_with_sweepintersector(
            trajectory_list=[track_two], angle_threshold=0, settings={}
        )

        #
        self.assertFalse(track_two_intersections_found)

        ############
        # Set up intersection with eachother and filter out the self-intersections
        (
            intersections_found,
            intersection_dict,
        ) = check_intersection_with_sweepintersector(
            trajectory_list=[track_one, track_two],
            angle_threshold=0,
            settings={},
            filter_intersection_dicts=[
                track_one_intersection_dict,
                track_two_intersection_dict,
            ],
        )

        self.assertFalse(intersections_found)
        self.assertFalse(intersection_dict)


if __name__ == "__main__":
    unittest.main()
