"""
Utility functions for the intersection calculations
TODO: use multidim_dot from angular_momentum_calculation file
"""

import numpy as np


def calculate_angle_segments(segment1, segment2):
    """
    Function to calculate the angle between two line segments.

    Returns the angle in degrees

    # TODO: can we create a method to do this in 1 go for arrays of segments?
    """

    # make vectors
    vec_1 = (segment1[1][0] - segment1[0][0], segment1[1][1] - segment1[0][1])
    vec_2 = (segment2[1][0] - segment2[0][0], segment2[1][1] - segment2[0][1])

    # calculate angle
    angle = np.arccos(
        (np.dot(vec_1, vec_2)) / (np.linalg.norm(vec_1) * np.linalg.norm(vec_2))
    )

    return np.rad2deg(angle)


def calculate_angle_segments_vectorized(segment_arr):
    """
    Function to calculate the angle between two line segments.

    Returns the angle in degrees
    """

    #
    segment1_arr = segment_arr[:-1]
    segment2_arr = segment_arr[1:]

    # Create the vectors
    vec_1_arr = np.zeros((len(segment1_arr), 2))
    vec_1_arr[:, 0] = segment1_arr[:, 1, 0] - segment1_arr[:, 0, 0]
    vec_1_arr[:, 1] = segment1_arr[:, 1, 1] - segment1_arr[:, 0, 1]

    vec_2_arr = np.zeros((len(segment1_arr), 2))
    vec_2_arr[:, 0] = segment2_arr[:, 1, 0] - segment2_arr[:, 0, 0]
    vec_2_arr[:, 1] = segment2_arr[:, 1, 1] - segment2_arr[:, 0, 1]

    # calculate lengths
    len_vec_1_arr = np.linalg.norm(vec_1_arr, axis=1)
    len_vec_2_arr = np.linalg.norm(vec_2_arr, axis=1)

    # Calculate dotproduct
    dot_arr = (vec_1_arr * vec_2_arr).sum(axis=1)

    # calculate angle
    angle_arr = np.arccos((dot_arr) / (len_vec_1_arr * len_vec_2_arr))

    # Convert to deg
    deg_arr = np.rad2deg(angle_arr)

    return deg_arr


def create_list_of_lists(shape, default_value):
    """
    Function to create an empty list structure
    """

    list_of_lists = []

    for i in range(shape[0]):
        listo = []
        for j in range(shape[1]):
            listo.append(default_value)
        list_of_lists.append(listo)
    return list_of_lists
