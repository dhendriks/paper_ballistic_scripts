"""
Functions to handle some of the data header building things
"""

import numpy as np
from ballistic_integration_code.scripts.L1_stream.functions.build_description_headerstring import (
    build_description_headerstring,
)


def get_index_dict(df, columns, offset):
    """
    Function to write get the index dictionary of the columns

    Offset can be added to shift the values. Offset is *added* to the index, so for a negative shift provide negative offset values
    """

    index_dict = {}

    for col in columns:
        index = list(df.columns).index(col) + offset
        index_dict[col] = index

    return index_dict


def data_header_write_input_defines(
    parameter_list, index_dict, filehandle, definition_basename
):
    """
    Function to write the input defines
    """

    # Set the input index basename
    input_index_basename = "{}_TABLE_INPUT_INDEX".format(definition_basename)

    # Loop over parameters
    for parameter in parameter_list:
        filehandle.write(
            "# define {}_{} {}\n\n".format(
                input_index_basename, parameter.upper(), index_dict[parameter]
            )
        )


def data_header_write_output_defines(
    settings, parameter_list, index_dict, filehandle, definition_basename
):
    """
    Function to write the output defines

    NOTE: if we include the interpolation radii we change the writing from ..INDEX_RADIUS_0 X to INDEX_RADIUS_MIN X, to only include the edge points
    """

    # Set the input index basename
    output_index_basename = "{}_TABLE_OUTPUT_INDEX".format(definition_basename)

    # Loop over parameters
    if not settings["specific_angular_momentum_stream_interpolation_enabled"]:
        for parameter in parameter_list:
            filehandle.write(
                "# define {}_{} {}\n\n".format(
                    output_index_basename, parameter.upper(), index_dict[parameter]
                )
            )

    else:
        if settings["specific_angular_momentum_stream_interpolation_enabled"]:
            interpolation_radius_fractions = np.linspace(
                settings["specific_angular_momentum_stream_interpolation_lower_bound"],
                settings["specific_angular_momentum_stream_interpolation_upper_bound"],
                settings["specific_angular_momentum_stream_interpolation_num_radii"],
            )

        for parameter in parameter_list:
            if not parameter.startswith("interpolation_radius"):
                filehandle.write(
                    "# define {}_{} {}\n\n".format(
                        output_index_basename, parameter.upper(), index_dict[parameter]
                    )
                )
            else:
                if parameter.endswith("0"):
                    filehandle.write(
                        "# define {}_{} {}\n\n".format(
                            output_index_basename,
                            "interpolation_radius_min".upper(),
                            index_dict[parameter],
                        )
                    )
                elif parameter.endswith(
                    str(
                        settings[
                            "specific_angular_momentum_stream_interpolation_num_radii"
                        ]
                        - 1
                    )
                ):
                    filehandle.write(
                        "# define {}_{} {}\n\n".format(
                            output_index_basename,
                            "interpolation_radius_max".upper(),
                            index_dict[parameter],
                        )
                    )


def data_header_write_data_array(
    df, length_decimals, num_lines, filehandle, definition_basename
):
    """
    Function to handle the writing of the data array
    """

    # Add start for the data array
    filehandle.write("# define {}_TABLE_DATA \\\n".format(definition_basename))

    # Write data to file
    for lineno, line in enumerate(df.values.tolist()):
        termination = ",\\\n" if not lineno == num_lines - 1 else "\n"
        outputline = (
            ",".join([str(round(el, length_decimals)) for el in line]) + termination
        )

        filehandle.write(outputline)
    print("Wrote data header")


def data_header_write_meta_data(
    settings,
    input_parameter_list,
    output_parameter_list,
    parameter_dict,
    stage_number,
    num_lines,
    num_input_parameters,
    num_output_parameters,
    filehandle,
    definition_basename,
    comment_symbol="//",
    write_defines=True,
):
    """
    Function to write the meta data for the header
    """

    # Write a description header
    headerstring = build_description_headerstring(
        input_parameters=input_parameter_list,
        output_parameters=output_parameter_list,
        parameter_dict=parameter_dict,
        comment_symbol=comment_symbol,
    )
    filehandle.write(headerstring + "\n")

    if write_defines:
        # Write the stage name and some metadata
        filehandle.write(
            "# define {}_STAGE_{}\n\n".format(definition_basename, stage_number)
        )
        filehandle.write(
            "# define {}_TABLE_NUM_DATALINES {}\n\n".format(
                definition_basename, num_lines
            )
        )
        filehandle.write(
            "# define {}_TABLE_NUM_INPUT_PARAMETERS {}\n\n".format(
                definition_basename, num_input_parameters
            )
        )
        filehandle.write(
            "# define {}_TABLE_NUM_OUTPUT_PARAMETERS {}\n\n".format(
                definition_basename, num_output_parameters
            )
        )

    #################
    # Add stream interpolation radii to output parameters if flag is set
    if settings["specific_angular_momentum_stream_interpolation_enabled"]:
        interpolation_radius_fractions = np.linspace(
            settings["specific_angular_momentum_stream_interpolation_lower_bound"],
            settings["specific_angular_momentum_stream_interpolation_upper_bound"],
            settings["specific_angular_momentum_stream_interpolation_num_radii"],
        )

        # # loop over stream interpolation radii
        # for stream_interpolation_radius_i in range(
        #     settings["specific_angular_momentum_stream_interpolation_num_radii"]
        # ):
        #     interpolation_radius_i_string = "interpolation_radius_{}".format(
        #         stream_interpolation_radius_i
        #     )
        #     interpolation_radius_fraction = interpolation_radius_fractions[
        #         stream_interpolation_radius_i
        #     ]

        #     # Write to file
        #     filehandle.write(
        #         "# define {}_{} {}\n\n".format(
        #             definition_basename,
        #             interpolation_radius_i_string.upper(),
        #             interpolation_radius_fraction,
        #         )
        #     )

        if write_defines:
            # Write min and max and number
            filehandle.write(
                "# define {}_{} {}\n\n".format(
                    definition_basename,
                    "interpolation_radius_min".upper(),
                    interpolation_radius_fractions[0],
                )
            )
            filehandle.write(
                "# define {}_{} {}\n\n".format(
                    definition_basename,
                    "interpolation_radius_max".upper(),
                    interpolation_radius_fractions[-1],
                )
            )
            filehandle.write(
                "# define {}_{} {}\n\n".format(
                    definition_basename,
                    "interpolation_radius_num".upper(),
                    len(interpolation_radius_fractions),
                )
            )
