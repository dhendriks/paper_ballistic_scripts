"""
Function to plot the
"""

import astropy.constants as const
import astropy.units as u
import numpy as np
from ballistic_integration_code.scripts.L1_stream.functions.mass_stream_area_functions import (
    calculate_distance_donor_to_l1,
    calculate_normalised_mass_stream_area,
)
from ballistic_integration_code.settings import standard_system_dict
from ballistic_integrator.functions.frame_of_reference.center_of_mass import (
    calculate_position_donor,
)
from ballistic_integrator.functions.frame_of_reference.wrapper import (
    calculate_lagrange_points,
)

MU_PHOT = 1


def calculate_grav_potential_factor(dl1, q_acc):
    """
    Function to calculate the gravitational potential factor
    """

    donor_term = 1.0 / np.power(dl1, 3)
    accretor_term = q_acc / np.power(1 - dl1, 3)

    return donor_term + accretor_term


def calculate_vtherm(T):
    """
    test function for calculate_vtherm
    """

    k = const.k_B
    ma = const.u

    vtherm = np.sqrt((3 * k * T) / (MU_PHOT * ma))

    return vtherm.si


def calculate_normalised_thermal_velocity(donor_temperature, separation, omega):
    """
    Function to calculate the normalized thermal velocity based
    """

    thermal_velocity = calculate_vtherm(T=donor_temperature)

    normalised_thermal_velocity = thermal_velocity / (separation * omega)

    return normalised_thermal_velocity.si


def calculate_A_stream(settings, T, macc, mdon, omega, f_sync, separation):
    """
    test function to calculate A_stream
    """

    q_acc = macc / mdon
    q_don = mdon / macc
    mu_acc = macc / (macc + mdon)

    #
    distance_donor_to_l1 = calculate_distance_donor_to_l1(settings)

    ####
    # calculate the terms for the mass stream area and combine
    grav_potential_factor = calculate_grav_potential_factor(
        dl1=distance_donor_to_l1, q_acc=q_acc
    )

    # geometry term
    geometry_term = np.power(
        grav_potential_factor
        * (grav_potential_factor - (1 + q_acc) * np.power(f_sync, 2)),
        -0.5,
    )

    #
    molecular_mass = MU_PHOT * const.u
    specific_gas_constant = const.k_B / molecular_mass

    # SEE p4 of ritter. that has the correct one.

    # my version A_stream = (2 * np.pi * R * T / MU_PHOT) * (q_acc / (mu_acc * omega**2)) * geometry_term
    # davis version  A_stream = (2 * np.pi * R * T / (MU_PHOT * const.u)) * ((q_don * separation ** 3) / (const.G * mdon)) * geometry_term
    # MAYBE CORRECT?    A_stream = (2 * np.pi * const.k_B * T / (MU_PHOT * const.u)) * ((q_don * separation ** 3) / (const.G * mdon)) * geometry_term

    A_stream = (
        (2 * np.pi * specific_gas_constant * T)
        * ((q_don * separation**3) / (const.G * mdon))
        * geometry_term
    )

    return A_stream.si


def calculate_normalised_A_stream(settings, T, macc, mdon, omega, f_sync, separation):
    """
    Function to calculate the normalised stream area
    """

    A_stream = calculate_A_stream(
        settings=settings,
        T=T,
        macc=macc,
        mdon=mdon,
        omega=omega,
        f_sync=f_sync,
        separation=separation,
    )

    normalised_A_stream = A_stream / (separation**2)

    return normalised_A_stream.si


universal_gas_constant = const.R
molar_mass = u.kg / u.mol
specific_gas_constant = universal_gas_constant / molar_mass

# quit()

######
# Define system properties
settings = {
    "include_asynchronicity_donor_for_lagrange_points": True,
    "frame_of_reference": "center_of_mass",
}

#
synchronicity_factor = 1
T = 1000 * u.K
mdon = 1 * u.Msun
macc = 1 * u.Msun
separation = 100 * u.Rsun

omega = np.sqrt(const.G * (mdon + macc) / (separation**3)).si
massratio_accretor_donor = macc.value / mdon.value

# Set system dict
settings["system_dict"] = {
    **standard_system_dict,
    "synchronicity_factor": synchronicity_factor,
    "mass_accretor": massratio_accretor_donor * standard_system_dict["mass_donor"],
}

# Calculate velocities
vtherm = calculate_vtherm(T)
normalised_vtherm = calculate_normalised_thermal_velocity(
    donor_temperature=T, separation=separation, omega=omega
)

# Set grid point
settings["grid_point"] = {
    "massratio_accretor_donor": massratio_accretor_donor,
    "synchronicity_factor": synchronicity_factor,
    "log10normalized_thermal_velocity_dispersion": np.log10(normalised_vtherm),
}


# Calculate stream areas
A_stream = calculate_A_stream(
    settings=settings,
    T=T,
    macc=macc,
    mdon=mdon,
    omega=omega,
    f_sync=synchronicity_factor,
    separation=separation,
)
normalised_A_stream = calculate_normalised_A_stream(
    settings=settings,
    T=T,
    macc=macc,
    mdon=mdon,
    omega=omega,
    f_sync=synchronicity_factor,
    separation=separation,
)

# Other formula
normalised_A_stream_formula = calculate_normalised_mass_stream_area(settings=settings)
A_stream_formula = normalised_A_stream_formula * (separation**2)
# print(A_stream_formula)

# Print stuff
print("Thermal velocity: {} normalised: {}".format(vtherm.si, normalised_vtherm))
print("Stream area: {} normalised: {}".format(A_stream.si, normalised_A_stream))
print(
    "formula Stream area: {} normalised: {}".format(
        A_stream_formula.si, normalised_A_stream_formula
    )
)

# TODO:
# print(A_stream.to(u.Rsun**2))
# H^{2} * omega_ORB / vs^{2}: what is that?
