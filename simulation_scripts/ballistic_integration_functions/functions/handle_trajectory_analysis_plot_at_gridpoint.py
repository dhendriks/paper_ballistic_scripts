"""
Functions to handle plotting the trajectory analysis

TODO: think of a method to reliably plot the 'average' evolution of a value (importantly: each trajectory is not equal length in steps etc)
TODO: add plot for accelarations
TODO: make the colors of the individual trajectories different st we can distinguish between plots
"""

import os
import shutil

import matplotlib.pyplot as plt
import numpy as np
from ballistic_integration_code.scripts.L1_stream.functions.angular_momentum_calculations import (
    calculate_specific_angular_momenta_with_respect_to,
)
from ballistic_integration_code.scripts.L1_stream.functions.select_trajectory_categories import (
    select_trajectory_categories,
)
from ballistic_integrator.functions.frame_of_reference import (
    calculate_position_accretor,
)
from ballistic_integrator.functions.frame_of_reference.center_of_mass import (
    calculate_position_accretor,
    calculate_position_donor,
)
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.pdf_functions import (
    add_pdf_and_bookmark,
    generate_chapterpage_pdf,
)
from david_phd_functions.plotting.utils import (
    align_axes,
    align_both_axes,
    delete_out_of_frame_text,
    show_and_save_plot,
)

#
from PyPDF2 import PdfMerger

custom_mpl_settings.load_mpl_rc()


def generate_trajectory_analysis_plot_filename(gridpoint):
    """
    Function to generate the filename for the trajectory analysis
    """

    """
    Function to generate a filename for the plot for the current gridpoint
    """

    # Construct filename from the relevant grid point properties
    base_string = ""
    for key in sorted(gridpoint.keys()):
        base_string += "trajectory_analysis_{}_{}_".format(key, gridpoint[key])

    return base_string + ".pdf"


def plot_accelarations(trajectory_list, plot_settings):
    """
    Function to plot the accelarations of the particles
    """

    plot_kwargs = plot_settings["plot_kwargs"]

    ##########
    # Set up figure
    fig = plt.figure(figsize=(8, 16))

    # Set up gridspec
    gs = fig.add_gridspec(nrows=2, ncols=1)

    ax_x = fig.add_subplot(gs[0, 0])
    ax_y = fig.add_subplot(gs[1, 0])

    #
    cmap = plt.cm.get_cmap("viridis", len(trajectory_list))
    cmaplist = [cmap(i) for i in range(cmap.N)]
    # cmaplist = [plot_kwargs["color"] for i in range(cmap.N)]

    # loop over trajectories
    for trajectory_i, trajectory in enumerate(trajectory_list):
        # Plot data
        ax_x.plot(
            trajectory["times"],
            trajectory["accelarations"][:, 0],
            alpha=trajectory["weight"],
            color=cmaplist[trajectory_i],
        )
        ax_y.plot(
            trajectory["times"],
            trajectory["accelarations"][:, 1],
            alpha=trajectory["weight"],
            color=cmaplist[trajectory_i],
        )

    ax_x.set_title(
        "category: {}\naccelarations".format(plot_kwargs["label"]), fontsize=26
    )

    ax_x.set_ylabel("accelaration x")
    ax_y.set_ylabel("accelaration y")

    ax_y.set_xlabel("Time [s]")

    # Align axes
    align_both_axes(fig=fig, axes_list=[ax_x, ax_y])

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


def plot_velocities(trajectory_list, plot_settings):
    """
    Function to plot the velocities of the particles
    """

    plot_kwargs = plot_settings["plot_kwargs"]

    ##########
    # Set up figure
    fig = plt.figure(figsize=(8, 16))

    # Set up gridspec
    gs = fig.add_gridspec(nrows=2, ncols=1)

    ax_x = fig.add_subplot(gs[0, 0])
    ax_y = fig.add_subplot(gs[1, 0])

    #
    cmap = plt.cm.get_cmap("viridis", len(trajectory_list))
    cmaplist = [cmap(i) for i in range(cmap.N)]
    # cmaplist = [plot_kwargs["color"] for i in range(cmap.N)]

    # loop over trajectories
    for trajectory_i, trajectory in enumerate(trajectory_list):
        # Plot data
        ax_x.plot(
            trajectory["times"],
            trajectory["velocities"][:, 0],
            alpha=trajectory["weight"],
            color=cmaplist[trajectory_i],
        )
        ax_y.plot(
            trajectory["times"],
            trajectory["velocities"][:, 1],
            alpha=trajectory["weight"],
            color=cmaplist[trajectory_i],
        )

    ax_x.set_title("category: {}\nvelocities".format(plot_kwargs["label"]), fontsize=26)

    ax_x.set_ylabel("velocity x")
    ax_y.set_ylabel("velocity y")

    ax_y.set_xlabel("Time [s]")

    # Align axes
    align_both_axes(fig=fig, axes_list=[ax_x, ax_y])

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


def plot_positions(trajectory_list, plot_settings):
    """
    Function to plot the positions of the particles
    """

    plot_kwargs = plot_settings["plot_kwargs"]

    ##########
    # Set up figure
    fig = plt.figure(figsize=(8, 16))

    # Set up gridspec
    gs = fig.add_gridspec(nrows=2, ncols=1)

    ax_x = fig.add_subplot(gs[0, 0])
    ax_y = fig.add_subplot(gs[1, 0])

    #
    cmap = plt.cm.get_cmap("viridis", len(trajectory_list))
    cmaplist = [cmap(i) for i in range(cmap.N)]
    # cmaplist = [plot_kwargs["color"] for i in range(cmap.N)]

    # loop over trajectories
    for trajectory_i, trajectory in enumerate(trajectory_list):
        # Plot data
        ax_x.plot(
            trajectory["times"],
            trajectory["positions"][:, 0],
            alpha=trajectory["weight"],
            color=cmaplist[trajectory_i],
        )
        ax_y.plot(
            trajectory["times"],
            trajectory["positions"][:, 1],
            alpha=trajectory["weight"],
            color=cmaplist[trajectory_i],
        )

    ax_x.set_title("category: {}\npositions".format(plot_kwargs["label"]), fontsize=26)

    ax_x.set_ylabel("Position x")
    ax_y.set_ylabel("Position y")

    ax_y.set_xlabel("Time [s]")

    # Align axes
    align_both_axes(fig=fig, axes_list=[ax_x, ax_y])

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


def plot_specific_angular_momenta(trajectory_list, plot_settings):
    """
    Function to plot the specific angular momenta
    """

    plot_kwargs = plot_settings["plot_kwargs"]

    ##########
    # Set up figure
    fig = plt.figure(figsize=(8, 24))

    # Set up gridspec
    gs = fig.add_gridspec(nrows=3, ncols=1)

    ax_acc = fig.add_subplot(gs[0, 0])
    ax_don = fig.add_subplot(gs[1, 0])
    ax_com = fig.add_subplot(gs[2, 0])

    #
    cmap = plt.cm.get_cmap("viridis", len(trajectory_list))
    cmaplist = [cmap(i) for i in range(cmap.N)]
    # cmaplist = [plot_kwargs["color"] for i in range(cmap.N)]

    ##########
    # loop over trajectories
    for trajectory_i, trajectory in enumerate(trajectory_list):
        system_dict = trajectory["settings"]["system_dict"]
        frame_of_reference = trajectory["settings"]["frame_of_reference"]
        position_vector = trajectory["positions"]
        velocity_vector = trajectory["velocities"]

        ##########
        # calculate specific angular momenta

        # wrt accretor
        angular_momenta_wrt_accretor = (
            calculate_specific_angular_momenta_with_respect_to(
                to="accretor",
                reference_frame=frame_of_reference,
                system_dict=system_dict,
                position_vector=position_vector,
                velocity_vector=velocity_vector,
            )
        )

        ax_acc.plot(
            trajectory["times"],
            angular_momenta_wrt_accretor,
            alpha=trajectory["weight"],
            color=cmaplist[trajectory_i],
        )

        # wrt donor
        angular_momenta_wrt_donor = calculate_specific_angular_momenta_with_respect_to(
            to="donor",
            reference_frame=frame_of_reference,
            system_dict=system_dict,
            position_vector=position_vector,
            velocity_vector=velocity_vector,
        )

        ax_don.plot(
            trajectory["times"],
            angular_momenta_wrt_donor,
            alpha=trajectory["weight"],
            color=cmaplist[trajectory_i],
        )

        # wrt center of mass
        angular_momenta_wrt_com = calculate_specific_angular_momenta_with_respect_to(
            to="center_of_mass",
            reference_frame=frame_of_reference,
            system_dict=system_dict,
            position_vector=position_vector,
            velocity_vector=velocity_vector,
        )

        ax_com.plot(
            trajectory["times"],
            angular_momenta_wrt_com,
            alpha=trajectory["weight"],
            color=cmaplist[trajectory_i],
        )

    #
    ax_acc.set_ylabel(r"$h_{\mathrm{acc}}$")
    ax_don.set_ylabel(r"$h_{\mathrm{don}}$")
    ax_com.set_ylabel(r"$h_{\mathrm{com}}$")

    ax_com.set_xlabel("Times [s]")

    # Set title
    ax_acc.set_title(
        "category: {}\nspecific angular momenta".format(plot_kwargs["label"]),
        fontsize=26,
    )

    # Align all axes
    # align_both_axes(fig=fig, axes_list=[ax_acc, ax_don, ax_com])
    align_axes(fig=fig, axes_list=[ax_acc, ax_don, ax_com], which_axis="x")

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


def plot_torques(trajectory_list, plot_settings):
    """
    Function to plot the specific angular momenta
    """

    plot_kwargs = plot_settings["plot_kwargs"]

    ##########
    # Set up figure
    fig = plt.figure(figsize=(8, 24))

    # Set up gridspec
    gs = fig.add_gridspec(nrows=3, ncols=1)

    ax_acc = fig.add_subplot(gs[0, 0])
    ax_don = fig.add_subplot(gs[1, 0])
    ax_com = fig.add_subplot(gs[2, 0])

    #
    cmap = plt.cm.get_cmap("viridis", len(trajectory_list))
    cmaplist = [cmap(i) for i in range(cmap.N)]
    # cmaplist = [plot_kwargs["color"] for i in range(cmap.N)]

    ##########
    # loop over trajectories
    for trajectory_i, trajectory in enumerate(trajectory_list):
        system_dict = trajectory["settings"]["system_dict"]
        frame_of_reference = trajectory["settings"]["frame_of_reference"]
        position_vector = trajectory["positions"]
        velocity_vector = trajectory["velocities"]

        #
        timesteps = np.diff(trajectory["times"])

        ##########
        # calculate specific angular momenta

        # wrt accretor
        angular_momenta_wrt_accretor = (
            calculate_specific_angular_momenta_with_respect_to(
                to="accretor",
                reference_frame=frame_of_reference,
                system_dict=system_dict,
                position_vector=position_vector,
                velocity_vector=velocity_vector,
            )
        )

        d_angular_momenta_wrt_accretor = np.diff(angular_momenta_wrt_accretor)
        d_angular_momenta_wrt_accretor_dt = d_angular_momenta_wrt_accretor / timesteps

        ax_acc.plot(
            trajectory["times"][:-1],
            d_angular_momenta_wrt_accretor_dt,
            alpha=trajectory["weight"],
            color=cmaplist[trajectory_i],
        )

        # wrt donor
        angular_momenta_wrt_donor = calculate_specific_angular_momenta_with_respect_to(
            to="donor",
            reference_frame=frame_of_reference,
            system_dict=system_dict,
            position_vector=position_vector,
            velocity_vector=velocity_vector,
        )

        d_angular_momenta_wrt_donor = np.diff(angular_momenta_wrt_donor)
        d_angular_momenta_wrt_donor_dt = d_angular_momenta_wrt_donor / timesteps

        ax_don.plot(
            trajectory["times"][:-1],
            d_angular_momenta_wrt_donor_dt,
            alpha=trajectory["weight"],
            color=cmaplist[trajectory_i],
        )

        # wrt center of mass
        angular_momenta_wrt_com = calculate_specific_angular_momenta_with_respect_to(
            to="center_of_mass",
            reference_frame=frame_of_reference,
            system_dict=system_dict,
            position_vector=position_vector,
            velocity_vector=velocity_vector,
        )

        d_angular_momenta_wrt_com = np.diff(angular_momenta_wrt_com)
        d_angular_momenta_wrt_com_dt = d_angular_momenta_wrt_com / timesteps

        ax_com.plot(
            trajectory["times"][:-1],
            d_angular_momenta_wrt_com_dt,
            alpha=trajectory["weight"],
            color=cmaplist[trajectory_i],
        )

    #
    ax_acc.set_ylabel(r"$\tau_{\mathrm{acc}}$")
    ax_don.set_ylabel(r"$\tau_{\mathrm{don}}$")
    ax_com.set_ylabel(r"$\tau_{\mathrm{com}}$")

    ax_com.set_xlabel("Times [s]")

    # Set title
    ax_acc.set_title("category: {}\nTorques".format(plot_kwargs["label"]), fontsize=26)

    # Align all axes
    # align_both_axes(fig=fig, axes_list=[ax_acc, ax_don, ax_com])
    align_axes(fig=fig, axes_list=[ax_acc, ax_don, ax_com], which_axis="x")

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


def handle_trajectory_analysis_plot_at_gridpoint(settings, trajectory_set):
    """
    Main function to handle plotting the trajectory analysis plot at the gridpoint

    Here we set up the figure logic and decide which things we plot

    TODO: function to make a sequence of plots and create a combined pdf out of it.
    """

    # Construct name based on gridpoint
    plot_filename = generate_trajectory_analysis_plot_filename(
        gridpoint=settings["grid_point"]
    )

    # handle creating plot directory
    plot_dir = os.path.join(settings["result_dir"], "trajectory_analysis_plots")
    os.makedirs(plot_dir, exist_ok=True)

    tmp_plot_dir = os.path.join(plot_dir, "tmp")
    os.makedirs(tmp_plot_dir, exist_ok=True)

    # get the trajectory sets
    trajectory_set_per_category = select_trajectory_categories(trajectory_set)

    ###########
    # Set up the pdf stuff
    merger = PdfMerger()
    pdf_page_number = 0

    ###########
    # Loop over all trajectory categories
    for trajectory_category in trajectory_set_per_category:
        if not trajectory_category["trajectory_list"]:
            continue

        print(trajectory_category)
        ###################
        # Common plots

        #########
        # add positions plot
        basename = "{}_positions.pdf".format(trajectory_category["name"])
        fullname = os.path.join(tmp_plot_dir, basename)
        plot_positions(
            trajectory_list=trajectory_category["trajectory_list"],
            plot_settings={
                "show_plot": False,
                "output_name": fullname,
                "plot_kwargs": trajectory_category["plot_kwargs"],
            },
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            fullname,
            pdf_page_number,
            bookmark_text="{}".format(basename[:-4]),
        )

        #########
        # add velocities plot
        basename = "{}_velocities.pdf".format(trajectory_category["name"])
        fullname = os.path.join(tmp_plot_dir, basename)
        plot_velocities(
            trajectory_list=trajectory_category["trajectory_list"],
            plot_settings={
                "show_plot": False,
                "output_name": fullname,
                "plot_kwargs": trajectory_category["plot_kwargs"],
            },
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            fullname,
            pdf_page_number,
            bookmark_text="{}".format(basename[:-4]),
        )

        #########
        # add accelarations plot
        basename = "{}_accelaration.pdf".format(trajectory_category["name"])
        fullname = os.path.join(tmp_plot_dir, basename)
        plot_accelarations(
            trajectory_list=trajectory_category["trajectory_list"],
            plot_settings={
                "show_plot": False,
                "output_name": fullname,
                "plot_kwargs": trajectory_category["plot_kwargs"],
            },
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            fullname,
            pdf_page_number,
            bookmark_text="{}".format(basename[:-4]),
        )

        #########
        # add specific angular momenta plots
        basename = "{}_angular_momenta.pdf".format(trajectory_category["name"])
        fullname = os.path.join(tmp_plot_dir, basename)
        plot_specific_angular_momenta(
            trajectory_list=trajectory_category["trajectory_list"],
            plot_settings={
                "show_plot": False,
                "output_name": fullname,
                "plot_kwargs": trajectory_category["plot_kwargs"],
            },
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            fullname,
            pdf_page_number,
            bookmark_text="{}".format(basename[:-4]),
        )

        #########
        # add torques plots
        basename = "{}_torques.pdf".format(trajectory_category["name"])
        fullname = os.path.join(tmp_plot_dir, basename)
        plot_torques(
            trajectory_list=trajectory_category["trajectory_list"],
            plot_settings={
                "show_plot": False,
                "output_name": fullname,
                "plot_kwargs": trajectory_category["plot_kwargs"],
            },
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            fullname,
            pdf_page_number,
            bookmark_text="{}".format(basename[:-4]),
        )

        ###################
        # Category specific plots
        # TODO: make category specific plots

    ###########
    # Round off the plot
    combined_plot_filename = os.path.join(plot_dir, plot_filename)
    merger.write(combined_plot_filename)
    merger.close()
    print("created {}".format(combined_plot_filename))

    # remove tmp dir
    shutil.rmtree(tmp_plot_dir)

    return combined_plot_filename
