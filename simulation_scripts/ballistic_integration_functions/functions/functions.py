"""
Function for sepinsky based integration
"""

import json
import os

import numpy as np
import pandas as pd
from ballistic_integration_code.scripts.L1_stream.functions.calculate_trajectory_result_summary import (
    readout_resultfile_and_calculate_trajectory_result_summary,
)
from ballistic_integration_code.scripts.L1_stream.project_settings import (
    EXIT_CODE_MESSAGE_DICT,
    EXIT_CODES,
)
from ballistic_integrator.functions.functions import json_encoder
from ballistic_integrator.functions.integrator.trajectory_integrator_class import (
    trajectory_integrator,
)
from ballistic_integrator.functions.lagrange_points.lagrange_points_sepinski import (
    calculate_lagrange_points_sepinsky,
)
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.repo_info.functions import get_git_info_and_time

custom_mpl_settings.load_mpl_rc()


def write_failed_passed_systems(
    grid_point,
    result_summary,
    settings,
    failed_systems_filename,
    passed_systems_filename,
):
    """
    Function to write the failed or passed system to a file
    """

    #######
    # Check if system failed or passed
    if settings["write_failed_passed_systems"]:
        system_failed = (
            result_summary["exit_code"] not in settings["allowed_exit_codes"]
        )

        if system_failed:
            with open(failed_systems_filename, "a+") as f:
                f.write(json.dumps(grid_point) + "\n")
        else:
            with open(passed_systems_filename, "a+") as f:
                f.write(json.dumps(grid_point) + "\n")


def write_settings_to_file(settings, output_filename):
    """
    Function to write the settings to an output file
    """

    # Make dir if not existant
    dirname = os.path.dirname(output_filename)
    os.makedirs(dirname, exist_ok=True)

    # Write settings json
    with open(output_filename, "w") as f:
        f.write(json.dumps(settings, default=json_encoder))


def filter_df_on_exit_codes(dataframe, allowed_exit_codes, fail_silently=False):
    """
    Function to filter based on allowed exit codes
    """

    passed_test = True
    allowed_df = dataframe.query("exit_code in {}".format(allowed_exit_codes))
    not_allowed_df = dataframe.query("exit_code not in {}".format(allowed_exit_codes))

    if len(not_allowed_df.index):
        passed_test = False
        if not fail_silently:
            # Print the exit code, the count, the name and the message
            for exit_code, count in (
                not_allowed_df.groupby(["exit_code"])["exit_code"].count().iteritems()
            ):
                exit_code_string = (
                    "Exit code: {} count: {} name: {} message: {}".format(
                        exit_code,
                        count,
                        EXIT_CODES(exit_code).name,
                        EXIT_CODE_MESSAGE_DICT[exit_code],
                    )
                )

            raise ValueError(
                "Found systems with exit codes that are not allowed:\n\t{}".format(
                    exit_code_string
                )
            )

    return allowed_df, not_allowed_df, passed_test


def readout_grid_result(result_dir):
    """
    Function that reads out the result files of the grid and returns a pandas dataframe that contains all the information
    """

    filter_wrong_exit_codes = True

    # get all datafiles
    datafiles = []
    for file in os.listdir(result_dir):
        if file.endswith(".json"):
            datafiles.append(os.path.join(result_dir, file))

    result_list = []

    for filename in datafiles:
        # Plot the trajectory to check
        # plot_resultfile(filename, plot_settings={"show_plot": True})

        # Readout data and find closest file
        result_dict = readout_resultfile_and_calculate_trajectory_result_summary(
            filename
        )

        if filter_wrong_exit_codes:
            if result_dict["exit_code"] == EXIT_CODES.CUSTOM_ERROR.value:
                result_list.append(result_dict)
            else:
                print(
                    "Found systems that did not exit on custom error or normal finish. Not including current result: {} {}".format(
                        result_dict["exit_code"], EXIT_CODES.CUSTOM_ERROR.value
                    )
                )

    # Convert to pandas and convert to
    df = pd.DataFrame(result_list)
    df["massratio_accretor_donor"] = df["mass_accretor"] / df["mass_donor"]

    # sort and return
    df = df.sort_values(by=["massratio_accretor_donor", "synchronicity_factor"])

    return df


def main_from_L1(
    mass_accretor,
    mass_donor,
    separation,
    extra_settings={},
    custom_termination_routine=None,
):
    """
    Main function to run the trajectory calculator
    """

    ######
    # Initialise

    # Get base position
    L_points = calculate_lagrange_points_sepinsky(
        mass_accretor=mass_accretor, mass_donor=mass_donor, f=1
    )
    l1_pos = L_points["L1"]
    initial_position_base = np.array(l1_pos[:2])

    # Set offset position
    initial_position_offset = np.array(
        extra_settings.get("initial_position_offset", [0.0, 0.0])
    )
    initial_r = initial_position_base + initial_position_offset

    # Set initial velocity
    initial_v = np.array(extra_settings.get("initial_velocity_offset", [0, 0]))

    #####
    # handle filename and directory
    result_dir = extra_settings["result_dir"]
    filename = "L1_f={}_q={}_x={}_v={}.json".format(
        extra_settings["synchronicity_factor"],
        mass_accretor / mass_donor,
        "{},{}".format(initial_r[0], initial_r[1]),
        "{},{}".format(initial_v[0], initial_v[1]),
    )
    full_filename = os.path.join(result_dir, filename)

    # Create directory
    if not os.path.isdir(os.path.abspath(result_dir)):
        os.makedirs(os.path.abspath(result_dir), exist_ok=True)

    #####
    # Handle evolution
    if extra_settings.get("run", True):
        # Set up the integrator
        integrator = trajectory_integrator(
            mass_accretor=mass_accretor,
            mass_donor=mass_donor,
            separation=separation,
            initial_position=initial_r,
            initial_velocity=initial_v,
            control_settings=extra_settings,
        )

        # Set the extra functions
        if extra_settings["use_custom_termination_routine"]:
            integrator.custom_termination_routine = extra_settings[
                "custom_termination_routine"
            ]

        if extra_settings["use_custom_timestep_control_routine"]:
            integrator.custom_timestep_control_routine = extra_settings[
                "custom_timestep_control_routine"
            ]

        # Calculate the trajectory
        result = integrator.evolve()

        with open(full_filename, "w") as f:
            json.dump(result, f, default=json_encoder)

    return full_filename
