"""
Function to build a latex table for the parameters
"""

import os

import pandas as pd
from ballistic_integration_code.scripts.L1_stream.project_settings import parameter_dict


def build_latex_table(
    input_parameters, output_parameters, parameter_dict, output_filename, averaged=False
):
    """
    Function ot build a latex table for the parameters
    """

    # Determin the columns that we include in the table building
    included_columns = ["longname", "description", "reference", "parameter_type"]
    rename_dict = {
        "longname": "Name",
        "description": "Description",
        "reference": "Reference",
        "parameter_type": "Input/Output",
    }

    # Set up the correct dataframes
    input_df = pd.DataFrame.from_records(
        [parameter_dict[input_parameter] for input_parameter in input_parameters]
    )
    input_df["parameter_type"] = "Input"
    output_df = pd.DataFrame.from_records(
        [parameter_dict[output_parameter] for output_parameter in output_parameters]
    )
    output_df["parameter_type"] = "Output"

    # Combine dfs
    combined_df = pd.concat([input_df, output_df], ignore_index=True)

    # Select columns
    combined_df = combined_df[included_columns]

    # Rename columns
    combined_df = combined_df.rename(columns=rename_dict)

    # Create latex string (Set context to not limit the string length)
    column_format = "".join(
        ["p{3.5cm}" if el != "Description" else "p{6cm}" for el in combined_df.columns]
    )
    with pd.option_context("max_colwidth", None):
        latex_string = combined_df.style.hide(axis="index").to_latex(
            label="tab:dummy",
            hrules=True,
            column_format=column_format,
            caption="dummy caption",
        )

    # Write latex string to file
    with open(output_filename, "w") as f:
        f.write(latex_string)
    print("Write description table to {}".format(output_filename))


if __name__ == "__main__":
    input_parameters = ["A_factor", "massratio_accretor_donor"]
    output_parameters = ["rmin", "rcirc"]

    #
    output_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/tables"
    )
    output_filename = os.path.join(output_dir, "dataset_description_table.tex")

    #
    build_latex_table(
        input_parameters=input_parameters,
        output_parameters=output_parameters,
        parameter_dict=parameter_dict,
        output_filename=output_filename,
    )
