"""
Function to handle the plotting of all the trajectories for a given gridpoint
"""

import os

from ballistic_integration_code.scripts.L1_stream.functions.generate_gridpoint_plot_filename import (
    generate_gridpoint_plot_filename,
)
from ballistic_integration_code.scripts.L1_stream.functions.handle_trajectory_analysis_plot_at_gridpoint import (
    handle_trajectory_analysis_plot_at_gridpoint,
)
from ballistic_integration_code.scripts.L1_stream.functions.plot_functions import (
    plot_system,
)
from ballistic_integration_code.scripts.L1_stream.functions.select_trajectory_categories import (
    select_trajectory_categories,
)
from david_phd_functions.plotting.pdf_functions import add_pdf_and_bookmark
from david_phd_functions.plotting.utils import (
    delete_out_of_frame_text,
    show_and_save_plot,
)
from PyPDF2 import PdfMerger


def handle_plotting_at_gridpoint(settings, grid_point, trajectory_set):
    """
    Function to handle the plotting of all the trajectories for the given gridpoint
    """

    ##########
    # Prepare

    # Construct name based on gridpoint
    plot_filename = generate_gridpoint_plot_filename(gridpoint=grid_point)

    # handle creating plot directory
    plot_dir = os.path.join(settings["result_dir"], "plots")
    os.makedirs(plot_dir, exist_ok=True)

    ########
    # Get category sets
    trajectory_sets_per_category = select_trajectory_categories(trajectory_set)

    ##############
    # Handle plotting or catching the exception

    # Handle plotting of the trajectories in the current roche potential
    fig, axes_list = plot_system(
        system_dict=settings["system_dict"],
        frame_of_reference=settings["frame_of_reference"],
        settings=settings,
        plot_trajectory=True,
        resultdata=trajectory_set[0]["result_summary"],
        resultdata_sets=trajectory_sets_per_category,
        plot_rochelobe_equipotential_meshgrid=True,
        add_gradients=False,
        return_fig=True,
        plot_settings={
            "plot_bounds_x": settings["plot_bounds_x"],
            "plot_bounds_y": settings["plot_bounds_y"],
            "plot_resolution": settings["plot_resolution"],
            "star_text_color": "white",
        },
    )

    ######
    # Resize
    for ax in axes_list:
        ax.set_xlim(settings["plot_bounds_x"])
        ax.set_ylim(settings["plot_bounds_y"])

    # Remove text that falls out of frame
    fig, axes_list[0] = delete_out_of_frame_text(fig, axes_list[0])

    # Handle saving
    trajectory_plot_filename = os.path.join(plot_dir, plot_filename)
    show_and_save_plot(
        fig=fig,
        plot_settings={
            "show_plot": False,
            "output_name": trajectory_plot_filename,
        },
        verbose=1 if settings["verbosity"] > 1 else 0,
    )

    ###################
    # Handle plotting of the trajectory
    if settings["generate_trajectory_analysis_plot_at_gridpoint"]:
        trajectory_analysis_plots = handle_trajectory_analysis_plot_at_gridpoint(
            settings=settings,
            trajectory_set=trajectory_set,
        )

    ###################
    # combine plots
    merger = PdfMerger()
    pdf_page_number = 0

    # Add trajectories plots
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        trajectory_plot_filename,
        pdf_page_number,
        bookmark_text="{}".format("Trajectories in roche lobe"),
    )

    # Add analysis plots
    if settings["generate_trajectory_analysis_plot_at_gridpoint"]:
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            trajectory_analysis_plots,
            pdf_page_number,
            add_source_bookmarks=True,
        )

    combined_plot_filename = os.path.join("combined_pdf.pdf")
    merger.write(combined_plot_filename)
    merger.close()
