"""
Functions to handle setting up a routine to re-run the failed systems
"""

import json
import os

from ballistic_integration_code.scripts.L1_stream.functions.plot_functions import (
    plot_system,
)
from ballistic_integration_code.settings import standard_system_dict
from ballistic_integrator.functions.functions import json_encoder


def handle_plotting_at_re_run(
    settings, result, system_dict, full_trajectory_data_filename
):
    """
    Function to handle plotting of system evolution during the re-run
    """

    if settings["plot_system_at_re_run"]:
        with open(full_trajectory_data_filename, "w") as f:
            json.dump(result, f, default=json_encoder)

        # Plot results
        plot_system(
            system_dict=system_dict,
            frame_of_reference=settings["frame_of_reference"],
            plot_trajectory=True,
            resultfile=full_trajectory_data_filename,
            plot_rochelobe_equipotential_meshgrid=True,
            plot_rochelobe_equipotentials=True,
        )
        os.remove(full_trajectory_data_filename)


def readout_settings_file(settings_filename):
    """
    Function to readout the settings file
    """

    with open(settings_filename, "r") as f:
        settings = json.loads(f.read())

    return settings


def readout_failed_systems_file(failed_systems_filename):
    """
    Generator function to handle generating the grid points for the failed systems
    """

    with open(failed_systems_filename, "r") as f:
        for line in f:
            # readout system dict
            stripped = line.strip()
            system_dict = json.loads(stripped)

            yield system_dict


def re_run_failed_systems_main(target_function, simulation_dir, re_run_settings):
    """
    Main function to re-run failed systems. The target function should be a function that accepts 1 argument: settings
    """

    # Get the settings of the simulation
    settings_filename = os.path.join(simulation_dir, "settings.json")
    settings = readout_settings_file(settings_filename)

    # Set up failed system generator
    failed_systems_filename = os.path.join(simulation_dir, "failed_systems.txt")
    failed_system_generator = readout_failed_systems_file(failed_systems_filename)

    # Loop over the failed systems
    for failed_system_dict in failed_system_generator:
        # Combine with settings
        complete_system_config = {**settings, **failed_system_dict}

        # Update the system
        complete_system_config["system_dict"] = {
            **standard_system_dict,
            "synchronicity_factor": complete_system_config["synchronicity_factor"],
            "mass_accretor": complete_system_config["q_acc_don"]
            * standard_system_dict["mass_donor"],
        }

        # Update the configuration with re-run settings
        updated_complete_system_config = {**complete_system_config, **re_run_settings}

        # Call upon the target function
        target_function(updated_complete_system_config)


def dummy_target_function(settings):
    """
    Dummy function to just print the settings
    """

    print(settings)


if __name__ == "__main__":
    settings_filename = "/home/david/data_projects/ballistic_data/L1_stream/ballistic_stream_integration_results_stage_2_TEST/settings.json"
    settings = readout_settings_file(settings_filename)

    # Get failed systems
    failed_systems_filename = "/home/david/data_projects/ballistic_data/L1_stream/ballistic_stream_integration_results_stage_2_TEST/passed_systems.txt"
    failed_system_generator = readout_failed_systems_file(failed_systems_filename)

    # Combine with settings
    system_dict = next(failed_system_generator)
    complete_system_config = {**settings, **system_dict}

    #
    simulation_dir = "/home/david/data_projects/ballistic_data/L1_stream/ballistic_stream_integration_results_stage_2_TEST"
    re_run_settings = {}

    re_run_failed_systems(
        target_function=dummy_target_function,
        simulation_dir=simulation_dir,
        re_run_settings={"result_dir": "/tmp/vader/_rerun_tests"},
    )
