"""
Plot routines for the L1 stream code

TODO: add function to plot gradient of potential
"""

import json

import astropy.units as u
import matplotlib
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import numpy as np
from ballistic_integration_code.scripts.L1_stream.functions.intersection.thinning_functions import (
    thin_trajectory,
)
from ballistic_integrator.functions.frame_of_reference import (
    calculate_lagrange_points,
    calculate_position_accretor,
    calculate_position_donor,
    calculate_potential,
)
from ballistic_integrator.functions.functions import (
    calc_mass_ratio_accretor_donor,
    calc_mu_accretor,
    calc_orbital_period,
    calculate_center_of_mass,
)
from ballistic_integrator.functions.lagrange_points.lagrange_points_sepinski import (
    A_formula,
)
from david_phd_functions.plotting import custom_mpl_settings
from matplotlib import colors

custom_mpl_settings.load_mpl_rc()


offset_text = 0.15
fontsize = 32
markersize = 3


def plot_stars_and_COM_function(
    fig, ax, system_dict, frame_of_reference, plot_settings
):
    """
    Function to plot the location of the stars and the center of mass
    """

    ##############
    # Calculate positions of stars
    pos_accretor = calculate_position_accretor(
        system_dict=system_dict, frame_of_reference=frame_of_reference
    )
    pos_donor = calculate_position_donor(
        system_dict=system_dict, frame_of_reference=frame_of_reference
    )
    x_pos_COM = calculate_center_of_mass(
        m1=system_dict["mass_donor"],
        m2=system_dict["mass_accretor"],
        x1=pos_donor[0],
        x2=pos_accretor[0],
    )

    ##############
    # Plot positions of stars and center of mass

    # Accretor
    ax.text(
        pos_accretor[0],
        pos_accretor[1] - plot_settings.get("offset_text", offset_text),
        "M accretor",
        fontsize=fontsize,
        color=plot_settings.get("star_text_color", "black"),
    )
    ax.plot(
        pos_accretor[0],
        pos_accretor[1],
        "rx",
        markersize=plot_settings.get("markersize", markersize),
    )

    # donor
    ax.text(
        pos_donor[0],
        pos_donor[1] - plot_settings.get("offset_text", offset_text),
        "M donor",
        fontsize=fontsize,
        color=plot_settings.get("star_text_color", "black"),
        horizontalalignment="right",
    )
    ax.plot(
        pos_donor[0],
        pos_donor[1],
        "rx",
        markersize=plot_settings.get("markersize", markersize),
    )

    # Plot CoM
    COM = 0
    ax.plot(
        COM, x_pos_COM, "bx", markersize=plot_settings.get("markersize", markersize)
    )
    ax.text(
        COM,
        -plot_settings.get("offset_text", offset_text),
        "COM",
        fontsize=fontsize,
        color=plot_settings.get("COM_text_color", "black"),
        horizontalalignment="center",
    )

    return fig, ax


def plot_system_information_panel_function(fig, ax, data):
    """
    Function to plot information about the system on the side of the screen
    """

    settings = data["settings"]

    # Config
    fontsize_info = 26

    # Read out things from the data and calculate the quantities
    system_dict = settings["system_dict"]
    frame_of_reference = settings["frame_of_reference"]

    mass_accretor = system_dict["mass_accretor"] * u.Msun
    mass_donor = system_dict["mass_donor"] * u.Msun

    synchronicity_factor = system_dict["synchronicity_factor"]
    A_factor_sepinksy = A_formula(f=synchronicity_factor)

    mass_ratio_accretor_donor = calc_mass_ratio_accretor_donor(
        mass_accretor=mass_accretor, mass_donor=mass_donor
    )
    mu_accretor = calc_mu_accretor(
        mass_accretor=mass_accretor.value, mass_donor=mass_donor.value
    )

    separation = system_dict["separation"] * u.Rsun
    orbital_period = calc_orbital_period(
        mass_accretor=mass_accretor.value,
        mass_donor=mass_donor.value,
        separation=separation.value,
    )
    omega = 2 * np.pi / orbital_period

    M2 = mass_accretor.value
    M1 = mass_donor.value
    a = separation.value
    mu = M2 / (M1 + M2)
    mu1 = M1 / (M1 + M2)
    a = mu1 + mu

    # Get L1 location and the phi value at that point
    lagrange_points = calculate_lagrange_points(
        system_dict=system_dict, frame_of_reference=frame_of_reference
    )

    # Calculate potential values at those points
    phi_l1 = calculate_potential(
        settings=settings,
        system_dict=system_dict,
        frame_of_reference=frame_of_reference,
        x_pos=lagrange_points["L1"][0],
        y_pos=lagrange_points["L1"][1],
    )
    phi_l2 = calculate_potential(
        settings=settings,
        system_dict=system_dict,
        frame_of_reference=frame_of_reference,
        x_pos=lagrange_points["L2"][0],
        y_pos=lagrange_points["L2"][1],
    )
    phi_l3 = calculate_potential(
        settings=settings,
        system_dict=system_dict,
        frame_of_reference=frame_of_reference,
        x_pos=lagrange_points["L3"][0],
        y_pos=lagrange_points["L3"][1],
    )

    # Construct the strings with the information
    mass_line = "Maccretor: {} [{}]".format(
        mass_accretor.value, mass_accretor.unit.to_string("latex_inline")
    ) + "\nMdonor: {} [{}]".format(
        mass_donor.value, mass_donor.unit.to_string("latex_inline")
    )
    ratio_line = r"$\mu$: {:.2E}".format(mu_accretor) + "\nq: {:.2E}".format(
        mass_ratio_accretor_donor
    )  # Ratios
    synch_line = (
        r"$f_{\mathrm{synch}}$: "
        + "{:.2E}\n".format(synchronicity_factor)
        + r"$\mathcal{A}$: "
        + "{:.2E}".format(A_factor_sepinksy)
    )  # Synchronicity etc
    velocity_line = ""

    separation_line = "separation: {:.2E} [{}]".format(
        separation.value, separation.unit.to_string("latex_inline")
    )  # Distances
    period_omega_line = r"$\Omega$: {:.2E} [{}]".format(
        omega.value, omega.unit
    ) + "\nP: {:.2E} [{}]".format(
        orbital_period.value, orbital_period.unit.to_string("latex_inline")
    )
    lagrange_potential_values_line = (
        r"$\Phi_{{L1}}$: {:.2E}".format(phi_l1)
        + "\n"
        + r"$\Phi_{{L2}}$: {:.2E}".format(phi_l2)
        + "\n"
        + r"$\Phi_{{L3}}$: {:.2E}".format(phi_l3)
    )

    # Combine them
    textstr = "\n\n".join(
        (
            mass_line,
            ratio_line,
            synch_line,
            velocity_line,
            separation_line,
            period_omega_line,
            lagrange_potential_values_line,
        )
    )

    # these are matplotlib.patch.Patch properties
    props = dict(boxstyle="round", facecolor="wheat", alpha=0.5)

    # place a text box in upper left in axes coords
    ax.text(
        0.05,
        0.95,
        textstr,
        transform=ax.transAxes,
        fontsize=fontsize_info,
        verticalalignment="top",
        bbox=props,
        wrap=True,
    )

    # Remove all shit of the axes there.
    ax.set_axis_off()

    return fig, ax


def plot_L_points_function(
    fig,
    ax,
    system_dict,
    frame_of_reference,
    plot_settings,
):
    """
    Calculates the Lagrange points and plots these on the figure object
    """

    # Calculate the lagrange points
    lagrange_points = calculate_lagrange_points(
        system_dict=system_dict, frame_of_reference=frame_of_reference
    )

    # Loop over Lagrange points and plot them
    for L_point in lagrange_points:
        # Get location
        L_point_val = lagrange_points[L_point]

        # Plot
        ax.plot(
            [L_point_val[0]],
            [L_point_val[1]],
            marker="o",
            markersize=plot_settings.get("markersize", markersize),
            color="red",
        )
        ax.text(
            L_point_val[0],
            L_point_val[1] - plot_settings.get("offset_text", offset_text),
            L_point,
            fontsize=fontsize,
        )

    return fig, ax


def plot_rochelobe_equipotentials_function(
    fig, ax, settings, system_dict, frame_of_reference, plot_settings
):
    """
    Function to plot the contour for the roche lobe
    """

    bounds_x = plot_settings["plot_bounds_x"]
    bounds_y = plot_settings["plot_bounds_y"]
    resolution = plot_settings["plot_resolution"]

    # System description
    q = system_dict["mass_accretor"] / system_dict["mass_donor"]

    # Get the lagrange points and the corresponding potential values
    lagrange_points = calculate_lagrange_points(
        system_dict=system_dict, frame_of_reference=frame_of_reference
    )

    # Calculate potential values at those points
    phi_l1 = calculate_potential(
        settings=settings,
        system_dict=system_dict,
        frame_of_reference=frame_of_reference,
        x_pos=lagrange_points["L1"][0],
        y_pos=lagrange_points["L1"][1],
    )
    phi_l2 = calculate_potential(
        settings=settings,
        system_dict=system_dict,
        frame_of_reference=frame_of_reference,
        x_pos=lagrange_points["L2"][0],
        y_pos=lagrange_points["L2"][1],
    )
    phi_l3 = calculate_potential(
        settings=settings,
        system_dict=system_dict,
        frame_of_reference=frame_of_reference,
        x_pos=lagrange_points["L3"][0],
        y_pos=lagrange_points["L3"][1],
    )

    #
    lagrange_contour_dict = {
        "l1": {"phi": phi_l1, "color": "blue", "label": "L1"},
        "l2": {"phi": phi_l2, "color": "orange", "label": "L2"},
        "l3": {"phi": phi_l3, "color": "green", "label": "L3"},
    }

    # check if values are double
    if lagrange_contour_dict["l2"]["phi"] == lagrange_contour_dict["l3"]["phi"]:
        del lagrange_contour_dict["l3"]

    # Set up list and sort
    lagrange_contour_list = sorted(
        list(lagrange_contour_dict.values()),
        key=lambda x: x["phi"],
    )

    #########
    # Plot the entire potential

    # Create a meshgrid for x and y
    xx, yy = np.meshgrid(
        np.linspace(bounds_x[0], bounds_x[1], resolution),
        np.linspace(bounds_y[0], bounds_y[1], resolution),
    )

    # Calculate all the phi values
    phi_values = calculate_potential(
        settings=settings,
        system_dict=system_dict,
        frame_of_reference=frame_of_reference,
        x_pos=xx,
        y_pos=yy,
        z_pos=np.zeros(yy.shape),
    )

    # Draw the contours
    CS = ax.contour(
        xx,
        yy,
        phi_values,
        levels=[el["phi"] for el in lagrange_contour_list],
        colors=[el["color"] for el in lagrange_contour_list],
        linewidths=plot_settings.get("linewidth", 2),
    )

    clabel_dict = {
        "L1": r"$\Phi_{\mathrm{L1}}$",
        "L2": r"$\Phi_{\mathrm{L2}}$",
        "L3": r"$\Phi_{\mathrm{L3}}$",
    }

    # Add labels for the contours
    fmt = {}
    for l, s in zip(
        CS.levels, [clabel_dict[el["label"]] for el in lagrange_contour_list]
    ):
        fmt[l] = s

    # Label every other level using strings
    ax.clabel(
        CS,
        CS.levels,
        inline=True,
        fmt=fmt,
        fontsize=plot_settings.get("textsize", 16),
        colors="k",
        inline_spacing=15,
    )

    return fig, ax


def plot_rochelobe_equipotential_meshgrid_function(
    fig,
    ax,
    ax_cb,
    settings,
    system_dict,
    frame_of_reference,
    add_gradients,
    add_colorbar,
    plot_settings,
):
    """
    Function to plot the potential as a mesh
    """

    bounds_x = plot_settings["plot_bounds_x"]
    bounds_y = plot_settings["plot_bounds_y"]
    resolution = plot_settings["plot_resolution"]

    # get donor location
    position_donor = calculate_position_donor(
        system_dict=system_dict,
        frame_of_reference=frame_of_reference,
    )

    # Create a meshgrid for x and y
    x_arr = np.linspace(bounds_x[0], bounds_x[1], resolution)
    y_arr = np.linspace(bounds_y[0], bounds_y[1], resolution)
    xx, yy = np.meshgrid(
        x_arr,
        y_arr,
    )

    # Calculate all the phi values
    phi_values = calculate_potential(
        settings=settings,
        system_dict=system_dict,
        frame_of_reference=frame_of_reference,
        x_pos=xx,
        y_pos=yy,
    )

    # find values close to the stars and mask things that are too small
    phi_value_near_donor = calculate_potential(
        settings=settings,
        system_dict=system_dict,
        frame_of_reference=frame_of_reference,
        x_pos=position_donor[0] + 0.1,
        y_pos=position_donor[1] + 0.1,
    )

    # Mask all values lower than that
    phi_values[phi_values < phi_value_near_donor] = phi_value_near_donor

    # # Turn these values into negative log so the plotting is more insightful
    # phi_values = np.log10(-phi_values)

    norm = colors.Normalize(
        vmin=phi_values[~np.isnan(phi_values)].min(),
        vmax=phi_values[~np.isnan(phi_values)].max(),
    )

    # # Set up the colormesh plot
    # _ = ax.pcolormesh(
    #     x_arr,
    #     y_arr,
    #     phi_values,
    #     norm=norm,
    #     shading="auto",
    #     antialiased=True,
    #     rasterized=True,
    # )

    # contour
    contour = ax.contourf(
        x_arr,
        y_arr,
        phi_values,
        levels=40,
        norm=norm,
        antialiased=True,
    )

    if add_colorbar:
        # make colorbar
        cmap = plt.cm.get_cmap("viridis", 40)  # 11 discrete colors
        cbar = matplotlib.colorbar.ColorbarBase(
            ax_cb, cmap=cmap, norm=norm, extend="min"
        )
        cbar.ax.set_ylabel(
            r"Potential $\Phi$",
            fontsize=plot_settings.get("colorbar_fontsize", 32),
        )

        cbar.solids.set_antialiased(True)
        cbar.solids.set_rasterized(True)

    for c in contour.collections:
        c.set_rasterized(True)

    ################
    # add gradients
    if add_gradients:
        # Read https://stackoverflow.com/questions/54471306/gradients-and-quiver-plots

        res = 100

        dx = (np.abs(bounds_x[0]) + np.abs(bounds_x[1])) / res
        dy = (np.abs(bounds_y[0]) + np.abs(bounds_y[1])) / res

        # Create a meshgrid for x and y
        x_arr = np.linspace(bounds_x[0], bounds_x[1], res)
        y_arr = np.linspace(bounds_y[0], bounds_y[1], res + 1)
        xx, yy = np.meshgrid(
            x_arr,
            y_arr,
        )

        # Calculate all the phi values
        phi_values = calculate_potential(
            settings=settings,
            system_dict=system_dict,
            frame_of_reference=frame_of_reference,
            x_pos=xx,
            y_pos=yy,
        )

        # find values close to the stars and mask things that are too small
        phi_value_near_donor = calculate_potential(
            settings=settings,
            system_dict=system_dict,
            frame_of_reference=frame_of_reference,
            x_pos=position_donor[0] + 0.1,
            y_pos=position_donor[1] + 0.1,
        )

        # Mask all values lower than that
        phi_values[phi_values < phi_value_near_donor] = phi_value_near_donor

        #
        e_field_y, e_field_x = np.gradient(-phi_values, dx, dy)

        # Make quiver and streamplot
        ax.quiver(xx, yy, e_field_x, e_field_y, alpha=0.25)
        ax.streamplot(xx, yy, e_field_x, e_field_y, density=2, arrowsize=2)

    return fig, ax, ax_cb


def plot_trajectory_function(
    fig,
    ax,
    resultdata,
    settings,
    add_velocities_and_accelarations=False,
    thin_trajectories=False,
    plot_kwargs={},
):
    """
    Function that adds the trajectories plotting to an axis
    """

    if add_velocities_and_accelarations:
        # Plot velocities
        v_multiplier = 5
        ax.quiver(
            resultdata["positions"][:, 0],
            resultdata["positions"][:, 1],
            resultdata["velocities"][:, 0] * v_multiplier,
            resultdata["velocities"][:, 1] * v_multiplier,
            scale=5,
            scale_units="inches",
            color="orange",
            alpha=0.5,
        )

        # # Plot accelarations
        a_multiplier = 5
        ax.quiver(
            resultdata["positions"][:, 0],
            resultdata["positions"][:, 1],
            resultdata["accelarations"][:, 0] * a_multiplier,
            resultdata["accelarations"][:, 1] * a_multiplier,
            scale=5,
            scale_units="inches",
            color="red",
            alpha=0.5,
        )

    trajectory = resultdata["positions"]

    if thin_trajectories:
        trajectory = thin_trajectory(trajectory, settings)

    # Plot trajectory
    ax.plot(
        trajectory[:, 0],
        trajectory[:, 1],
        marker="o",
        antialiased=True,
        rasterized=True,
        **plot_kwargs,
    )

    #
    return fig, ax


def plot_system(
    system_dict,
    frame_of_reference,
    settings,
    plot_settings,
    resultfile=None,
    resultdata=None,
    resultdata_list=None,
    resultdata_sets=None,
    #
    plot_rochelobe_equipotentials=True,
    plot_rochelobe_equipotential_meshgrid=False,
    plot_stars_and_COM=True,
    plot_L_points=True,
    plot_trajectory=True,
    plot_system_information_panel=True,
    plot_system_add_colorbar=False,
    add_gradients=False,
    add_velocities_and_accelarations=False,
    thin_trajectories=False,
    return_fig=False,
    fig=None,
):
    """
    function to plot system with sepinsky based lagrange points

    resultdata_sets is a dict with trajectory sets and plot instructions (colors)
    """

    if plot_settings is None:
        plot_settings = {}

    ####################
    #
    # Set up canvas
    if fig is None:
        fig = plt.figure(figsize=(30, 18))
    fig.subplots_adjust(hspace=0.7)
    axes_list = []

    # Set up gridspec
    gs = fig.add_gridspec(nrows=1, ncols=8)

    #
    if plot_system_information_panel and plot_system_add_colorbar:
        raise ValueError("cant add the informationpanel AND a colorbar currently")

    if plot_system_information_panel:
        ax = fig.add_subplot(gs[:, :6])
        ax_info = fig.add_subplot(gs[:, -2:])
        axes_list.append(ax)
        axes_list.append(ax_info)
        ax_cb = None

    elif plot_system_add_colorbar:
        ax = fig.add_subplot(gs[:, :-1])
        ax_cb = fig.add_subplot(gs[:, -1:])
        axes_list.append(ax)
        axes_list.append(ax_cb)
    else:
        ax = fig.add_subplot(gs[:, :])
        axes_list.append(ax)
        ax_cb = None

    #####################
    # Add all the plot components
    if plot_rochelobe_equipotentials:
        fig, ax = plot_rochelobe_equipotentials_function(
            fig,
            ax,
            settings=settings,
            system_dict=system_dict,
            frame_of_reference=frame_of_reference,
            plot_settings=plot_settings,
        )

    if plot_rochelobe_equipotential_meshgrid:
        fig, ax, ax_cb = plot_rochelobe_equipotential_meshgrid_function(
            fig,
            ax,
            ax_cb,
            settings=settings,
            system_dict=system_dict,
            frame_of_reference=frame_of_reference,
            add_gradients=add_gradients,
            add_colorbar=plot_system_add_colorbar,
            plot_settings=plot_settings,
        )

    if plot_stars_and_COM:
        fig, ax = plot_stars_and_COM_function(
            fig,
            ax,
            system_dict=system_dict,
            frame_of_reference=frame_of_reference,
            plot_settings=plot_settings,
        )

    if plot_L_points:
        fig, ax = plot_L_points_function(
            fig,
            ax,
            system_dict=system_dict,
            frame_of_reference=frame_of_reference,
            plot_settings=plot_settings,
        )

    if plot_trajectory:
        #############
        # Plot a specific trajectory based on loaded data or a file containing the data
        if resultfile or resultdata:
            if resultfile:
                # Load data
                with open(resultfile, "r") as json_file:
                    data = json.load(json_file)
            elif resultdata:
                data = resultdata
            else:
                raise ValueError(
                    "PLease provide a filename or the result data to plot the trajectory"
                )

            #
            fig, ax = plot_trajectory_function(
                fig=fig,
                ax=ax,
                resultdata=data,
                settings=settings,
                add_velocities_and_accelarations=add_velocities_and_accelarations,
                thin_trajectories=thin_trajectories,
            )

        #############
        # Plot a list of trajectories
        if resultdata_list:
            for resultdata_i in resultdata_list:
                #
                fig, ax = plot_trajectory_function(
                    fig=fig,
                    ax=ax,
                    resultdata=resultdata_i,
                    settings=settings,
                    plot_kwargs={"alpha": resultdata_i["weight"]},
                    thin_trajectories=thin_trajectories,
                )

        #############
        # Plot a dict of lists of trajectories:
        if resultdata_sets:
            label_color_list = []
            for trajectory_set_dict in resultdata_sets:
                trajectory_list = trajectory_set_dict["trajectory_list"]
                plot_kwargs = trajectory_set_dict["plot_kwargs"]

                if trajectory_list:
                    label_color_list.append(plot_kwargs)

                # Loop over the trajectories
                for resultdata_i in trajectory_list:
                    fig, ax = plot_trajectory_function(
                        fig=fig,
                        ax=ax,
                        resultdata=resultdata_i,
                        plot_kwargs={"alpha": resultdata_i["weight"], **plot_kwargs},
                        settings=settings,
                        thin_trajectories=thin_trajectories,
                    )

            # Add a legend based on the colors and labels
            lines = []
            for label_color_dict in label_color_list:
                lines.append(
                    mlines.Line2D(
                        [],
                        [],
                        color=label_color_dict["color"],
                        marker="D",
                        ls="",
                        label=label_color_dict["label"],
                    )
                )
            ax.legend(
                handles=lines,
                frameon=True,
                fontsize=plot_settings.get(
                    "trajectory_classification_legend_fontsize", 36
                ),
            )

    #########
    if plot_system_information_panel:
        if resultfile:
            # Load data
            with open(resultfile, "r") as json_file:
                data = json.load(json_file)
        elif resultdata:
            data = resultdata

        fig, ax_info = plot_system_information_panel_function(fig, ax_info, data)

    #
    if not return_fig:
        plt.show()
    else:
        return fig, axes_list
