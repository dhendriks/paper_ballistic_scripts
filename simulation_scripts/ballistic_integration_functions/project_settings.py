"""
File containing the project level settings for this stage. Used by stage_settings in each stage
"""

from enum import Enum, auto

import astropy.units as u
from ballistic_integration_code.settings import settings as global_settings

dimensionless_unit = u.m / u.m


class EXIT_CODES(Enum):
    """
    Updated version of the exit codes for stage 1
    """

    PREEMPTIVE = -1
    NORMAL_FINISH = auto()
    INTEGRATOR_TERMINATION = auto()
    SELF_INTERSECTION = auto()
    FALLBACK_INTO_ROCHELOBE = auto()
    JACOBI_ERROR = auto()
    WRONG_DIRECTION = auto()
    TIMESTEP_TOO_SMALL = auto()
    EXCEEDED_MAX_STEPS = auto()
    ACCRETION_ONTO_ACCRETOR = auto()
    ACCRETION_ONTO_DONOR = auto()
    LOST_FROM_SYSTEM_UNDEFINED = auto()


# Define exit code messages
EXIT_CODE_MESSAGE_DICT = {
    EXIT_CODES.PREEMPTIVE.value: "Preemptive termination. RUNNING changed but not checked properly",
    EXIT_CODES.NORMAL_FINISH.value: "Integration finished succesfully. Terminated by max_time condition",
    EXIT_CODES.INTEGRATOR_TERMINATION.value: "Integrator failed with error. See integrator_return_code",
    EXIT_CODES.SELF_INTERSECTION.value: "Trajectory self-intersected",
    EXIT_CODES.FALLBACK_INTO_ROCHELOBE.value: "Trajectory fallback",
    EXIT_CODES.JACOBI_ERROR.value: "Jacobi constant not constant",
    EXIT_CODES.WRONG_DIRECTION.value: "Particle falling back to center",
    EXIT_CODES.TIMESTEP_TOO_SMALL.value: "Timestep too small",
    EXIT_CODES.EXCEEDED_MAX_STEPS.value: "Exceeded maximum number of allowed steps",
    EXIT_CODES.ACCRETION_ONTO_ACCRETOR.value: "Terminated due to accretion onto accretor",
    EXIT_CODES.ACCRETION_ONTO_DONOR.value: "Terminated due to accretion onto donor",
    EXIT_CODES.LOST_FROM_SYSTEM_UNDEFINED.value: "Terminated due trajectory might be lost from the system but is not well defined",
}


# Parameter dictionary: dictionary that contains all the information about the parameters that we use (input and output)
parameter_dict = {
    # Input parameters
    "log10normalized_thermal_velocity_dispersion": {
        "longname": "Log10(Normalized thermal velocity dispersion)",
        "shortname": "log10normalized_thermal_velocity_dispersion",
        "description": "Logarithm of the thermal velocity dispersion of the material in the envelope of the donor at L1, normalized by the separation (a) times orbital frequency",
        "reference": "See \Eqref{eq:sigma_thermal}",
    },
    "log10normalized_stream_area": {
        "longname": "Log10(Normalized mass stream cross section area)",
        "shortname": "log10normalized_stream_area",
        "description": "",
        "reference": "See \Eqref{eq:normalized_surface_area}",
    },
    "A_factor": {
        "longname": "A factor from Sepinsky 2007",
        "shortname": "A_factor",
        "description": "A factor from Sepinsky 2007 that takes into account the synchronicity, the eccentricity and the mean anomaly of the orbit.",
        "reference": "",
    },
    "massratio_accretor_donor": {
        "longname": "Mass ratio accretor / donor",
        "shortname": "mass_ratio",
        "description": "Mass ratio of the accretor (Mass ratio = Mass accretor / Mass donor)",
        "reference": "",
    },
    # Output parameters
    "rmin": {
        "longname": "Radius of closest approach",
        "shortname": "rmin",
        "description": "Radius of closest approach to the accretor of the mass transfer stream. In units of the separation of the system.",
        "reference": "",
    },
    "rcirc": {
        "longname": "Circularisation radius",
        "shortname": "rcirc",
        "description": "Radius of circularisation based on the angular momentum content of the stream at the radius of closest approach. In units of the separation of the system.",
        "reference": "",
    },
    "fraction_onto_donor": {
        "longname": "Fraction MT onto donor",
        "shortname": "fraction_onto_donor",
        "description": "Fraction of transferred mass that falls back onto the donor.",
        "reference": "See \Eqref{eq:fraction_self_accretion}",
    },
    "fraction_onto_accretor": {
        "longname": "Fraction MT onto accretor",
        "shortname": "fraction_onto_accretor",
        "unit": dimensionless_unit,
        "description": "Fraction of transferred mass that falls towards the accretor.",
        "reference": "Like \Eqref{eq:fraction_self_accretion}",
    },
    "specific_angular_momentum_multiplier_self_accretion": {
        "longname": "Specific angular momentum multiplier self-accretion",
        "shortname": "specific_angular_momentum_multiplier_self_accretion",
        "unit": dimensionless_unit,
        "description": "angular momentum change factor ($h_{\mathrm{fin,\ don}}/h_{\mathrm{init,\ don}}$)",
        "reference": "\Secref{} and  \Eqref{eq:angmom_wrt_don}",
    },
    "weight_valid": {
        "longname": "Weight valid trajectories",
        "shortname": "weight_valid",
        "unit": dimensionless_unit,
        "description": "Total weight of the classified trajectories (i.e those not terminated or rejected).",
        "reference": "\Secref{} and  \Eqref{eq:angmom_wrt_don}",
    },
    "stream_orientation": {
        "longname": "Stream orientation",
        "shortname": "stream_orientation",
        "unit": dimensionless_unit,
        "description": "Orientation of the mass transfer stream when accreting onto the accretor.",
        "reference": "",
    },
    "fraction_self_intersection": {
        "longname": "Fraction of systems that self-intersect",
        "shortname": "fraction_self_intersection",
        "unit": dimensionless_unit,
        "description": "Weighted fraction of all valid trajectories that self-intersect.",
        "reference": "",
    },
    "fraction_other_intersection": {
        "longname": "Fraction of trajectories that intersect with other trajectories.",
        "shortname": "fraction_other_intersection",
        "unit": dimensionless_unit,
        "description": "Weighted fraction of all valid trajectories that intersect with trajectories other than themselves.",
        "reference": "",
    },
    "specific_torque_orbit_onto_accretor": {
        "longname": "Specific torque on the orbit due to accretion onto the accretor.",
        "shortname": "specific_torque_orbit_onto_accretor",
        "unit": dimensionless_unit,
        "description": "Specific torque on the orbit due to accretion onto the accretor.",
        "reference": "",
    },
    "specific_torque_orbit_onto_donor": {
        "longname": "Specific torque on the orbit due to accretion onto the donor.",
        "shortname": "specific_torque_orbit_onto_donor",
        "unit": dimensionless_unit,
        "description": "Specific torque on the orbit due to accretion onto the donor. ",
        "reference": "",
    },
    "h_init_don": {
        "longname": "Initial specific angular momentum of the stream w.r.t. the donor",
        "shortname": "h_init_don",
        "unit": dimensionless_unit,
        "description": "Initial specific angular momentum of the stream w.r.t the donor",
        "reference": "",
    },
    "magnitude_eccentricity": {
        "longname": "Eccentricity magnitude at radius of closest approach for accretion onto the accretor.",
        "shortname": "magnitude_eccentricity",
        "unit": dimensionless_unit,
        "description": "The eccentricity magnitude of the stream at the radius of closest approach for accretion onto the accretor.",
        "reference": "",
    },
    "L1_x": {
        "longname": "First lagrange point wrt Donor",
        "shortname": "L1_x",
        "unit": dimensionless_unit,
        "description": "First lagrange point w.r.t. donor star (takes into account the asynchronous rotation).",
        "reference": "",
    },
    "ratio_new_eggleton": {
        "longname": "Ratio asynchronous Roche-lobe radius to Eggleton Roche-lobe radius",
        "shortname": "ratio_new_eggleton",
        "unit": dimensionless_unit,
        "description": "Ratio of asynchronous Roche-lobe radius to Eggleton Roche-lobe radius. We calculate this by covering the surface of the L1 equipontial of the donor with a series of points, calculating the volume with ss.ConvexHull(volume_points_array) and calculating the radius of a sphere with equal volume.",
        "reference": "",
    },
}

# Control settings
project_settings = {
    **global_settings,
    ############
    # Global settings
    "frame_of_reference": "center_of_mass",
    "csv_output_separator": "\t",
    # Grid settings
    "backup_if_data_exists": True,
    "run": True,
    "write_full_output_to_file": False,
    "write_summary_output_to_file": True,
    "num_cores": 4,
    "max_job_queue_size": 100,
    "separator": "\t",
    "write_failed_passed_systems": True,
    "verbosity": 0,
    "length_decimals_dataheader": 6,
    "print_frequency": 10,
    "generate_plot_at_gridpoint": True,
    "plot_bounds_x": [-1.5, 1.5],
    "plot_bounds_y": [-1.5, 1.5],
    "plot_resolution": 1000,
    "include_asynchronicity_donor_during_integration": False,  # generally for the integration we would not need to include this (see discussion rob)
    "include_asynchronicity_donor_for_lagrange_points": True,
    "generate_trajectory_analysis_plot_at_gridpoint": False,
    "fraction_offset": 100,
    "non_synch_velocity_offset_distance_to": "donor",
    "direction_velocity_asynchronous_offset": +1,  # -1 was old style. +1 is new style
    "actually_run_system": True,
    "integrator_type": "dopri5",
    "integrator_configuration": {},
    #
    ######################
    # Stream trajectory interpolation settings
    "specific_angular_momentum_stream_interpolation_enabled": True,
    "specific_angular_momentum_stream_interpolation_lower_bound": 0.1,  # In terms of roche lobe radius of accretor
    "specific_angular_momentum_stream_interpolation_upper_bound": 1.0,  # in terms of roche lobe radius of accretor
    "specific_angular_momentum_stream_interpolation_num_radii": 10,  # number of radii to calculate properties at
    ######################
    # Intersection detection settings
    "handle_intersecting_trajectories": True,
    "trajectory_thinning_length": 100,
    "self_intersection_angle_threshold": 0,
    "intersection_angle_threshold": 45,
    "trajectory_thinning_length": 100,
    "thin_trajectory": True,
    "thin_by_index": False,
    "thin_by_distance": False,
    "thin_by_angle": True,
    "trajectory_thinning_angle_threshold": 2.5,
    ######################
    # trajectory integration settings
    "max_time": 15,
    "max_steps": 30000,
    "custom_termination_maximum_path_length": 2,
    "dt": 0.0001,
    "steps_check_self_intersection": -1,
    "jacobi_error_tol": 1e-6,
    "initial_velocity_offset": [0.0, 0.0],
    "log10normalized_thermal_velocity_dispersion": -6,
    "print_info": False,
    "allow_fallback_to_center": True,
    "timestep_increase_factor": 1.05,
    #
    ######################
    # custom termination control
    "use_custom_termination_routine": True,
    #
    ######################
    # custom timestep control
    "use_custom_timestep_rejection_routine": True,
    # Limit on timestep
    "limit_timestep_on_stepsize": True,
    "max_position_diff_timestep_rejection": 0.0001,
    # Limit on jacobi constant
    "limit_timestep_on_jacobi_constant_change": True,
    # Timestep limit and increase
    "global_minimum_timestep": 1e-20,
    #######################
    # Rochelobe size calculation
    "store_lagrange_point_info_in_gridpoint": False,
    "store_roche_lobe_volumes": False,
    # TODO: depracate the above 2
    "lagrange_point_data_file_basename": "lagrange_point_data.txt",
    "lagrange_point_csv_interpolation_table_file_basename": "lagrange_point_and_rochelobe_volume_interpolation_table.csv",
    "lagrange_point_header_interpolation_table_file_basename": "RLOF_sepinsky2007_lagrange_interpolation_table.h",
    "rochelobe_passed_gridpoint_file_basename": "RL_passed_gridpoints.txt",
    "rochelobe_failed_gridpoint_file_basename": "RL_failed_gridpoints.txt",
    "volume_roche_lobe_xgrid_n": 25,
    "volume_roche_lobe_ygrid_n": 25,
    "volume_roche_lobe_size_factor": 1.5,
    #
    "trajectory_data_file_basename": "trajectory_summary_data.txt",
    "trajectory_header_interpolation_table_file_basename": "RLOF_Hendriks2022_ballistic_stream_table.h",
    "trajectory_csv_interpolation_table_file_basename": "RLOF_Hendriks2022_ballistic_stream_table_datafile.csv",
}
