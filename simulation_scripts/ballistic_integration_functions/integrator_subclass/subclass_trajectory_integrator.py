"""
Subclass of the trajectory integrator for stage 1 of the ballistic integration project

The changes here will be of the following functions:
- override the EXIT_CODES with an updated version of the exit codes, so that we can more easily create an updated set of the exit codes
- override the custom_stopping_criterion to more flexibly set the stopping situation
- override the custom_timestep_control to more flexibly set the custom timestepping
"""

from ballistic_integration_code.scripts.L1_stream.integrator_subclass.custom_termination_routine import (
    custom_termination_routine,
)
from ballistic_integration_code.scripts.L1_stream.integrator_subclass.custom_timestep_rejection_routine import (
    custom_timestep_rejection_routine,
)
from ballistic_integration_code.scripts.L1_stream.project_settings import (
    EXIT_CODE_MESSAGE_DICT,
    EXIT_CODES,
)
from ballistic_integrator.functions.integrator.trajectory_integrator_class import (
    trajectory_integrator,
)


class trajectory_integrator_class(trajectory_integrator):
    """
    Subclass of trajectory integrator for stage 1
    """

    # Set custom termination routine
    custom_termination_routine = custom_termination_routine

    # Set custom timestep control routine
    custom_timestep_rejection_routine = custom_timestep_rejection_routine

    def __init__(
        self,
        system_dict,
        frame_of_reference,
        initial_position,
        initial_velocity,
        control_settings,
        verbosity=1,
    ):
        # https://rhettinger.wordpress.com/2011/05/26/super-considered-super/
        super().__init__(
            system_dict,
            frame_of_reference,
            initial_position,
            initial_velocity,
            control_settings,
            verbosity,
        )

        # Update the exit codes
        self.EXIT_CODES = EXIT_CODES

        # Update the exit code messages
        self.EXIT_CODE_MESSAGE_DICT = EXIT_CODE_MESSAGE_DICT


if __name__ == "__main__":
    import inspect

    from ballistic_integration_code.settings import standard_system_dict

    stage_1_trajectory_integrator_class_instance = trajectory_integrator_class(
        system_dict=standard_system_dict,
        frame_of_reference="center_of_mass",
        initial_position=[0, 0],
        initial_velocity=[0, 0],
        control_settings={},
        verbosity=1,
    )

    print(
        inspect.getsource(trajectory_integrator_class.custom_timestep_control_routine)
    )
