"""
Script containing the custom termination routine for the ballistic stream integration

TODO: we need to refine this to handle the surface of the donor better. that is, use the (equi)potentials for the donor to determine accretion onto donor
TODO: we need to use eccentricity of orbit around the accretor to determine the bound-ness of the orbit around it. in that way we can exclude some of the trajectories

NOTE:
- I changed this routine a bit so that the equipotential, and L1 point, that is used in these calculations, is the one that is relevant for the donor.
    Specifically: the self-accretion should happen on the critical surface of the donor that takes into account the rotation

"""

import numpy as np
from ballistic_integrator.functions.frame_of_reference import (
    calculate_position_accretor,
    calculate_position_donor,
    calculate_potential,
)


def custom_termination_routine(self):
    """
    Custom termination function to deal with termination when test particle moves away from accretor

    This termination routine will try to categorise the trajectory behaviour:
    - Accretion onto accretor:
        - particle moving towards accretor
        - particle moving away from donor
        - particle moving away from l1
        - current potential energy lower than l1

    - Lost from system:
        - When the particle's distance from the COM is larger than 2 * separation

    At each step, it will look at the behaviour of the particle, and signal some value to a list of previous signals.

    In some cases we base the conclusion of the trajectory type on 1 step, others require a couple of steps in a row signalling the same value
    """

    # ignore if we are already not running anymore
    if not self.RUNNING:
        return

    # Set up
    if self.step == 0:
        self.trajectory_classification_dict["trajectory_classified"] = False
        self.trajectory_classification_dict["trajectory_type"] = None
        self.trajectory_classification_dict["classification_signals"] = []
        self.trajectory_classification_dict["accretor_classification_counter"] = 0

    # Get positions
    pos_accretor = calculate_position_accretor(
        system_dict=self.system_dict, frame_of_reference=self.frame_of_reference
    )
    pos_donor = calculate_position_donor(
        system_dict=self.system_dict, frame_of_reference=self.frame_of_reference
    )
    pos_L1 = self.control_settings["L_points_donor"]["L1"][:2]
    pos_particle = self.position

    # get distances
    distance_to_accretor = np.linalg.norm(pos_particle - pos_accretor)
    distance_to_donor = np.linalg.norm(pos_particle - pos_donor)
    distance_to_COM = np.linalg.norm(pos_particle)
    distance_to_L1 = np.linalg.norm(pos_particle - pos_L1)
    distance_accretor_to_L1 = np.linalg.norm(pos_accretor - pos_L1)
    distance_donor_to_L1 = np.linalg.norm(pos_donor - pos_L1)

    # Get the potential
    L1_potential_val = calculate_potential(
        settings=self.control_settings,
        system_dict=self.system_dict,
        frame_of_reference=self.frame_of_reference,
        x_pos=pos_L1[0],
        y_pos=pos_L1[1],
    )
    particle_potential_val = calculate_potential(
        settings=self.control_settings,
        system_dict=self.system_dict,
        frame_of_reference=self.frame_of_reference,
        x_pos=pos_particle[0],
        y_pos=pos_particle[1],
    )

    ##############
    # Determing trajectory type
    if (self.step > 0) and (
        not self.trajectory_classification_dict["trajectory_classified"]
    ):
        ##############
        # The logic here is if else based, so these classifications should be as mutually
        # exclusive as possible to prevent accidental prioritisation

        ########
        # Identifying the stream onto accretor:
        if all(
            [
                ##########
                # Particle needs to be moving towards accretor
                distance_to_accretor
                < self.trajectory_classification_dict["previous_distance_to_accretor"],
                # Particle needs to be moving away from donor
                distance_to_donor
                > self.trajectory_classification_dict["previous_distance_to_donor"],
                # Particle needs to be moving away from L1 point
                distance_to_L1
                > self.trajectory_classification_dict["previous_distance_to_L1"],
                # Deeper in the potential than L1 point
                particle_potential_val < L1_potential_val,
                # Particle needs to be within the circle centered on accretor with radius (|x_acc - x_L1|)
                distance_to_accretor < distance_accretor_to_L1,
                # Particle needs to be within the circle centered on accretor with radius 0.5 * (|x_acc - x_L1|)
                distance_to_accretor < 0.8 * distance_accretor_to_L1,
                ##########
            ]
        ):
            if self.verbosity > 0:
                print("identified trajectory as accretion onto accretor")

            #############
            # TODO: build in a method to count the conscecutive steps
            self.trajectory_classification_dict[
                "trajectory_type"
            ] = "accretion_onto_accretor"
            self.trajectory_classification_dict["trajectory_classified"] = True

        ########
        # Identifying the stream onto donor:
        if all(
            [
                ##########
                # moving towards donor
                distance_to_donor
                < self.trajectory_classification_dict["previous_distance_to_donor"],
                # moving away from accretor
                distance_to_accretor
                > self.trajectory_classification_dict["previous_distance_to_accretor"],
                # Particle needs to be moving away from L1 point
                distance_to_L1
                > self.trajectory_classification_dict["previous_distance_to_L1"],
                # Particle should be deeper in the potential well than L1 point
                particle_potential_val < L1_potential_val,
                # Particle needs to be within the circle centered on donor with radius (|x_don - x_L1|)
                distance_to_donor < distance_donor_to_L1,
                ##########
            ]
        ):
            if self.verbosity > 0:
                print("identified trajectory as accretion onto donor")

            self.trajectory_classification_dict[
                "trajectory_type"
            ] = "accretion_onto_donor"
            self.trajectory_classification_dict["trajectory_classified"] = True

        ########
        # Identifying the trajectory as undefined
        if all(
            [
                ##########
                # moving towards donor
                distance_to_donor
                > self.trajectory_classification_dict["previous_distance_to_donor"],
                # moving away from accretor
                distance_to_accretor
                > self.trajectory_classification_dict["previous_distance_to_accretor"],
                # Particle needs to be moving away from L1 point
                distance_to_L1
                > self.trajectory_classification_dict["previous_distance_to_L1"],
                # Particle needs to be quite a bit from the L1 point
                distance_to_L1 > 1,
                # # Particle should be deeper in the potential well than L1 point
                # particle_potential_val < L1_potential_val,
                # # Particle needs to be within the circle centered on donor with radius (|x_don - x_L1|)
                # distance_to_donor < distance_donor_to_L1,
                ##########
            ]
        ):
            if self.verbosity > 0:
                print("identified trajectory as lost from system/undefined")

            self.trajectory_classification_dict[
                "trajectory_type"
            ] = "lost_from_system_or_undefined"
            self.trajectory_classification_dict["trajectory_classified"] = True

        ########
        # Identifying the trajectory as undefined because trajectory too long
        if (
            self.distance_travelled
            > self.control_settings["custom_termination_maximum_path_length"]
        ):
            if self.verbosity > 0:
                print("identified trajectory as lost from system/undefined")

            self.trajectory_classification_dict[
                "trajectory_type"
            ] = "lost_from_system_or_undefined"
            self.trajectory_classification_dict["trajectory_classified"] = True

    ##############
    # Handle termination requirements for each trajectory type
    if self.trajectory_classification_dict["trajectory_classified"]:
        ##########################
        # Handle trajectory type: accretion_onto_accretor
        if (
            self.trajectory_classification_dict["trajectory_type"]
            == "accretion_onto_accretor"
        ):
            ############
            # TODO: Add method to handle disk accretion and direct  impact accretion separately

            #######
            # When a trajectory is recognized to be one that falls towards the accretor,
            # we should stop that calculation at the point where the trajectory starts moving away
            # from the accretor (point of closest approach)
            if (
                distance_to_accretor
                > self.trajectory_classification_dict["previous_distance_to_accretor"]
            ):
                # Signal stop running and set exit code
                self.RUNNING = False
                self.EXIT_CODE = self.EXIT_CODES.ACCRETION_ONTO_ACCRETOR

        ##########################
        # Handle trajectory type: accretion_onto_donor
        if (
            self.trajectory_classification_dict["trajectory_type"]
            == "accretion_onto_donor"
        ):
            # When identified as accretion onto donor we should stop immediatly.
            # This will be okay cause the donor is overflowing its roche lobe so it
            # should directly hit the donor again.
            # Signal stop running and set exit code
            self.RUNNING = False
            self.EXIT_CODE = self.EXIT_CODES.ACCRETION_ONTO_DONOR

        ##########################
        # Handle trajectory type: lost_from_system
        if (
            self.trajectory_classification_dict["trajectory_type"]
            == "lost_from_system_or_undefined"
        ):
            # When identified as undefined / lost from system we should stop immediately (for now)
            # In this case we wont be capturing this trajectory well and we need a better method for it.

            # Signal stop running and set exit code
            self.RUNNING = False
            self.EXIT_CODE = self.EXIT_CODES.LOST_FROM_SYSTEM_UNDEFINED

    ######################
    # Store previous distances and other info to use it in the next step
    self.trajectory_classification_dict[
        "previous_distance_to_accretor"
    ] = distance_to_accretor
    self.trajectory_classification_dict[
        "previous_distance_to_donor"
    ] = distance_to_donor
    self.trajectory_classification_dict["previous_distance_to_COM"] = distance_to_COM
    self.trajectory_classification_dict["previous_distance_to_L1"] = distance_to_L1
