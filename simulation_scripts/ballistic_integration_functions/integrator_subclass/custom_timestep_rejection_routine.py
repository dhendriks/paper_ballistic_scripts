"""
Script that contains the custom timestep control functions
"""

import numpy as np
from ballistic_integrator.functions.frame_of_reference.wrapper import (
    calculate_jacobi_constant,
)


def custom_timestep_rejection_routine(self):
    """
    Function to control the timestep based on a control parameter passed through the control.settings
    """

    #
    reject = False

    # Only when we can
    if self.step > 1:
        ########
        # Limit timestep based on stepsize
        if self.control_settings["limit_timestep_on_stepsize"]:
            current_pos = self.position
            previous_pos = self.positions[-1]
            dpos = np.linalg.norm(current_pos - previous_pos)

            if dpos > self.control_settings["max_position_diff_timestep_rejection"]:
                if self.verbosity > 2:
                    print(
                        "Distance in current step too large. dpos: {}. max_allowed_dpos: {}.  Rejecting timestep".format(
                            dpos,
                            self.control_settings[
                                "max_position_diff_timestep_rejection"
                            ],
                        )
                    )
                reject = True

        # Limit timestep on jacobi error
        if self.control_settings["limit_timestep_on_jacobi_constant_change"]:
            if self.jacobi_deviation > self.jacobi_error_tol:
                if self.verbosity > 2:
                    print(
                        "Jacobi constant deviation in current step (dt={}) too large. jacobi error: {}. max_allowed_jacobi_error: {}. Rejecting timestep".format(
                            self.dt,
                            self.jacobi_deviation,
                            self.jacobi_error_tol,
                        )
                    )
                reject = True

        ########
        # Limit timestep based on distance from accretor
        #   here the timestep is limited based on the distance from the accretor expressed in the distance of accretor to L1 point (indicating the progress of the trajectory)
        # if self.control_settings["limit_timestep_on_distance_from_accretor"]:
        #     pos_accretor = calculate_position_accretor(
        #         system_dict=system_dict, frame_of_reference=frame_of_reference
        #     )
        #     current_pos = self.positions[-1]
        #     l1_pos = self.control_settings["L_points"]["L1"]

        #     # Get distances
        #     dist_l1_to_accretor = np.abs(pos_accretor - l1_pos[:2])
        #     dist_particle_to_accretor = np.linalg.norm(pos_accretor - dist_l1_to_accretor)

        ########
        # TODO: introduce other limiting factors

    return reject
