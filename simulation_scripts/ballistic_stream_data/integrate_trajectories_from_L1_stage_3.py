"""
Function to run the trajectories. Handles setting up the trajectory class, and the sampling around the area
"""

import os
import time

import numpy as np
import pandas as pd
from ballistic_integration_code.scripts.L1_stream.functions.calculate_and_store_lagrange_points import (
    calculate_and_store_lagrange_points,
)
from ballistic_integration_code.scripts.L1_stream.functions.calculate_and_store_roche_lobe_information import (
    calculate_and_store_roche_lobe_information,
)
from ballistic_integration_code.scripts.L1_stream.functions.calculate_trajectory_result_summary import (
    calculate_trajectory_result_summary,
)
from ballistic_integration_code.scripts.L1_stream.functions.filter_trajectories_based_on_intersection import (
    filter_trajectories_based_on_intersection,
)
from ballistic_integration_code.scripts.L1_stream.functions.functions import (
    write_failed_passed_systems,
)
from ballistic_integration_code.scripts.L1_stream.functions.generate_full_trajectory_data_filename import (
    generate_full_trajectory_data_filename,
)
from ballistic_integration_code.scripts.L1_stream.functions.generate_result_dict import (
    generate_result_dict,
)
from ballistic_integration_code.scripts.L1_stream.functions.handle_plotting_at_gridpoint import (
    handle_plotting_at_gridpoint,
)
from ballistic_integration_code.scripts.L1_stream.functions.handle_trajectory_analysis_plot_at_gridpoint import (
    handle_trajectory_analysis_plot_at_gridpoint,
)
from ballistic_integration_code.scripts.L1_stream.functions.intersection.intersection_detection import (
    handle_intersecting_trajectories,
)
from ballistic_integration_code.scripts.L1_stream.functions.mass_stream_area_functions import (
    calculate_normalised_mass_stream_area,
    generate_L1_area_offset_range_and_weights,
)
from ballistic_integration_code.scripts.L1_stream.functions.plot_functions import (
    plot_system,
)
from ballistic_integration_code.scripts.L1_stream.functions.rerun_failed_systems_functions import (
    handle_plotting_at_re_run,
)
from ballistic_integration_code.scripts.L1_stream.functions.set_initial_location import (
    set_initial_location,
)
from ballistic_integration_code.scripts.L1_stream.functions.set_initial_velocity import (
    set_initial_velocity,
)
from ballistic_integration_code.scripts.L1_stream.functions.utils import (
    get_weighted_average,
)
from ballistic_integration_code.scripts.L1_stream.functions.write_to_file import (
    write_to_file,
)
from ballistic_integration_code.scripts.L1_stream.integrator_subclass.subclass_trajectory_integrator import (
    trajectory_integrator_class,
)


def integrate_trajectories_from_L1_stage_3(
    settings,
):
    """
    Main function to run the trajectory calculator
    """

    global_start = time.time()
    timing_dict = {}

    #
    trajectory_set = []
    num_samples_area_sampling = settings["num_samples_area_sampling"]

    # Set filenames
    result_dir = settings["result_dir"]
    trajectory_summary_data_filename = os.path.join(
        result_dir, "trajectory_summary_data.txt"
    )

    failed_systems_filename = os.path.join(result_dir, "failed_systems.txt")
    passed_systems_filename = os.path.join(result_dir, "passed_systems.txt")

    # Create directory
    os.makedirs(os.path.abspath(result_dir), exist_ok=True)

    # Create directory of trajectories
    trajectory_result_dir = os.path.join(result_dir, "trajectory_data")
    os.makedirs(os.path.abspath(trajectory_result_dir), exist_ok=True)

    #########
    # Handle sampling of the normalized area around L1
    start_trajectory_calculation = time.time()

    #
    normalised_mass_stream_area = calculate_normalised_mass_stream_area(
        settings=settings
    )
    log10normalized_stream_area = np.log10(normalised_mass_stream_area)

    # Set up the weights
    (
        area_sampling_offset_array,
        area_sampling_weight_array,
    ) = generate_L1_area_offset_range_and_weights(
        log10normalized_stream_area=log10normalized_stream_area,
        n_samples=num_samples_area_sampling,
    )
    for area_sampling_ID, (area_sampling_offset, area_sampling_weight) in enumerate(
        zip(area_sampling_offset_array, area_sampling_weight_array)
    ):
        if settings["verbosity"] > 1:
            print(
                "area_sampling_ID: {} area_sampling_offset: {} area_sampling_weight: {}".format(
                    area_sampling_ID, area_sampling_offset, area_sampling_weight
                )
            )

        ################
        # Set initial velocity
        settings = set_initial_velocity(settings)

        ################
        # Set initial location
        settings = set_initial_location(settings, L1_area_offset=area_sampling_offset)

        # Full trajectory filename
        full_trajectory_data_filename = generate_full_trajectory_data_filename(
            settings=settings,
            system_dict=settings["system_dict"],
            trajectory_result_dir=trajectory_result_dir,
        )

        #####
        # Handle evolution
        if settings.get("run", True):
            # Set up the integrator
            integrator = trajectory_integrator_class(
                system_dict=settings["system_dict"],
                frame_of_reference=settings["frame_of_reference"],
                initial_position=settings["initial_position"],
                initial_velocity=settings["initial_velocity"],
                control_settings=settings,
                verbosity=settings["verbosity"],
            )

            # Calculate the trajectory
            result = integrator.evolve()

            # Calculate the result summary (adding some values)
            result_summary = calculate_trajectory_result_summary(result)
            result_summary["weight"] = area_sampling_weight

            # Set up result dict that contains the results of the trajectories sampled around the L1 point for this specific grid-point
            result_dict = generate_result_dict(
                settings=settings,
                weight=area_sampling_weight,
                result_summary=result_summary,
                grid_point=settings["grid_point"],
            )

            # Store all the trajectory data s.t. we can go filter them
            result_dict["trajectory_index"] = area_sampling_ID
            trajectory_set.append(
                {
                    "result_dict": result_dict,
                    "result_summary": result_summary,
                    "trajectory_index": area_sampling_ID,
                }
            )

            # Handle writing of failed or passed systems
            write_failed_passed_systems(
                grid_point={
                    **settings["grid_point"],
                    "area_sampling_offset": area_sampling_offset,
                },
                result_summary=result_summary,
                settings=settings,
                failed_systems_filename=failed_systems_filename,
                passed_systems_filename=passed_systems_filename,
            )

            # Handle plotting of the system during re-run
            handle_plotting_at_re_run(
                settings=settings,
                system_dict=settings["system_dict"],
                result=result,
                full_trajectory_data_filename=full_trajectory_data_filename,
            )
    end_trajectory_calculation = time.time()
    timing_dict["trajectory_calculation"] = (
        end_trajectory_calculation - start_trajectory_calculation
    )

    #############
    # Handle intersections detection
    if settings.get("handle_intersecting_trajectories", False):
        start_intersection_calculation = time.time()
        trajectory_set = handle_intersecting_trajectories(
            trajectory_set=trajectory_set, settings=settings
        )
        end_intersection_calculation = time.time()
        timing_dict["Intersection_calculation"] = (
            end_intersection_calculation - start_intersection_calculation
        )

    #############
    # Filter based on intersections
    if settings.get("filter_on_interstecting_trajectories", False):
        trajectory_set = filter_trajectories_based_on_intersection(
            trajectory_set, settings
        )

    #############
    # Summary section
    #   In this section we make a summary of the data, based on classifications
    start_summarising = time.time()

    # Construct entire gridpoint dataframe
    gridpoint_df = pd.DataFrame([el["result_dict"] for el in trajectory_set])

    #############
    # Get the systems that have no correct classification (i.e. terminated due to different reasons)
    terminated_df = gridpoint_df.query("terminated == 1")

    #
    count_terminated = len(terminated_df.index)
    weight_terminated = get_weighted_average(gridpoint_df, "terminated")

    #############
    # Get the rejected systems
    rejected_df = gridpoint_df.query("rejected_based_on_intersection == 1")

    # Get coint and weight
    count_rejected = len(rejected_df.index)
    weight_rejected = get_weighted_average(
        gridpoint_df, "rejected_based_on_intersection"
    )
    # (optional): filter out self-accreting trajectories
    # (optional): filter out other-accreting trajectories

    ##############
    # Filter out the rejected and terminated systems
    valid_indices = list(
        set(gridpoint_df.index) - set(rejected_df.index) - set(terminated_df.index)
    )

    valid_df = gridpoint_df.filter(items=valid_indices, axis=0)
    valid_df = gridpoint_df.reset_index(drop=True)

    #
    count_valid = len(valid_df.index)
    weight_valid = 1 - weight_rejected - weight_terminated
    indices_valid = valid_df["trajectory_index"]

    ###############

    #######################
    # Handle the combining of all the results of the area sampling.

    # Select sub-dfs
    accretion_onto_accretor_df = valid_df.query("accretion_onto_accretor == 1")
    accretion_onto_donor_df = valid_df.query("accretion_onto_donor == 1")
    undefined_df = valid_df.query("undefined_accretion == 1")

    #################
    # Calculate quantities of interest for each

    ############
    ## overall
    # Get fractions of each path
    fraction_onto_accretor = get_weighted_average(
        valid_df, col="accretion_onto_accretor"
    )
    fraction_onto_donor = get_weighted_average(valid_df, col="accretion_onto_donor")
    fraction_undefined = get_weighted_average(valid_df, col="undefined_accretion")

    #
    average_h_init_don = get_weighted_average(valid_df, col="h_init_don")

    ############
    ## accretion onto accretor
    # Get averaged values for accretion onto accretor
    average_rmin = get_weighted_average(accretion_onto_accretor_df, col="rmin")
    average_rcirc = get_weighted_average(accretion_onto_accretor_df, col="rcirc")
    average_stream_orientation = get_weighted_average(
        accretion_onto_accretor_df, col="stream_orientation"
    )
    average_h_min_acc = get_weighted_average(
        accretion_onto_accretor_df, col="h_min_acc"
    )
    averaged_specific_torque_orbit_onto_accretor = get_weighted_average(
        accretion_onto_accretor_df, col="specific_torque_orbit_onto_accretor"
    )
    averaged_magnitude_eccentricity = get_weighted_average(
        accretion_onto_accretor_df, col="magnitude_eccentricity_onto_accretor"
    )
    averaged_true_anomaly = get_weighted_average(
        accretion_onto_accretor_df, col="true_anomaly_onto_accretor"
    )

    ############
    ## accretion onto donor
    # Get averaged values for accretion onto donor
    average_specific_angular_momentum_multiplier_self_accretion = get_weighted_average(
        accretion_onto_donor_df,
        col="specific_angular_momentum_multiplier_self_accretion",
    )
    averaged_specific_torque_orbit_onto_donor = get_weighted_average(
        accretion_onto_accretor_df, col="specific_torque_orbit_onto_donor"
    )

    ############
    # Store in a dictionary
    averaged_result_dict = {
        ############
        # Grid point
        **settings["grid_point"],
        ############
        # Information on the terminated or rejected trajectories
        "count_rejected": count_rejected,
        "weight_rejected": weight_rejected,
        "count_terminated": count_terminated,
        "weight_terminated": weight_terminated,
        "count_valid": count_valid,
        "weight_valid": weight_valid,
        ############
        # Fractions of classes for accepted trajectories
        "fraction_onto_accretor": fraction_onto_accretor,
        "fraction_onto_donor": fraction_onto_donor,
        "fraction_undefined": fraction_undefined,
        ############
        # Actual results
        "h_init_don": average_h_init_don,
        # Accretion onto accretor
        "rmin": average_rmin,
        "rcirc": average_rcirc,
        "stream_orientation": average_stream_orientation,
        "h_min_acc": average_h_min_acc,
        "specific_torque_orbit_onto_accretor": averaged_specific_torque_orbit_onto_accretor,
        "magnitude_eccentricity": averaged_magnitude_eccentricity,
        ############
        # Accretion onto donor
        "specific_angular_momentum_multiplier_self_accretion": average_specific_angular_momentum_multiplier_self_accretion,
        "specific_torque_orbit_onto_donor": averaged_specific_torque_orbit_onto_donor,
    }
    end_summarising = time.time()
    timing_dict["summarising"] = end_summarising - start_summarising

    ############
    # handle stream interpolation points
    if settings["specific_angular_momentum_stream_interpolation_enabled"]:
        average_specific_angular_momentum_stream_interpolation_result_dict = {}
        specific_angular_momentum_stream_interpolation_keys = [
            col
            for col in accretion_onto_accretor_df.columns
            if col.startswith("specific_angmom_interpolation_radius_")
        ]

        # loop and calculate values
        for (
            specific_angular_momentum_stream_interpolation_key
        ) in specific_angular_momentum_stream_interpolation_keys:
            average_specific_angular_momentum_stream_interpolation_result_dict[
                specific_angular_momentum_stream_interpolation_key
            ] = get_weighted_average(
                accretion_onto_accretor_df,
                col=specific_angular_momentum_stream_interpolation_key,
                ignore_value=0,
            )

        # print(average_specific_angular_momentum_stream_interpolation_result_dict)
        # for (
        #     key,
        #     value,
        # ) in average_specific_angular_momentum_stream_interpolation_result_dict.items():
        #     print(key, value)

        # store in final result dict
        averaged_result_dict = {
            **averaged_result_dict,
            **average_specific_angular_momentum_stream_interpolation_result_dict,
        }

    ############
    # Handle storing lagrange points in the dict
    if settings["store_lagrange_point_info_in_gridpoint"]:
        averaged_result_dict = calculate_and_store_lagrange_points(
            settings=settings, result_dict=averaged_result_dict
        )

    ############
    # Handle storing the roche lobe volume (and fraction)
    if settings["store_roche_lobe_volumes"]:
        averaged_result_dict = calculate_and_store_roche_lobe_information(
            settings=settings, result_dict=averaged_result_dict
        )

    ##############
    # Get the fraction of trajectories that intersect with self or others
    if settings.get("handle_intersecting_trajectories", False):
        fraction_self_intersection = 0
        fraction_other_intersection = 0
        for trajectory_dict in trajectory_set:
            if not trajectory_dict["trajectory_index"] in indices_valid:
                continue

            # Get the self intersection
            if trajectory_dict["intersection_data"]["self_intersection_data"][
                "self_intersection_found"
            ]:
                fraction_self_intersection += trajectory_dict["result_summary"][
                    "weight"
                ]

            # get the other-intersection
            if trajectory_dict["intersection_data"]["other_intersection_data"][
                "other_intersection_found"
            ]:
                fraction_other_intersection += trajectory_dict["result_summary"][
                    "weight"
                ]
        fraction_self_intersection = fraction_self_intersection / weight_valid
        fraction_other_intersection = fraction_other_intersection / weight_valid

        # store in dict
        averaged_result_dict["fraction_self_intersection"] = fraction_self_intersection
        averaged_result_dict[
            "fraction_other_intersection"
        ] = fraction_other_intersection

    ###################
    # Handle plotting of the trajectory
    if settings["generate_plot_at_gridpoint"]:
        handle_plotting_at_gridpoint(
            settings=settings,
            grid_point=settings["grid_point"],
            trajectory_set=trajectory_set,
        )

    ###################
    # Return the trajectory set instead of writing everything
    if settings.get("return_trajectory_set", False):
        return trajectory_set, averaged_result_dict

    ################
    # Write the output to file
    if settings["verbosity"] > 1:
        print("writing to file {}".format(trajectory_summary_data_filename))
    write_to_file(
        settings=settings,
        trajectory_summary_data_filename=trajectory_summary_data_filename,
        result_dict=averaged_result_dict,
    )

    ################
    # Print timing information
    global_end = time.time()
    global_total = global_end - global_start

    if settings["verbosity"] > 0:
        print(
            "In total the current gridpoint calculaton took {:.2e}s".format(
                global_total
            )
        )
        for name, value in timing_dict.items():
            print(
                "{} took {:.2e}s ({:2f}% of global_total)".format(
                    name, value, 100 * (value / global_total)
                )
            )
