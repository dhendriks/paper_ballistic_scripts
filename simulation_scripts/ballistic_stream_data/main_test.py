"""
Main script to run stage 2 of the ballistic integrations:
- grid over A/a, , v_thermal/a omega, f and q
- sample trajectories /around/ L1 with appropriate weights
"""

import os

import numpy as np
from ballistic_integration_code.scripts.L1_stream.functions.run_all import run_all
from ballistic_integration_code.scripts.L1_stream.stage_3.stage_settings import (
    stage_3_settings,
)

######################
# Run grid           #
######################

settings = stage_3_settings

############
# Production run

# Area sampling disabled
settings = {
    **settings,
    "num_cores": 48,
    "num_samples_area_sampling": 25,
    "store_roche_lobe_volumes": False,
    "generate_plot_at_gridpoint": True,
    "direction_velocity_asynchronous_offset": 1,
    "result_dir": os.path.join(
        os.environ["PROJECT_DATA_ROOT"],
        "ballistic_data",
        "L1_stream",
        "server_results",
        "ballistic_stream_integration_results_stage_3_OPPOSITE_ASYNCHRONOUS_OFFSET_WITH_INTERPOLATION",
    ),
}
run_all(
    settings=settings,
    stage="stage_3",
    run_trajectories=False,
    generate_report=True,
    generate_output_interpolation_files=True,
    generate_description_table=True,
    generate_rochelobe_interpolation_data=True,
)
