"""
Script containing function to convert the interpolation table file to the structure for binary_c

The read-out file contains the summarized results of the ballistic trajectories. instead of first saving the trajectory data we now just process that immediately
"""

import copy
import os

import numpy as np
import pandas as pd
from ballistic_integration_code.scripts.L1_stream.functions.data_header_functions import (
    data_header_write_data_array,
    data_header_write_input_defines,
    data_header_write_meta_data,
    data_header_write_output_defines,
    get_index_dict,
)
from ballistic_integration_code.scripts.L1_stream.functions.functions import (
    filter_df_on_exit_codes,
    get_git_info_and_time,
)
from ballistic_integration_code.scripts.L1_stream.project_settings import (
    dimensionless_unit,
)
from ballistic_integration_code.scripts.L1_stream.stage_3.generate_data_header_stage_3 import (
    handle_interpolation_radii_parameter,
)
from ballistic_integration_code.scripts.L1_stream.stage_3.stage_settings import (
    stage_3_settings,
)
from ballistic_integrator.functions.lagrange_points.lagrange_points_sepinski import (
    A_formula,
)


def generate_csv_datafile_stage_3(
    input_interpolation_textfile,
    output_interpolation_textfile,
    settings,
    parameter_dict,
):
    """
    Function to write the interpolation data as a header array
    """

    #####################
    # Config
    definition_basename = "RLOF_HENDRIKS2022_BALLISTIC_STREAM_INTERPOLATION"
    stage_number = 3
    comment_symbol = "#"
    parameter_dict = copy.deepcopy(parameter_dict)

    #
    length_decimals = settings["length_decimals_dataheader"]
    input_parameter_list = settings["input_parameter_list_data_header"]
    output_parameter_list = copy.copy(settings["output_parameter_list_data_header"])

    # update with interpolation radii information
    output_parameter_list, parameter_dict = handle_interpolation_radii_parameter(
        output_parameter_list=output_parameter_list,
        settings=settings,
        parameter_dict=parameter_dict,
    )

    # Read out interpolation textfile
    df = pd.read_csv(input_interpolation_textfile, sep="\s+", header=0)

    # Add columns to df
    df["A_factor"] = A_formula(f=df["synchronicity_factor"])

    # ############
    # # Prepare for write-out

    # Make sure the order is correct
    filtered_df = df[input_parameter_list + output_parameter_list]

    # make sure to sort the columns
    filtered_df = filtered_df.sort_values(by=input_parameter_list)

    # Get the number of lines we will output to the table and some others
    num_lines = len(filtered_df.index)
    num_input_parameters = len(input_parameter_list)
    num_output_parameters = len(output_parameter_list)

    # Get git info
    git_info = get_git_info_and_time()

    #####################
    # Write header info to file
    print("Writing to {}".format(output_interpolation_textfile))
    with open(output_interpolation_textfile, "w") as f:
        ########################
        # Write top-level explanation
        f.write(
            """{comment_symbol} Ballistic stream trajectory integration stage {stage_number} dataset.
{comment_symbol} This dataset aims to provide an interpolation dataset to determine rmin and racc for the stream based on the mass ratio q, the initial thermal velocity v, and the A factor
{comment_symbol} that captures synchronicity, eccentricty and mean anomaly (see sepinksy 2007), to improve RLOF calculations and estimates for interactions
{comment_symbol} and specifically for accretion disks.\n
""".format(
                comment_symbol=comment_symbol, stage_number=stage_number
            )
        )

        # Write git revision information
        f.write(
            "{comment_symbol} Generated on: {datetime} with git repository: {repo_name} branch: {branch_name} commit: {commit_sha} repository url: {repo_url}\n\n".format(
                comment_symbol=comment_symbol,
                datetime=git_info["datetime_string"],
                repo_name=git_info["repo_name"],
                branch_name=git_info["branch_name"],
                commit_sha=git_info["commit_sha"],
                repo_url="https://gitlab.com/phd-dhendriks/ballistic_integration_code",
            )
        )

        ######
        # Write data header metadata (parameter descriptions, stage number, number of data lines etc)
        data_header_write_meta_data(
            settings=settings,
            input_parameter_list=input_parameter_list,
            output_parameter_list=output_parameter_list,
            parameter_dict=parameter_dict,
            stage_number=stage_number,
            num_lines=num_lines,
            num_input_parameters=num_input_parameters,
            num_output_parameters=num_output_parameters,
            filehandle=f,
            definition_basename=definition_basename,
            comment_symbol=comment_symbol,
            write_defines=False,
        )

        #########
        # Write the data to csv
        filtered_df.to_csv(
            f,
            header=True,
            index=False,
            sep=settings["csv_output_separator"],
            float_format=lambda x: round(x, length_decimals),
        )


if __name__ == "__main__":
    from ballistic_integration_code.scripts.L1_stream.project_settings import (
        parameter_dict,
    )
    from ballistic_integration_code.scripts.L1_stream.stage_3.stage_settings import (
        stage_3_settings,
    )

    settings = stage_3_settings

    #
    simname = "ballistic_stream_integration_results_stage_3_OPPOSITE_ASYNCHRONOUS_OFFSET_WITH_INTERPOLATION"

    result_dir = os.path.join(
        os.getenv("PROJECT_DATA_ROOT"),
        "ballistic_data",
        "L1_stream",
        "server_results",
        simname,
    )

    # Get input file
    input_filename = os.path.join(
        result_dir,
        "trajectory_summary_data.txt",
    )

    #
    output_filename = os.path.join(result_dir, "csv_datafile.txt")
    os.makedirs(os.path.dirname(output_filename), exist_ok=True)

    generate_csv_datafile_stage_3(
        input_interpolation_textfile=input_filename,
        output_interpolation_textfile=output_filename,
        settings=settings,
        parameter_dict=parameter_dict,
    )

    # test readout
    df = pd.read_csv(output_filename, skiprows=39)

    print(df)
