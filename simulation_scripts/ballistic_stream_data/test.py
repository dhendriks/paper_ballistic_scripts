"""
Main script to run stage 2 of the ballistic integrations:
- grid over A/a, , v_thermal/a omega, f and q
- sample trajectories /around/ L1 with appropriate weights
"""

import os

import numpy as np
from ballistic_integration_code.scripts.L1_stream.functions.run_all import run_all
from ballistic_integration_code.scripts.L1_stream.stage_3.stage_settings import (
    stage_3_settings,
)

######################
# Run grid           #
######################

settings = stage_3_settings

############
# Test run
settings = {
    **settings,
    "result_dir": os.path.join(
        os.environ["PROJECT_DATA_ROOT"],
        "ballistic_data",
        "L1_stream",
        "ballistic_stream_integration_results_stage_3_individual_trajectories_testing",
    ),
    # "jacobi_error_tol": 1e-5,
    # "max_position_diff_timestep_rejection": 1e-3,
    # "dt": 1e-4,
    #
    # "q_range": [1 * 10**0, 0.5 * 10**0, 2 * 10**0],
    # "f_range": [1],
    # #
    # "log10normalized_thermal_velocity_dispersion_range": [-3],
    #
    # "q_range": [0.4832930238571752],
    # "f_range": [0.2],
    "q_range": [10],
    "f_range": [1.1],
    "log10normalized_thermal_velocity_dispersion_range": [-3.5],
    "num_cores": 1,
    "num_samples_area_sampling": 1,
    "store_roche_lobe_volumes": False,
    "generate_plot_at_gridpoint": False,
    "generate_trajectory_analysis_plot_at_gridpoint": False,
    "verbosity": 0,
    "backup_if_data_exists": True,
    "thin_trajectory": True,
    "thin_by_angle": True,
    "trajectory_thinning_angle_threshold": 2.5,
}

run_all(
    settings=settings,
    stage="stage_3",
    run_trajectories=True,
    generate_report=False,
    generate_output_interpolation_files=True,
    generate_rochelobe_interpolation_data=True,
    generate_description_table=False,
)
