"""
Main script to run stage 2 of the ballistic integrations:
- grid over A/a, , v_thermal/a omega, f and q
- sample trajectories /around/ L1 with appropriate weights
"""

import os

import numpy as np
from ballistic_integration_code.scripts.L1_stream.functions.run_all import run_all
from ballistic_integration_code.scripts.L1_stream.stage_3.stage_settings import (
    stage_3_settings,
)

######################
# Run grid           #
######################

settings = stage_3_settings

# ############
# # Test run
# settings = {
#     **settings,
#     "result_dir": os.path.join(
#         os.environ["PROJECT_DATA_ROOT"],
#         "ballistic_data",
#         "L1_stream",
#         "ballistic_stream_integration_results_stage_3_TEST",
#     ),
#     "jacobi_error_tol": 1e-2,

#     # "q_range": 10 ** np.linspace(-2, 2, 2),
#     # "f_range": np.linspace(0.1, 1, 2),
#     "log10normalized_thermal_velocity_dispersion_range": np.linspace(-3.5, -1, 2),
#     # "log10normalized_stream_area_range": 10 ** np.linspace(-3, -1, 2),
#     "q_range": np.concatenate([10**np.linspace(-2,0, 20), 10**np.linspace(0, 2, 10)[1:]]),
#     "f_range": np.linspace(0.1, 2, 20),

#     #
#     "q_range": [10**-1.5],
#     "f_range": [0.15],
#     # "q_range": [1],
#     # "f_range": [1],
#     #
#     "log10normalized_thermal_velocity_dispersion_range": [-1.5],
#     # "log10normalized_stream_area_range": [-5],
#     "num_cores": 4,
#     "num_samples_area_sampling": 5,
#     # "store_roche_lobe_volumes": False,
#     # "generate_plot_at_gridpoint": True,
#     # "actually_run_system": True,
#     # "verbosity": ,
#     # "integrator_type": "dopri5"
# }

# run_all(
#     settings=settings,
#     stage='stage_3',
#     run_trajectories=True,
#     generate_report=True,
#     create_data_header=True,
#     generate_description_table=True
# )
# re_run_settings = {"result_dir": settings['result_dir'] + "RE_RUN", "plot_system_at_re_run": True}
# # run_all(settings=settings, stage='stage_1', run_trajectories=False, generate_report=False, create_data_header=False, generate_description_table=False
# #     re_run_failed_systems=True, re_run_settings=re_run_settings
# # )


############
# Production run

settings = {
    **settings,
    "num_cores": 48,
    "store_roche_lobe_volumes": False,
    "generate_plot_at_gridpoint": True,
}

# with area sampling opposite asynchronous offset
settings = {
    **settings,
    "num_samples_area_sampling": 5,
    "direction_velocity_asynchronous_offset": 1,
    "result_dir": os.path.join(
        os.environ["PROJECT_DATA_ROOT"],
        "ballistic_data",
        "L1_stream",
        "ballistic_stream_integration_results_stage_3_INTER______________OPPOSITE_ASYNCHRONOUS_OFFSET_WITH_INTERPOLATION",
    ),
    "log10normalized_thermal_velocity_dispersion_range": [-1.5],
    "f_range": [1.0],
}
run_all(
    settings=settings,
    stage="stage_3",
    run_trajectories=True,
    generate_report=True,
    create_data_header=True,
    generate_description_table=False,
)

# with area sampling opposite asynchronous offset
settings = {
    **settings,
    "num_samples_area_sampling": 5,
    "direction_velocity_asynchronous_offset": 1,
    "result_dir": os.path.join(
        os.environ["PROJECT_DATA_ROOT"],
        "ballistic_data",
        "L1_stream",
        "ballistic_stream_integration_results_stage_3_INTER_LOW_F_____________OPPOSITE_ASYNCHRONOUS_OFFSET_WITH_INTERPOLATION",
    ),
    "log10normalized_thermal_velocity_dispersion_range": [-1.5],
    "f_range": [0.5],
}
run_all(
    settings=settings,
    stage="stage_3",
    run_trajectories=True,
    generate_report=True,
    create_data_header=True,
    generate_description_table=False,
)

# with area sampling opposite asynchronous offset
settings = {
    **settings,
    "num_samples_area_sampling": 5,
    "direction_velocity_asynchronous_offset": 1,
    "result_dir": os.path.join(
        os.environ["PROJECT_DATA_ROOT"],
        "ballistic_data",
        "L1_stream",
        "ballistic_stream_integration_results_stage_3_INTER_HIGH_F_____________OPPOSITE_ASYNCHRONOUS_OFFSET_WITH_INTERPOLATION",
    ),
    "log10normalized_thermal_velocity_dispersion_range": [-1.5],
    "f_range": [1.5],
}
run_all(
    settings=settings,
    stage="stage_3",
    run_trajectories=True,
    generate_report=True,
    create_data_header=True,
    generate_description_table=False,
)
