"""
Stage specific settings configuration
"""

import os

import numpy as np
from ballistic_integration_code.scripts.L1_stream.project_settings import (
    EXIT_CODES,
    project_settings,
)

#######################
# Control settings
stage_3_settings = {
    **project_settings,
    ############
    # Grid settings
    "result_dir": os.path.join(
        os.environ["PROJECT_DATA_ROOT"],
        "ballistic_data",
        "L1_stream",
        "ballistic_stream_integration_results_stage_3",
    ),
    "allowed_exit_codes": [
        EXIT_CODES.ACCRETION_ONTO_ACCRETOR.value,
        EXIT_CODES.ACCRETION_ONTO_DONOR.value,
        EXIT_CODES.LOST_FROM_SYSTEM_UNDEFINED.value,
    ],
    # store lagrange points
    "store_lagrange_point_info_in_gridpoint": False,
    # calculate and store roche lobe volume info
    "store_roche_lobe_volumes": False,
    # Parameter grid
    "log10normalized_thermal_velocity_dispersion_range": np.linspace(-3.5, -0.5, 13),
    "q_range": np.concatenate(
        [10 ** np.linspace(-2, 0, 20), 10 ** np.linspace(0, 2, 10)[1:]]
    ),
    "f_range": np.linspace(0.1, 2, 20),
    "num_samples_area_sampling": 10,
    ############
    # Data header output control
    "input_parameter_list_data_header": [
        "log10normalized_thermal_velocity_dispersion",
        "A_factor",
        "massratio_accretor_donor",
    ],
    "output_parameter_list_data_header": [
        # Global
        "weight_valid",
        # self-intersection
        "fraction_self_intersection",
        "fraction_other_intersection",
        # accretion fractions
        "fraction_onto_accretor",
        "fraction_onto_donor",
        # initial property stream
        "h_init_don",
        # donor accretion properties
        "specific_angular_momentum_multiplier_self_accretion",
        # accretor accretion properties
        "rmin",
        "rcirc",
        "stream_orientation",
    ],
}
