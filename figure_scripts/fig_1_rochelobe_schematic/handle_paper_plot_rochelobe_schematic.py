"""
Function to plot the roche lobe schematic
"""

import os

import matplotlib

from paper_ballistic_scripts.figure_scripts.utils import (
    delete_out_of_frame_text,
    load_mpl_rc,
    plot_system,
    show_and_save_plot,
)
from paper_ballistic_scripts.settings import standard_system_dict

load_mpl_rc()
matplotlib.rcParams["savefig.dpi"] = 300

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def plot_rochelobe_schematic(system_dict, settings, plot_settings):
    """
    Function to plot the roche lobe schematic
    """

    #
    fig, axes_list = plot_system(
        system_dict,
        settings=settings,
        frame_of_reference="center_of_mass",
        resultfile=None,
        resultdata=None,
        resultdata_list=None,
        #
        plot_rochelobe_equipotentials=True,
        plot_rochelobe_equipotential_meshgrid=True,
        plot_stars_and_COM=True,
        plot_L_points=True,
        plot_trajectory=False,
        plot_system_information_panel=False,
        plot_system_add_colorbar=True,
        # add_gradients=False,
        # add_velocities_and_accelarations=False
        return_fig=True,
        plot_settings=plot_settings,
    )

    # Add labels
    axes_list[0].set_xlabel(
        r"$\it{x}$", fontsize=plot_settings.get("axis_label_fontsize")
    )
    axes_list[0].set_ylabel(
        r"$\it{y}$", fontsize=plot_settings.get("axis_label_fontsize")
    )

    # Remove text that falls out of frame
    fig, axes_list[0] = delete_out_of_frame_text(fig, axes_list[0])

    axes_list[0].set_xlim(plot_settings["plot_bounds_x"])
    axes_list[0].set_ylim(plot_settings["plot_bounds_y"])

    # force the same axes ratio
    axes_list[0].set_aspect("equal")

    # fig.tight_layout()
    fig.subplots_adjust(wspace=-0.4)

    #
    show_and_save_plot(fig=fig, plot_settings=plot_settings)


def handle_paper_plot_rochelobe_schematic(output_name, show_plot):
    """
    Function to handle the paper version of the roche-lobe schematic plot
    """

    #########
    # Potential for particle in flight
    system_dict = {
        **standard_system_dict,
        "mass_donor": 1,
        "mass_accretor": 0.75,
        "synchronicity_factor": 1,
    }

    # basename
    basename = "rochelobe_schematic.pdf"
    plot_rochelobe_schematic(
        system_dict=system_dict,
        settings={
            "include_asynchronous_donor_effects": False,
            "include_asynchronicity_donor_during_integration": False,
        },
        plot_settings={
            "star_text_color": "white",
            "show_plot": show_plot,
            "output_name": output_name,
            "plot_bounds_x": [-1.4, 1.3],
            "plot_bounds_y": [-1.5, 1.5],
            "plot_resolution": 1000,
            "textsize": 36,
            "offset_text": -0.1,
            "markersize": 16,
            "axis_label_fontsize": 40,
            "colorbar_fontsize": 40,
        },
    )


if __name__ == "__main__":
    output_dir = os.path.join(this_file_dir, "plots/")

    handle_paper_plot_rochelobe_schematic(
        output_name=os.path.join(output_dir, "fig_1_rochelobe_schematic.pdf"),
        show_plot=True,
    )
