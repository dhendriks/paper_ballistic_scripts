"""
Main script to plot all the paper figures.

this function does not call the plot_XXX functions directly, but rather calls handle_paper_plot_XXX_<> functions that contain the correct configuration for each plot.
This is to keep things close to the source and not to clog this function

# TODO: use contextmanager to handle the increment? https://codesolid.com/python-with-using-and-writing-context-managers-in-python/
"""

import os
from functools import partial

from paper_ballistic_scripts.figure_scripts.fig_1_rochelobe_schematic.handle_paper_plot_rochelobe_schematic import (
    handle_paper_plot_rochelobe_schematic,
)

# from ballistic_integration_code.paper_scripts.classification_fraction_plot.plot_classification_fractions import (
#     handle_paper_plot_classification_fractions_high_v,
#     handle_paper_plot_classification_fractions_low_v,
# )
# from ballistic_integration_code.paper_scripts.initial_conditions_schematic.plot_initial_conditions_schematic import (
#     handle_paper_plot_initial_conditions_schematic,
# )
# from ballistic_integration_code.paper_scripts.intersection_overview.plot_intersection_overview import (
#     handle_paper_plot_intersection_overview_high_v,
#     handle_paper_plot_intersection_overview_low_v,
# )
# from ballistic_integration_code.paper_scripts.lagrange_point_overview_frames_of_reference.plot_lagrange_point_overview_frames_of_reference import (
#     handle_paper_plot_lagrange_point_overview_frames_of_reference,
# )
# from ballistic_integration_code.paper_scripts.plot_example_stream_interpolation_schematic.plot_example_stream_interpolation_schematic import (
#     handle_paper_plot_example_stream_interpolation_schematic,
# )
# from ballistic_integration_code.paper_scripts.plot_exploration_parameters.plot_exploration_all_parameters import (
#     handle_paper_plot_exploration_parameter_distributions,
# )
# from ballistic_integration_code.paper_scripts.plot_exploration_parameters.plot_exploration_alpha_parameter import (
#     handle_paper_plot_exploration_distribution_alpha_parameter,
# )
# from ballistic_integration_code.paper_scripts.plot_exploration_parameters.plot_exploration_alpha_vs_synchronicity import (
#     handle_paper_plot_alpha_f_distributions,
# )
# from ballistic_integration_code.paper_scripts.plot_schematic_velocity_offset_nonsychronous_rotation.plot_schematic_velocity_offset_nonsychronous_rotation import (
#     handle_paper_plot_schematic_velocity_offset_non_synchronous_rotation,
# )
# from ballistic_integration_code.paper_scripts.radii_plots.plot_ratio_rcirc_rmin import (
#     handle_paper_plot_ratio_rcirc_rmin_high_v,
#     handle_paper_plot_ratio_rcirc_rmin_low_v,
# )
# from ballistic_integration_code.paper_scripts.radii_plots.plot_rmin_for_non_synchronous_rotators import (
#     handle_paper_plot_radius_of_closest_approach_high_v,
#     handle_paper_plot_radius_of_closest_approach_low_v,
# )
# from ballistic_integration_code.paper_scripts.rochelobe_schematic.plot_rochelobe_schematic import (
#     handle_paper_plot_rochelobe_schematic,
# )
# from ballistic_integration_code.paper_scripts.specific_angmom_stream_interpolation.plot_specific_angmom_steam_interpolation import (
#     handle_paper_plot_specific_angmom_stream_interpolation_low_v_synchronous,
# )
# from ballistic_integration_code.paper_scripts.specific_angular_momentum_increase_self_accretion.plot_specific_angular_momentum_factor_self_accretion import (
#     handle_paper_plot_specific_angular_momentum_factor_self_accretion_high_v,
#     handle_paper_plot_specific_angular_momentum_factor_self_accretion_low_v,
# )
# from ballistic_integration_code.paper_scripts.stream_area_function_plot.stream_area_function_plot import (
#     handle_paper_plot_two_panel_geometry_and_stream_area,
# )
# from ballistic_integration_code.paper_scripts.trajectory_classification_example.plot_trajectory_classification_example_with_intersections import (
#     handle_paper_plot_trajectory_classification_example,
# )

#
this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def general_handle_figure_name_function(
    output_dir,
    basename,
    figure_counter,
    figure_format,
    omit_basename,
    prepend_figure_counter,
):
    """
    General function to handle the naming of the figure
    """

    ###################
    # Handle checks
    if omit_basename and (not prepend_figure_counter):
        raise ValueError("Can't omit basename and not prepend figure counter!")
    allowed_formats = ["pdf", "eps", "tiff", "png"]
    if figure_format not in allowed_formats:
        raise ValueError(
            "figure_format has to be one of the following: {}".format(allowed_formats)
        )

    ###################
    # Construct new basename
    figure_counter_string = "Fig_{}{}".format(
        figure_counter, "_" if not omit_basename else ""
    )

    new_basename = ""

    if prepend_figure_counter:
        new_basename += figure_counter_string
    if not omit_basename:
        new_basename += basename
    new_basename += ".{}".format(figure_format)
    print("Handling {}".format(new_basename))

    # Construct full path
    figure_output_name = os.path.join(output_dir, new_basename)

    return figure_output_name


def plot_all_paper_figures(
    exploration_data_filename,
    ballistic_data_filename,
    plot_output_dir,
    figure_flags=None,
):
    """
    Function to plot all the relevant plots.

    TODO: handle auto-increment to generate the plots with a set filename
    """

    if figure_flags is None:
        figure_flags = {}

    #
    # figure_format = "pdf"
    omit_basename = False
    prepend_figure_counter = True
    show_plot = False
    figure_counter = 1

    # Construct new function
    handle_figure_name = partial(
        general_handle_figure_name_function,
        output_dir=plot_output_dir,
        figure_format=figure_flags["figure_format"],
        omit_basename=omit_basename,
        prepend_figure_counter=prepend_figure_counter,
    )

    ###################
    # Roche-lobe schematic.
    if figure_flags.get("fig_1_rochelobe_schematic", True):
        output_name = handle_figure_name(
            basename="rochelobe_schematic", figure_counter=figure_counter
        )
        handle_paper_plot_rochelobe_schematic(
            output_name=output_name, show_plot=show_plot
        )
    figure_counter += 1

    # ###################
    # # combined area plot
    # if figure_flags.get("fig_2_combined_area_plot", True):
    #     output_name = handle_figure_name(
    #         basename="combined_area_plot", figure_counter=figure_counter
    #     )
    #     handle_paper_plot_two_panel_geometry_and_stream_area(
    #         output_name=output_name, show_plot=show_plot
    #     )
    # figure_counter += 1

    # ###################
    # # schematic_offset_v_nonsync
    # if figure_flags.get("fig_3_schematic_offset_v_nonsync", True):
    #     output_name = handle_figure_name(
    #         basename="schematic_offset_v_nonsync", figure_counter=figure_counter
    #     )
    #     handle_paper_plot_schematic_velocity_offset_non_synchronous_rotation(
    #         output_name=output_name, show_plot=show_plot
    #     )
    # figure_counter += 1

    # ###################
    # # initial_conditions_schematic
    # if figure_flags.get("fig_4_initial_conditions_schematic", True):
    #     output_name = handle_figure_name(
    #         basename="initial_conditions_schematic", figure_counter=figure_counter
    #     )
    #     handle_paper_plot_initial_conditions_schematic(
    #         output_name=output_name, show_plot=show_plot
    #     )
    # figure_counter += 1

    # ###################
    # # trajectory_classification_overview
    # if figure_flags.get("fig_5_trajectory_classification_overview", True):
    #     output_name = handle_figure_name(
    #         basename="trajectory_classification_overview", figure_counter=figure_counter
    #     )
    #     handle_paper_plot_trajectory_classification_example(
    #         output_name=output_name, show_plot=show_plot
    #     )
    # figure_counter += 1

    # ###################
    # # stream_interpolation_schematic
    # if figure_flags.get("fig_6_stream_interpolation_schematic", True):
    #     output_name = handle_figure_name(
    #         basename="stream_interpolation_schematic", figure_counter=figure_counter
    #     )
    #     handle_paper_plot_example_stream_interpolation_schematic(
    #         output_name=output_name, show_plot=show_plot
    #     )
    # figure_counter += 1

    # ###################
    # # exploration_parameter_ranges
    # if figure_flags.get("fig_7_exploration_parameter_ranges", True):
    #     output_name = handle_figure_name(
    #         basename="exploration_parameter_ranges", figure_counter=figure_counter
    #     )
    #     handle_paper_plot_exploration_parameter_distributions(
    #         input_filename=exploration_data_filename,
    #         output_name=output_name,
    #         show_plot=show_plot,
    #     )
    # figure_counter += 1

    # ###################
    # # exploration_alpha_parameter_range
    # if figure_flags.get("fig_8_exploration_alpha_parameter_range", True):
    #     output_name = handle_figure_name(
    #         basename="exploration_alpha_parameter_range", figure_counter=figure_counter
    #     )
    #     handle_paper_plot_exploration_distribution_alpha_parameter(
    #         input_filename=exploration_data_filename,
    #         output_name=output_name,
    #         show_plot=show_plot,
    #     )
    # figure_counter += 1

    # ###################
    # # exploration_alpha_vs_synchronicity
    # if figure_flags.get("fig_9_exploration_alpha_vs_synchronicity", True):
    #     output_name = handle_figure_name(
    #         basename="exploration_alpha_vs_synchronicity", figure_counter=figure_counter
    #     )
    #     handle_paper_plot_alpha_f_distributions(
    #         input_filename=exploration_data_filename,
    #         output_name=output_name,
    #         show_plot=show_plot,
    #     )
    # figure_counter += 1

    # ###################
    # # radii_of_closest_approach_low_v
    # if figure_flags.get("fig_10_radii_of_closest_approach_low_v", True):
    #     output_name = handle_figure_name(
    #         basename="radii_of_closest_approach_low_v", figure_counter=figure_counter
    #     )
    #     handle_paper_plot_radius_of_closest_approach_low_v(
    #         input_filename=ballistic_data_filename,
    #         output_name=output_name,
    #         show_plot=show_plot,
    #     )
    # figure_counter += 1

    # ###################
    # # ratio_rcirc_rmin_low_v
    # if figure_flags.get("fig_11_ratio_rcirc_rmin_low_v", True):
    #     output_name = handle_figure_name(
    #         basename="ratio_rcirc_rmin_low_v", figure_counter=figure_counter
    #     )
    #     handle_paper_plot_ratio_rcirc_rmin_low_v(
    #         input_filename=ballistic_data_filename,
    #         output_name=output_name,
    #         show_plot=show_plot,
    #     )
    # figure_counter += 1

    # ###################
    # # self_accretion_h_factor_low_v
    # if figure_flags.get("fig_12_self_accretion_h_factor_low_v", True):
    #     output_name = handle_figure_name(
    #         basename="self_accretion_h_factor_low_v", figure_counter=figure_counter
    #     )
    #     handle_paper_plot_specific_angular_momentum_factor_self_accretion_low_v(
    #         input_filename=ballistic_data_filename,
    #         output_name=output_name,
    #         show_plot=show_plot,
    #     )
    # figure_counter += 1

    # ###################
    # # classification_fractions_low_v
    # if figure_flags.get("fig_13_classification_fractions_low_v", True):
    #     output_name = handle_figure_name(
    #         basename="classification_fractions_low_v", figure_counter=figure_counter
    #     )
    #     handle_paper_plot_classification_fractions_low_v(
    #         input_filename=ballistic_data_filename,
    #         output_name=output_name,
    #         show_plot=show_plot,
    #     )
    # figure_counter += 1

    # ###################
    # # intersection_overview_low_v
    # if figure_flags.get("fig_14_intersection_overview_low_v", True):
    #     output_name = handle_figure_name(
    #         basename="intersection_overview_low_v", figure_counter=figure_counter
    #     )
    #     handle_paper_plot_intersection_overview_low_v(
    #         input_filename=ballistic_data_filename,
    #         output_name=output_name,
    #         show_plot=show_plot,
    #     )
    # figure_counter += 1

    # ###################
    # # specific_angular_momentum_of_the_stream_low_v_synchronous
    # if figure_flags.get(
    #     "fig_15_specific_angular_momentum_of_the_stream_low_v_synchronous", True
    # ):
    #     output_name = handle_figure_name(
    #         basename="specific_angular_momentum_of_the_stream_low_v_synchronous",
    #         figure_counter=figure_counter,
    #     )
    #     handle_paper_plot_specific_angmom_stream_interpolation_low_v_synchronous(
    #         input_filename=ballistic_data_filename,
    #         output_name=output_name,
    #         show_plot=show_plot,
    #     )
    # figure_counter += 1

    # ###################
    # # radii_of_closest_approach_high_v
    # if figure_flags.get("fig_16_radii_of_closest_approach_high_v", True):
    #     output_name = handle_figure_name(
    #         basename="radii_of_closest_approach_high_v", figure_counter=figure_counter
    #     )
    #     handle_paper_plot_radius_of_closest_approach_high_v(
    #         input_filename=ballistic_data_filename,
    #         output_name=output_name,
    #         show_plot=show_plot,
    #     )
    # figure_counter += 1

    # ###################
    # # ratio_rcirc_rmin_high_v
    # if figure_flags.get("fig_17_ratio_rcirc_rmin_high_v", True):
    #     output_name = handle_figure_name(
    #         basename="ratio_rcirc_rmin_high_v", figure_counter=figure_counter
    #     )
    #     handle_paper_plot_ratio_rcirc_rmin_high_v(
    #         input_filename=ballistic_data_filename,
    #         output_name=output_name,
    #         show_plot=show_plot,
    #     )
    # figure_counter += 1

    # ###################
    # # self_accretion_h_factor_high_v
    # if figure_flags.get("fig_18_self_accretion_h_factor_high_v", True):
    #     output_name = handle_figure_name(
    #         basename="self_accretion_h_factor_high_v", figure_counter=figure_counter
    #     )
    #     handle_paper_plot_specific_angular_momentum_factor_self_accretion_high_v(
    #         input_filename=ballistic_data_filename,
    #         output_name=output_name,
    #         show_plot=show_plot,
    #     )
    # figure_counter += 1

    # ###################
    # # classification_fractions_high_v
    # if figure_flags.get("fig_19_classification_fractions_high_v", True):
    #     output_name = handle_figure_name(
    #         basename="classification_fractions_high_v", figure_counter=figure_counter
    #     )
    #     handle_paper_plot_classification_fractions_high_v(
    #         input_filename=ballistic_data_filename,
    #         output_name=output_name,
    #         show_plot=show_plot,
    #     )
    # figure_counter += 1

    # ###################
    # # intersection_overview_high_v
    # if figure_flags.get("fig_20_intersection_overview_high_v", True):
    #     output_name = handle_figure_name(
    #         basename="intersection_overview_high_v", figure_counter=figure_counter
    #     )
    #     handle_paper_plot_intersection_overview_high_v(
    #         input_filename=ballistic_data_filename,
    #         output_name=output_name,
    #         show_plot=show_plot,
    #     )
    # figure_counter += 1

    # #########################################################################################
    # ##                           APPENDIX PLOTS                                            ##
    # #########################################################################################

    # ###################
    # # lagrange_points_FOR
    # if figure_flags.get("fig_21_lagrange_points_FOR", True):
    #     output_name = handle_figure_name(
    #         basename="lagrange_points_FOR", figure_counter=figure_counter
    #     )
    #     handle_paper_plot_lagrange_point_overview_frames_of_reference(
    #         output_name=output_name,
    #         show_plot=show_plot,
    #     )
    # figure_counter += 1


if __name__ == "__main__":
    ################
    # NOTE: make sure 'paper_ballistic_Hendriks2023_data_dir' environment variable is set or change this line
    RESULT_ROOT = os.getenv("paper_ballistic_Hendriks2023_data_dir")

    # Set up the exploration data filename
    exploration_data_filename = os.path.join(
        os.getenv("BINARYC_DATA_ROOT"),
        "RLOF",
        "server_results",
        "EVENTS_V2.2.4_HIGH_RES_EXPLORATION_BALLISTIC_RLOF_2021_MOE_DISTEFANO",
        "population_results",
        "Z0.02",
        "subproject_ballistic_exploration_velocity_dispersion_ensemble_data.csv",
    )

    # Set up ballistic data filename
    ballistic_data_filename = os.path.join(
        os.environ["PROJECT_DATA_ROOT"],
        "ballistic_data",
        "L1_stream",
        "server_results",
        "ballistic_stream_integration_results_stage_3_OPPOSITE_ASYNCHRONOUS_OFFSET_WITH_INTERPOLATION",
        "trajectory_summary_data.txt",
    )

    # Output dir
    plot_output_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    )

    # Output dir
    # plot_output_dir = os.path.join(this_file_dir, "all_paper_plots")

    #
    plot_all_paper_figures(
        exploration_data_filename=exploration_data_filename,
        ballistic_data_filename=ballistic_data_filename,
        plot_output_dir=plot_output_dir,
        figure_flags={
            # Schematics
            "fig_1_rochelobe_schematic": True,
            "fig_2_combined_area_plot": False,
            "fig_3_schematic_offset_v_nonsync": False,
            "fig_4_initial_conditions_schematic": False,
            "fig_5_trajectory_classification_overview": False,
            "fig_6_stream_interpolation_schematic": False,
            # Binary population exploration
            "fig_7_exploration_parameter_ranges": False,
            "fig_8_exploration_alpha_parameter_range": False,
            "fig_9_exploration_alpha_vs_synchronicity": False,
            # Ballistic data
            # low-v
            "fig_10_radii_of_closest_approach_low_v": False,
            "fig_11_ratio_rcirc_rmin_low_v": False,
            "fig_12_self_accretion_h_factor_low_v": False,
            "fig_13_classification_fractions_low_v": False,
            "fig_14_intersection_overview_low_v": False,
            "fig_15_specific_angular_momentum_of_the_stream_low_v_synchronous": True,
            # high-v
            "fig_16_radii_of_closest_approach_high_v": False,
            "fig_17_ratio_rcirc_rmin_high_v": False,
            "fig_18_self_accretion_h_factor_high_v": False,
            "fig_19_classification_fractions_high_v": False,
            "fig_20_intersection_overview_high_v": False,
            # appendix
            "fig_21_lagrange_points_FOR": False,
            "figure_format": "pdf",
        },
    )
