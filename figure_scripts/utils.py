import matplotlib.pyplot as plt

from paper_ballistic_scripts.ballistic_integrator_functions.frame_of_reference import (
    calculate_lagrange_points,
)


def load_mpl_rc():
    import matplotlib as mpl

    # https://matplotlib.org/users/customizing.html
    mpl.rc(
        "axes",
        labelweight="normal",
        linewidth=2,
        labelsize=30,
        grid=True,
        titlesize=40,
        facecolor="white",
    )

    mpl.rc("savefig", dpi=100)

    mpl.rc("lines", linewidth=4, color="g", markeredgewidth=2)

    mpl.rc(
        "ytick",
        **{
            "labelsize": 30,
            "color": "k",
            "left": True,
            "right": True,
            "major.size": 12,
            "major.width": 2,
            "minor.size": 6,
            "minor.width": 2,
            "major.pad": 12,
            "minor.visible": True,
            "direction": "inout",
        }
    )

    mpl.rc(
        "xtick",
        **{
            "labelsize": 30,
            "top": True,
            "bottom": True,
            "major.size": 12,
            "major.width": 2,
            "minor.size": 6,
            "minor.width": 2,
            "major.pad": 12,
            "minor.visible": True,
            "direction": "inout",
        }
    )

    mpl.rc("legend", frameon=False, fontsize=30, title_fontsize=30)

    mpl.rc("contour", negative_linestyle="solid")

    mpl.rc(
        "figure",
        figsize=[16, 16],
        titlesize=30,
        dpi=100,
        facecolor="white",
        edgecolor="white",
        frameon=True,
        max_open_warning=10,
        # autolayout=True
    )

    mpl.rc(
        "legend",
        fontsize=20,
        handlelength=2,
        loc="best",
        fancybox=False,
        numpoints=2,
        framealpha=None,
        scatterpoints=3,
        edgecolor="inherit",
    )

    mpl.rc("savefig", dpi="figure", facecolor="white", edgecolor="white")

    mpl.rc("grid", color="b0b0b0", alpha=0.5)

    mpl.rc("image", cmap="viridis")

    mpl.rc(
        "font",
        # weight='bold',
        serif="Palatino",
        size=20,
    )

    mpl.rc("errorbar", capsize=2)

    mpl.rc("mathtext", default="sf")


def show_and_save_plot(fig, plot_settings, verbose=1):
    """
    Wrapper to handle the saving and the showing of the plots
    """

    if plot_settings.get("show_plot", False):
        plt.show(block=plot_settings.get("block", True))
    else:
        plt.ioff()

    if plot_settings.get("output_name", None):
        if os.path.dirname(plot_settings["output_name"]):
            if verbose:
                print(
                    "creating output directory {}".format(
                        os.path.dirname(plot_settings["output_name"])
                    )
                )
            os.makedirs(os.path.dirname(plot_settings["output_name"]), exist_ok=True)
        fig.savefig(plot_settings["output_name"], bbox_inches="tight")

        try:
            caller_name = inspect.currentframe().f_back.f_code.co_name
        except:
            caller_name = ""
        if verbose:
            print(
                "{}wrote plot to {}".format(
                    "{}: ".format(caller_name) if caller_name else "",
                    plot_settings["output_name"],
                )
            )

    # Stuff to close and handle the cleaning of the garbage
    plt.close(fig)
    plt.close("all")


def add_labels_subplots(fig, axes_list, label_function_kwargs, custom_locs=None):
    """
    Function to loop over a list of axes of a figure and add subplots
    """

    # Add subplot labels
    for ax_i, ax in enumerate(axes_list):
        panel_label_function_kwargs = label_function_kwargs
        # Custom per-panel placement
        if custom_locs is not None:
            panel_label_function_kwargs = {
                **panel_label_function_kwargs,
                "x_loc": custom_locs[ax_i]["x"],
                "y_loc": custom_locs[ax_i]["y"],
            }

        add_label_subplot(fig=fig, ax=ax, label_i=ax_i, **panel_label_function_kwargs)


def add_label_subplot(
    fig,
    ax,
    label_i,
    x_loc=0.95,
    y_loc=0.95,
    fontsize=24,
    verticalalignment="top",
    bbox_dict={"pad": 0.5, "facecolor": "None", "boxstyle": "round"},
):
    """
    Function to add a label to the subplot
    """

    subplot_labels = "abcdefghijklmnopqrstuvwxyz"

    trans = mtransforms.ScaledTranslation(10 / 72, -5 / 72, fig.dpi_scale_trans)
    ax.text(
        x_loc,
        y_loc,
        subplot_labels[label_i],
        transform=ax.transAxes + trans,
        fontsize=fontsize,
        verticalalignment=verticalalignment,
        bbox=dict(**bbox_dict),
        zorder=500,
    )

    return fig, ax


def delete_out_of_frame_text(fig, ax):
    """
    Function that deletes text objects that fall out of frame
    """

    # find text out of bounds
    xlims = ax.get_xlim()
    ylims = ax.get_ylim()

    for text in ax.texts:
        text_pos = text.get_position()
        if (not xlims[0] <= text_pos[0] <= xlims[1]) or (
            not ylims[0] <= text_pos[1] <= ylims[1]
        ):
            text.remove()

    return fig, ax


def plot_system(
    system_dict,
    frame_of_reference,
    settings,
    plot_settings,
    resultfile=None,
    resultdata=None,
    resultdata_list=None,
    resultdata_sets=None,
    #
    plot_rochelobe_equipotentials=True,
    plot_rochelobe_equipotential_meshgrid=False,
    plot_stars_and_COM=True,
    plot_L_points=True,
    plot_trajectory=True,
    plot_system_information_panel=True,
    plot_system_add_colorbar=False,
    add_gradients=False,
    add_velocities_and_accelarations=False,
    thin_trajectories=False,
    return_fig=False,
    fig=None,
):
    """
    function to plot system with sepinsky based lagrange points

    resultdata_sets is a dict with trajectory sets and plot instructions (colors)
    """

    if plot_settings is None:
        plot_settings = {}

    ####################
    #
    # Set up canvas
    if fig is None:
        fig = plt.figure(figsize=(30, 18))
    fig.subplots_adjust(hspace=0.7)
    axes_list = []

    # Set up gridspec
    gs = fig.add_gridspec(nrows=1, ncols=8)

    #
    if plot_system_information_panel and plot_system_add_colorbar:
        raise ValueError("cant add the informationpanel AND a colorbar currently")

    if plot_system_information_panel:
        ax = fig.add_subplot(gs[:, :6])
        ax_info = fig.add_subplot(gs[:, -2:])
        axes_list.append(ax)
        axes_list.append(ax_info)
        ax_cb = None

    elif plot_system_add_colorbar:
        ax = fig.add_subplot(gs[:, :-1])
        ax_cb = fig.add_subplot(gs[:, -1:])
        axes_list.append(ax)
        axes_list.append(ax_cb)
    else:
        ax = fig.add_subplot(gs[:, :])
        axes_list.append(ax)
        ax_cb = None

    #####################
    # Add all the plot components
    if plot_rochelobe_equipotentials:
        fig, ax = plot_rochelobe_equipotentials_function(
            fig,
            ax,
            settings=settings,
            system_dict=system_dict,
            frame_of_reference=frame_of_reference,
            plot_settings=plot_settings,
        )

    if plot_rochelobe_equipotential_meshgrid:
        fig, ax, ax_cb = plot_rochelobe_equipotential_meshgrid_function(
            fig,
            ax,
            ax_cb,
            settings=settings,
            system_dict=system_dict,
            frame_of_reference=frame_of_reference,
            add_gradients=add_gradients,
            add_colorbar=plot_system_add_colorbar,
            plot_settings=plot_settings,
        )

    if plot_stars_and_COM:
        fig, ax = plot_stars_and_COM_function(
            fig,
            ax,
            system_dict=system_dict,
            frame_of_reference=frame_of_reference,
            plot_settings=plot_settings,
        )

    if plot_L_points:
        fig, ax = plot_L_points_function(
            fig,
            ax,
            system_dict=system_dict,
            frame_of_reference=frame_of_reference,
            plot_settings=plot_settings,
        )

    if plot_trajectory:
        #############
        # Plot a specific trajectory based on loaded data or a file containing the data
        if resultfile or resultdata:
            if resultfile:
                # Load data
                with open(resultfile, "r") as json_file:
                    data = json.load(json_file)
            elif resultdata:
                data = resultdata
            else:
                raise ValueError(
                    "PLease provide a filename or the result data to plot the trajectory"
                )

            #
            fig, ax = plot_trajectory_function(
                fig=fig,
                ax=ax,
                resultdata=data,
                settings=settings,
                add_velocities_and_accelarations=add_velocities_and_accelarations,
                thin_trajectories=thin_trajectories,
            )

        #############
        # Plot a list of trajectories
        if resultdata_list:
            for resultdata_i in resultdata_list:
                #
                fig, ax = plot_trajectory_function(
                    fig=fig,
                    ax=ax,
                    resultdata=resultdata_i,
                    settings=settings,
                    plot_kwargs={"alpha": resultdata_i["weight"]},
                    thin_trajectories=thin_trajectories,
                )

        #############
        # Plot a dict of lists of trajectories:
        if resultdata_sets:
            label_color_list = []
            for trajectory_set_dict in resultdata_sets:
                trajectory_list = trajectory_set_dict["trajectory_list"]
                plot_kwargs = trajectory_set_dict["plot_kwargs"]

                if trajectory_list:
                    label_color_list.append(plot_kwargs)

                # Loop over the trajectories
                for resultdata_i in trajectory_list:
                    fig, ax = plot_trajectory_function(
                        fig=fig,
                        ax=ax,
                        resultdata=resultdata_i,
                        plot_kwargs={"alpha": resultdata_i["weight"], **plot_kwargs},
                        settings=settings,
                        thin_trajectories=thin_trajectories,
                    )

            # Add a legend based on the colors and labels
            lines = []
            for label_color_dict in label_color_list:
                lines.append(
                    mlines.Line2D(
                        [],
                        [],
                        color=label_color_dict["color"],
                        marker="D",
                        ls="",
                        label=label_color_dict["label"],
                    )
                )
            ax.legend(
                handles=lines,
                frameon=True,
                fontsize=plot_settings.get(
                    "trajectory_classification_legend_fontsize", 36
                ),
            )

    #########
    if plot_system_information_panel:
        if resultfile:
            # Load data
            with open(resultfile, "r") as json_file:
                data = json.load(json_file)
        elif resultdata:
            data = resultdata

        fig, ax_info = plot_system_information_panel_function(fig, ax_info, data)

    #
    if not return_fig:
        plt.show()
    else:
        return fig, axes_list


def plot_rochelobe_equipotentials_function(
    fig, ax, settings, system_dict, frame_of_reference, plot_settings
):
    """
    Function to plot the contour for the roche lobe
    """

    bounds_x = plot_settings["plot_bounds_x"]
    bounds_y = plot_settings["plot_bounds_y"]
    resolution = plot_settings["plot_resolution"]

    # System description
    q = system_dict["mass_accretor"] / system_dict["mass_donor"]

    # Get the lagrange points and the corresponding potential values
    lagrange_points = calculate_lagrange_points(
        system_dict=system_dict, frame_of_reference=frame_of_reference
    )

    # Calculate potential values at those points
    phi_l1 = calculate_potential(
        settings=settings,
        system_dict=system_dict,
        frame_of_reference=frame_of_reference,
        x_pos=lagrange_points["L1"][0],
        y_pos=lagrange_points["L1"][1],
    )
    phi_l2 = calculate_potential(
        settings=settings,
        system_dict=system_dict,
        frame_of_reference=frame_of_reference,
        x_pos=lagrange_points["L2"][0],
        y_pos=lagrange_points["L2"][1],
    )
    phi_l3 = calculate_potential(
        settings=settings,
        system_dict=system_dict,
        frame_of_reference=frame_of_reference,
        x_pos=lagrange_points["L3"][0],
        y_pos=lagrange_points["L3"][1],
    )

    #
    lagrange_contour_dict = {
        "l1": {
            "phi": phi_l1,
            "color": "blue",
            "label": r"$\it{L}_{1}$",
            "linestyle": "solid",
        },
        "l2": {
            "phi": phi_l2,
            "color": "orange",
            "label": r"$\it{L}_{2}$",
            "linestyle": "--",
        },
        "l3": {
            "phi": phi_l3,
            "color": "green",
            "label": r"$\it{L}_{3}$",
            "linestyle": "-.",
        },
    }

    # check if values are double
    if lagrange_contour_dict["l2"]["phi"] == lagrange_contour_dict["l3"]["phi"]:
        del lagrange_contour_dict["l3"]

    # Set up list and sort
    lagrange_contour_list = sorted(
        list(lagrange_contour_dict.values()),
        key=lambda x: x["phi"],
    )

    #########
    # Plot the entire potential

    # Create a meshgrid for x and y
    xx, yy = np.meshgrid(
        np.linspace(bounds_x[0], bounds_x[1], resolution),
        np.linspace(bounds_y[0], bounds_y[1], resolution),
    )

    # Calculate all the phi values
    phi_values = calculate_potential(
        settings=settings,
        system_dict=system_dict,
        frame_of_reference=frame_of_reference,
        x_pos=xx,
        y_pos=yy,
        z_pos=np.zeros(yy.shape),
    )

    # Draw the contours
    CS = ax.contour(
        xx,
        yy,
        phi_values,
        levels=[el["phi"] for el in lagrange_contour_list],
        colors=[el["color"] for el in lagrange_contour_list],
        linewidths=plot_settings.get("linewidth", 2),
        linestyles=[el["linestyle"] for el in lagrange_contour_list],
    )

    clabel_dict = {
        "l1": r"$\Phi_{\mathrm{L1}}$",
        "l2": r"$\Phi_{\mathrm{L2}}$",
        "l3": r"$\Phi_{\mathrm{L3}}$",
    }

    # Add labels for the contours
    fmt = {}
    for l, s in zip(
        CS.levels, [clabel_dict[key] for key in lagrange_contour_dict.keys()]
    ):
        fmt[l] = s

    # Label every other level using strings
    ax.clabel(
        CS,
        CS.levels,
        inline=True,
        fmt=fmt,
        fontsize=plot_settings.get("xtextsize", 24),
        colors="k",
        inline_spacing=15,
        manual=plot_settings.get("manual_clabel_placement", False),
    )

    return fig, ax


def align_axes(fig, axes_list, which_axis="x"):
    """
    Function to align the x or y axis of a list of axes
    """

    if which_axis == "x":
        getter = "get_xlim"
        setter = "set_xlim"
        log_getter = "get_xscale"
        log_setter = "set_xscale"
    elif which_axis == "y":
        getter = "get_ylim"
        setter = "set_ylim"
        log_getter = "get_yscale"
        log_setter = "set_yscale"
    else:
        raise ValueError("not implemented yet")

    min_val = 1e9
    max_val = 1e-9

    #
    any_log = False
    for axis in axes_list:
        logscale = axis.__getattribute__(log_getter)()
        if logscale == "log":
            any_log = True

    # Find the min and max
    for axis in axes_list:
        lims = axis.__getattribute__(getter)()

        min_val = np.min([min_val, lims[0]])
        max_val = np.max([max_val, lims[1]])

    # Set the min and max
    for axis in axes_list:
        axis.__getattribute__(setter)([min_val, max_val])

    # handle setting to log if required
    if any_log:
        for axis in axes_list:
            logscale = axis.__getattribute__(log_setter)("log")
