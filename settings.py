# some global standards
standard_system_dict = {
    "mass_donor": 1,
    "mass_accretor": 1,
    "separation": 1,
    "synchronicity_factor": 1,
    "eccentricity": 0,
    "mean_anomaly": 0,
}
